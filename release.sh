#!/bin/bash

set -e

if ! git diff --quiet HEAD; then
    echo "Release aborted because there are uncommitted files\n" 1>&2
    exit 1
fi

VERSION=($(sed -n 's|.*spec.version.*"\([^"]*\)".*|\1|p' MindSDK.podspec | tr . ' '))

VERSION[2]="$(sed 's|-SNAPSHOT||' <<< ${VERSION[2]})"
sed -i '' "s|-SNAPSHOT||" MindSDK.podspec Sources/Info.plist Sources/MindSDK.swift
sed -Ei '' "s|(:tag => '\|\.\./)[0-9]*\.[0-9]*\.[0-9]*|\1${VERSION[0]}.${VERSION[1]}.${VERSION[2]}|" README.md
git commit -a -m "Prepare release ${VERSION[0]}.${VERSION[1]}.${VERSION[2]}"
git tag "${VERSION[0]}.${VERSION[1]}.${VERSION[2]}"
git tag -f latest
git push
git push -f --tags

sed -i '' "s|\"${VERSION[0]}\.${VERSION[1]}\.${VERSION[2]}\"|\"${VERSION[0]}.$((${VERSION[1]} + 1)).${VERSION[2]}-SNAPSHOT\"|" MindSDK.podspec
sed -i '' "s|>${VERSION[0]}\.${VERSION[1]}\.${VERSION[2]}<|>${VERSION[0]}.$((${VERSION[1]} + 1)).${VERSION[2]}-SNAPSHOT<|" Sources/Info.plist
sed -i '' "s|\"${VERSION[0]}\.${VERSION[1]}\.${VERSION[2]}\"|\"${VERSION[0]}.$((${VERSION[1]} + 1)).${VERSION[2]}-SNAPSHOT\"|" Sources/MindSDK.swift
git commit -a -m "Prepare for next development iteration"
git push
