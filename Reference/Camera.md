# `class` Camera

Camera class is used for representing all cameras of the iOS device. iOS doesn't allow capturing multiple cameras
simultaneously, that's why Mind iOS SDK represents all cameras with a single [multi-facing](#setfacingfacing) instance
of Camera class that can be got with [getCamera](DeviceRegistry.md#static-getcamera) method of
[DeviceRegistry](DeviceRegistry.md) class. Camera class implements
[MediaStreamVideoSupplier](MediaStreamVideoSupplier.md) protocol, so it can be used as a source of video for local
[MediaStream](MediaStream.md).

```swift
let deviceRegistry = MindSDK.getDeviceRegistry()
let camera = deviceRegistry.getCamera()
camera.set(facing: .USER)
camera.set(width: 1280, height: 720)
camera.set(fps: 25)
camera.set(bitrate: 2500000)
camera.set(adaptivity: 3)
let myStream = MindSDK.createMediaStream(nil, camera)
me.setMediaStream(myStream)
camera.acquire().catch({ error in
    print("Camera can't be acquired: \(error)")
})
```

Thread-Safety: all methods of Camera class can be called on the main thread only. Calling these methods on another
thread will result in an `Error` or undefined behavior.

## set(facing)

Sets the facing of the camera. The facing of the camera is a direction which the camera can be pointed to. Front and
back cameras on smartphones (and other mobile devices) are usually combined into a single multi-facing camera which is
represented with a single instance of `Camera` class. The facing of any multi-facing camera can be changed at any
moment regardless whether the camera is acquired or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;facing – The facing of the camera.

## set(width, height)

Sets the resolution of the camera. The resolution of the camera is a resolution which the camera should capture the
video in. The video from the camera can be transmitted over the network in
[multiple encodings (e.g. resolutions) simultaneously](#setadaptivity). The resolution can be changed at any moment
regardless whether the camera is acquired or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;width – The horizontal resolution of the camera.  
&nbsp;&nbsp;&nbsp;&nbsp;height – The vertical resolution of the camera.

## set(fps)

Sets the frame rate of the camera. The frame rate of the camera is a rate which the camera should capture the video at.
The frame rate can be changed at any moment regardless whether the camera is acquired or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;fps – The frame rate of the camera.

## set(bitrate)

Sets the bitrate of the camera. The bitrate of the camera is a number of bits which each second of video from the
camera should not exceed while being transmitting over the network. The bitrate is shared among all
[encodings](#setadaptivity) proportionally and can be changed at any moment regardless whether the camera is acquired
or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;bitrate – The bitrate of the camera.

## set(adaptivity)

Sets the adaptivity of the camera. The adaptivity of the camera is an integer (in range between 1 and 3, inclusively)
which defines a number of encodings in which the video from the camera should be transmitted over the network. The
encodings differ in resolution: the resolution of the first encoding equals the
[resolution of the camera](setwidth-height), the resolution of the second encoding is half (in each dimension) of the
resolution of the first one, the resolution of the third encoding is half (in each dimension) of the resolution of the
second one. For example, if the resolution of the camera is 1280x720 and adaptivity is 3, the video from the camera
would be transmitted over the network in 3 encoding: 1280x720, 640x360 and 320x180. The adaptivity can be changed at
any moment regardless whether the camera is acquired or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;adaptivity – The adaptivity of the camera.

## acquire()

Starts camera capturing. This is an asynchronous operation which assumes acquiring the underlying camera device and
distributing camera's video among all [consumers](MediaStream.md). This method returns a `Promise` which resolves with
no value (if the camera capturing starts successfully) or rejects with an `Error` (if there is no permission to access
the camera or if the camera was unplugged). If the camera capturing has been already started, this method returns
already resolved `Promise`.

**Returns:**

&nbsp;&nbsp;&nbsp;&nbsp;The promise that either resolves with no value or rejects with an `Error`.

## release()

Stops camera capturing. This is a synchronous operation which assumes revoking the previously distributed camera's
video and releasing the underlying camera device. The stopping is idempotent: the method does nothing if the camera is
not acquired, but it would fail if it was called in the middle of acquisition.
