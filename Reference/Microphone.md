# `class` Microphone

Microphone class is used for representing the microphone of the iOS device. The instance of Microphone class that
represent the built-in microphone can be got with [getMicrophone](DeviceRegistry.md#static-getmicrophone) method of
[DeviceRegistry](DeviceRegistry.md) class. Microphone class implements
[MediaStreamAudioSupplier](MediaStreamAudioSupplier.md) protocol, so it can be used as a source of audio for local
[MediaStream](MediaStream.md).

```swift
let deviceRegistry = MindSDK.getDeviceRegistry()
let microphone = deviceRegistry.getMicrophone()
MediaStream myStream = MindSDK.createMediaStream(microphone, nil)
me.setMediaStream(myStream)
microphone.acquire().catch({ error in
    print("Microphone can't be acquired: \(error)")
})
```

Thread-Safety: all methods of Microphone class can be called on the main thread only. Calling these methods on another
thread will result in an `Error` or undefined behavior.

## set(muted)

Sets the muted state of the microphone. The muted state of the microphone determines whether the microphone produces an
actual audio (if it is unmuted) or silence (if it is muted). The muted state can be changed at any moment regardless
whether the microphone is acquired or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;muted – The muted state of the microphone.

## acquire()

Starts microphone recording. This is an asynchronous operation which assumes acquiring the underlying microphone device
and distributing microphone's audio among all [consumers](MediaStream.md). This method returns a `Promise` which
resolves with no value (if the microphone recording starts successfully) or rejects with an `Error` (if there is no
permission to access the microphone or if the microphone was unplugged). If the microphone recording has been already
started, this method returns already resolved `Promise`.

**Returns:**

&nbsp;&nbsp;&nbsp;&nbsp;The promise that either resolves with no value or rejects with an `Error`.

## release()

Stops microphone recording. This is a synchronous operation which assumes revoking the previously distributed
microphone's audio and releasing the underlying microphone device. The stopping is idempotent: the method does nothing
if the microphone is not acquired, but it would fail if it was called in the middle of acquisition.
