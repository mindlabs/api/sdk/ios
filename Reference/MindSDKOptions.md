# `class` MindSDKOptions

MindSDKOptions class represents all available configuration options for Mind iOS SDK. The default initializer creates
an instance of MindSDKOptions class with the default values for all configuration options. If necessary, you can change
any default value before passing the instance to the static
[initialize](MindSDK.md#static-initializeoptions) method of [MindSDK](MindSDK.md) class:

```swift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let options = MindSDKOptions()
        options.setUseVp9ForSendingVideo(true)
        MindSDK.initialize(options)
        return true
    }

}
```

## setUseVp9ForSendingVideo(useVp9ForSendingVideo)

Sets whether VP9 codec should be used for sending video or not. If `true` then any outgoing video will be encoded with
VP9 in SVC mode, otherwise — with VP8 in simulcast mode. The default value is `false`.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;useVp9ForSendingVideo – Whether VP9 codec should be used for sending video or not.
