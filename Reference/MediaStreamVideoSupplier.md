# `protocol` MediaStreamVideoSupplier

Any video source of [MediaStream](MediaStream.md) is required to implement MediaStreamVideoSupplier protocol. The
protocol defines two methods: [addVideoConsumer](#addvideoconsumerconsumer) and
[removeVideoConsumer](#removevideoconsumerconsumer). [MediaStream](MediaStream.md) uses them to register and unregister
itself as a consumer of video from the supplier. The supplier should use `onVideoBuffer` method of
[MediaStream](MediaStream.md) class to supply its video to every registered media stream. The supplier is expected to
supply its video or `nil` value (if the video isn't available yet) to every newly registering media stream, and the
`nil` value to every unregistering media stream.

Thread-Safety: all methods of MediaStreamVideoSupplier class can be called on the main thread only. Calling these
methods on another thread will result in an `Error` or undefined behavior.

## addVideoConsumer(consumer)

Registers the specified [MediaStream](MediaStream.md) as a consumer of video from the supplier. It is expected that
during the registration the supplier will use `onVideoBuffer` method of [MediaStream](MediaStream.md) class to supply
its video or `nil` value (if the video isn't available yet) to the registering media stream.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;consumer – The media stream which should be registered as a consumer of video from the supplier.

## removeVideoConsumer(consumer)

Unregisters the specified [MediaStream](MediaStream.md) as a consumer of video from the supplier. It is expected that
during the unregistration the supplier will use `onVideoBuffer` method of [MediaStream](MediaStream.md) class to supply
the `nil` value to the unregistering media stream.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;consumer – The media stream which should be unregistered as a consumer of video from the
                                   supplier.
