# `class` DeviceRegistry

DeviceRegistry class provides access to all audio and video peripherals of the iOS device. It contains methods for
getting the [camera](#static-getcamera) and the [microphone](#static-getmicrophone).

## `static` getMicrophone()

Returns the [microphone](Microphone.md) of the iOS device.

**Returns:**

&nbsp;&nbsp;&nbsp;&nbsp;The microphone of the iOS device.

## `static` getCamera()

Returns the [camera](Camera.md) of the iOS device.

**Returns:**

&nbsp;&nbsp;&nbsp;&nbsp;The camera of the iOS device.
