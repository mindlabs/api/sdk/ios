# `protocol` MediaStreamVideoConsumer


Any video consumer of [MediaStream](MediaStream.md) is required to implement MediaStreamVideoConsumer protocol. The
protocol defines only one method: [onVideoBuffer](#onvideobuffervideobuffer-supplier). [MediaStream](MediaStream.md)
uses it to supply a new video to the consumer or cancel the previously supplied one. The consumer should use
`addVideoConsumer` and `removeVideoConsumer` methods of [MediaStream](MediaStream.md) class to register and unregister
itself as a consumer of video from the [MediaStream](MediaStream.md), respectively.

Thread-Safety: all methods of MediaStreamVideoConsumer class can be called on the main thread only. Calling these
methods on another thread will result in an `Error` or undefined behavior.

## onVideoBuffer(videoBuffer, supplier)

Supplies a new video to the consumer or cancels the previously supplied one in which case `nil` is passed as a value
for `videoBuffer` argument. It is guaranteed that during unregistration this method is always called with `nil` as a
value for `videoBuffer` argument regardless whether video has been supplied to the consumer or not.

**Parameters:**

&nbsp;&nbsp;&nbsp;&nbsp;videoBuffer – The new video buffer or `nil` if the previously supplied video buffer should be
                                      canceled.  
&nbsp;&nbsp;&nbsp;&nbsp;supplier – The media stream which supplies or cancels the video buffer.
