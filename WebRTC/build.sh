#!/bin/bash

set -e

if ! xcodebuild -version &> /dev/null; then
    echo "To build WebRTC framework you should install XCode first" 1>&2
    exit 1
fi

WEBRTC_DIRECTORY="$PWD/$(dirname "$0")"

mkdir -p build/webrtc_ios
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git build/depot_tools
export PATH="$PWD/build/depot_tools:$PATH"
cd build/webrtc_ios
fetch --nohooks webrtc_ios
cd src
git checkout branch-heads/6367 # Which corresponds to Chromium 124 <https://chromiumdash.appspot.com/branches>
gclient sync -D
# Build `WebRTC.xcframework` without debug symbols.
sed -Ei '' 's/WebRTC.dSYM/WebRTC.dSYM-inexistent/; /enable_dsyms/d' ./tools_webrtc/ios/build_ios_libs.py
# Enable support of VP9 video codec.
sed -Ei '' 's/ *LIBVPX_BUILD_VP9 *= *False/LIBVPX_BUILD_VP9 = True/' ./tools_webrtc/ios/build_ios_libs.py
# Add support of `scalabilityMode` for VP9 SVC.
patch -p1 << "EOF"
diff --git a/api/video_codecs/video_encoder_factory.h b/api/video_codecs/video_encoder_factory.h
index d28a2a4035..236aeea491 100644
--- a/api/video_codecs/video_encoder_factory.h
+++ b/api/video_codecs/video_encoder_factory.h
@@ -86,12 +86,9 @@ class VideoEncoderFactory {
       const SdpVideoFormat& format,
       absl::optional<std::string> scalability_mode) const {
     // Default implementation, query for supported formats and check if the
-    // specified format is supported. Returns false if scalability_mode is
-    // specified.
+    // specified format is supported.
     CodecSupport codec_support;
-    if (!scalability_mode) {
-      codec_support.is_supported = format.IsCodecInList(GetSupportedFormats());
-    }
+    codec_support.is_supported = format.IsCodecInList(GetSupportedFormats());
     return codec_support;
   }
 
diff --git a/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.h b/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.h
index 07f6b7a39c..cc4aa8d95d 100644
--- a/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.h
+++ b/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.h
@@ -56,6 +56,11 @@ RTC_OBJC_EXPORT
  */
 @property(nonatomic, copy, nullable) NSNumber *scaleResolutionDownBy;
 
+/** If non-nil, identifier of the scalability mode for video. If nil,
+ * implementation default scalability mode will be used.
+ */
+@property(nonatomic, copy, nullable) NSString *scalabilityMode;
+
 /** The SSRC being used by this encoding. */
 @property(nonatomic, readonly, nullable) NSNumber *ssrc;
 
diff --git a/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.mm b/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.mm
index d6087dafb0..90d42a2798 100644
--- a/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.mm
+++ b/sdk/objc/api/peerconnection/RTCRtpEncodingParameters.mm
@@ -21,6 +21,7 @@ @implementation RTC_OBJC_TYPE (RTCRtpEncodingParameters)
 @synthesize maxFramerate = _maxFramerate;
 @synthesize numTemporalLayers = _numTemporalLayers;
 @synthesize scaleResolutionDownBy = _scaleResolutionDownBy;
+@synthesize scalabilityMode = _scalabilityMode;
 @synthesize ssrc = _ssrc;
 @synthesize bitratePriority = _bitratePriority;
 @synthesize networkPriority = _networkPriority;
@@ -56,6 +57,9 @@ - (instancetype)initWithNativeParameters:
       _scaleResolutionDownBy =
           [NSNumber numberWithDouble:*nativeParameters.scale_resolution_down_by];
     }
+    if (nativeParameters.scalability_mode) {
+      _scalabilityMode = [NSString stringForStdString:*nativeParameters.scalability_mode];
+    }
     if (nativeParameters.ssrc) {
       _ssrc = [NSNumber numberWithUnsignedLong:*nativeParameters.ssrc];
     }
@@ -89,6 +93,9 @@ - (instancetype)initWithNativeParameters:
     parameters.scale_resolution_down_by =
         absl::optional<double>(_scaleResolutionDownBy.doubleValue);
   }
+  if (_scalabilityMode != nil) {
+    parameters.scalability_mode = absl::optional<std::string>([NSString stdStringForString:_scalabilityMode]);
+  }
   if (_ssrc != nil) {
     parameters.ssrc = absl::optional<uint32_t>(_ssrc.unsignedLongValue);
   }
diff --git a/sdk/objc/native/src/objc_video_encoder_factory.mm b/sdk/objc/native/src/objc_video_encoder_factory.mm
index d4ea79cc88..60f27ea22a 100644
--- a/sdk/objc/native/src/objc_video_encoder_factory.mm
+++ b/sdk/objc/native/src/objc_video_encoder_factory.mm
@@ -167,7 +167,13 @@ void OnCurrentEncoder(const SdpVideoFormat &format) override {
     SdpVideoFormat format = [supportedCodec nativeSdpVideoFormat];
     supported_formats.push_back(format);
   }
-
+  for (webrtc::SdpVideoFormat& format : supported_formats) {
+    if (format.name == "VP9") {
+      for (const auto scalability_mode : kAllScalabilityModes) {
+          format.scalability_modes.push_back(scalability_mode);
+      }
+    }
+  }
   return supported_formats;
 }
 
EOF
./tools_webrtc/ios/build_ios_libs.py
rm -rf "$WEBRTC_DIRECTORY/WebRTC.xcframework"
cp -a out_ios_libs/WebRTC.xcframework "$WEBRTC_DIRECTORY/"
rm -rf "$WEBRTC_DIRECTORY/build"
