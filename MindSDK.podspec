Pod::Spec.new do |spec|

  spec.name             = "MindSDK"
  spec.version          = "5.13.0-SNAPSHOT"
  spec.license          = { :type => "BSD 3-Clause", :file => "LICENSE" }
  spec.summary          = "Mind iOS SDK"
  spec.homepage         = "https://gitlab.com/mindlabs/api/sdk/ios"
  spec.author           = "Mind Labs"
  spec.platform         = :ios, "10.0"
  spec.source           = { :git => "https://gitlab.com/mindlabs/api/sdk/ios.git", :tag => spec.version }
  spec.source_files     = "Sources/**/*.{swift}"
  spec.swift_version    = "5.0"

  spec.dependency "PromisesSwift", "~> 2.4.0"

  spec.vendored_frameworks = "WebRTC/WebRTC.xcframework"

end
