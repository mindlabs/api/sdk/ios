Mind iOS SDK is a iOS library that includes everything you need to add [Mind API-powered](https://api.mind.com)
real-time voice, video and text communication to your iOS-applications.

* [Introduction](#introduction)
* [Supported Devices](#supported-devices)
* [Example](#example)
* [Getting the SDK](#getting-the-sdk)
* [API Reference](#api-reference)

# Introduction

[Mind API](https://api.mind.com) is a cloud media platform (aka PaaS) which allows you to create applications for
conducting online meeting where participants can communicate to each other in real-time. A meeting in Mind API is
called a conference. Anyone who is participating in a conference using participant token is called a participant.
Usually, it is either a client part of your application (based on Mind Web/Android/iOS SDK) or SIP/H.323-enabled phone.
The responsibility for arranging/ending conferences and inviting/expelling participants into/from them lies with a
server part of your application which interacts with Mind API using application token. Also, the application token
allows the server part of your application to join any conference it created and stay informed of all actions of the
participants.

To communicate to each other participants use text, audio and video. Text is transmitted as messages each of which is
just a string, while audio and video are transmitted as media streams, each of which can contain one audio and one
video. Any participant (if permitted) can send two independent media streams simultaneously: primary and secondary. The
primary media stream is intended for sending video and audio taken from a camera and a microphone, respectively,
whereas the secondary media stream can be used for sending an additional audio/video content (e.g. screen sharing).
Depending on their capabilities and desires participants can receive video/audio which are sent by other participants
in SFU or MCU mode. The SFU mode assumes that primary and secondary media streams of all participant are received as is
— each media stream separately, while in MCU mode audio and video of all participants are mixed inside Mind API
(according to the desired layout) and received as a single media stream.

Mind iOS SDK allows iOS-applications to [join](../5.12.0/reference/MindSDK.md#static-join2uri-token-listener)
conferences on behalf of participants only. A successful joining attempt results in getting an object of
[Session](../5.12.0/reference/Session.md) class, which represents a participation session. The conference (which
iOS-application is participating in) is represented with an object of
[Conference](../5.12.0/reference/Conference.md) class, whereas participants can be represented as objects of either
[Participant](../5.12.0/reference/Participant.md) or [Me](../4.16.0/reference/Me.md) class. The latter is used for
representing the participant on behalf of whom iOS-application is participating in the conference (aka the local
participant), the former — for all other participants (aka the remote participants). Messages are represented with
standard [String](https://developer.apple.com/documentation/swift/string) objects, whereas media streams are
represented with instances of [MediaStream](../5.12.0/reference/MediaStream.md) class.

# Supported Devices

Our SDK is always compatible with the latest stable version of iOS, but older versions of iOS — down to iOS 11.0,
inclusively — are supported as well. Mind iOS SDK can run on devices with ARM64 architectures and on the iOS simulators
for x86-64 and ARM64 architectures.

# Example

To demonstrate what you can do with Mind iOS SDK, we created a simple open-source iOS-application for video
conferencing named [Confy](https://gitlab.com/mindlabs/api/demo/ios). Here is a summary of it which shows how to
participate in conference in MCU mode:

```swift
class ConferenceViewController: UIViewController {

    @IBOutlet var conferenceVideo: Video!

    override func viewDidLoad() {
        let mindSdkOptions = MindSDKOptions()
        MindSDK.initialize(mindSdkOptions)
        let deviceRegistry = MindSDK.getDeviceRegistry()
        let camera = deviceRegistry.getCamera()
        let microphone = deviceRegistry.getMicrophone()
        let options = SessionOptions()
        MindSDK.join("<CONFERENCE_URI>", "<PARTICIPANT_TOKEN>", options).then({ session in
            let listener = SessionListener()
            session.setListener(listener)
            let conference = session.getConference()
            let conferenceStream = conference.getMediaStream()
            self.conferenceVideo.setMediaStream(conferenceStream)
            let me = conference.getMe()
            let myStream = MindSDK.createMediaStream(microphone, camera)
            me.setMediaStream(myStream)
            all(microphone.acquire(), camera.acquire()).catch({ error in
                print("Can't acquire camera or microphone: \(error)")
            })
        })
    }

}
```

The summary above is missing two mandatory identifiers: `<CONFERENCE_URI>` and `<PARTICIPANT_TOKEN>`. The former has to
be replaced with an URI of an existent conference, the later with a token of an existent participant. Both of them
should be created on behalf of server part of your application using application token.

# Getting the SDK

Mind iOS SDK is an open-source framework which is distributed as a
[CocoaPod](https://guides.cocoapods.org/making/making-a-cocoapod.html) through GitLab. To plug in the latest version of
the SDK to your application, make sure that [Git LFS is installed](https://github.com/git-lfs/git-lfs#installing), and
add the following lines to the `target` section of the `Podfile`:

```config
use_frameworks!

pod 'MindSDK', :git => 'https://gitlab.com/mindlabs/api/sdk/ios.git', :tag => '5.12.0'

post_install do |installer|
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['ENABLE_BITCODE'] = 'NO'
        end
    end
end
```

Mind iOS SDK includes pre-built binaries of [WebRTC library (milestone 111)](https://webrtc.googlesource.com/src/) and the
[script](WebRTC/build.sh) which we used for building it. The library was built without Bitcode, so you should either
disable Bitcode in `Build Settings` of your project, or rebuild the library with Bitcode on your own. 

# API Reference

This is an index of all components (classes, protocols and enumerations) of Mind iOS SDK.

  * [Camera](../5.12.0/Reference/Camera.md)
  * [CameraFacing](../5.12.0/Reference/CameraFacing.md)
  * [Conference](../5.12.0/Reference/Conference.md)
  * [ConferenceLayout](../5.12.0/Reference/ConferenceLayout.md)
  * [DeviceRegistry](../5.12.0/Reference/DeviceRegistry.md)
  * [Me](../5.12.0/Reference/Me.md)
  * [MediaStream](../5.12.0/Reference/MediaStream.md)
  * [MediaStreamAudioConsumer](../5.12.0/reference/MediaStreamAudioConsumer.md)
  * [MediaStreamAudioStatistics](../5.12.0/Reference/MediaStreamAudioStatistics.md)
  * [MediaStreamAudioSupplier](../5.12.0/Reference/MediaStreamAudioSupplier.md)
  * [MediaStreamVideoConsumer](../5.12.0/reference/MediaStreamVideoConsumer.md)
  * [MediaStreamVideoStatistics](../5.12.0/Reference/MediaStreamVideoStatistics.md)
  * [MediaStreamVideoSupplier](../5.12.0/Reference/MediaStreamVideoSupplier.md)
  * [Microphone](../5.12.0/Reference/Microphone.md)
  * [MindSDK](../5.12.0/Reference/MindSDK.md)
  * [MindSDKOptions](../5.12.0/Reference/MindSDKOptions.md)
  * [Participant](../5.12.0/Reference/Participant.md)
  * [ParticipantRole](../5.12.0/Reference/ParticipantRole.md)
  * [Session](../5.12.0/Reference/Session.md)
  * [SessionListener](../5.12.0/Reference/SessionListener.md)
  * [SessionOptions](../5.12.0/reference/SessionOptions.md)
  * [SessionState](../5.12.0/Reference/SessionState.md)
  * [SessionStatistics](../5.12.0/Reference/SessionStatistics.md)
