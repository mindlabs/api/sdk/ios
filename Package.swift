// swift-tools-version:5.5.0
import PackageDescription

let package = Package(
    name: "MindSDK",
    platforms: [
        .iOS(.v10)
    ],
    products: [
        .library(name: "MindSDK", targets: ["MindSDK"])
    ],
    dependencies: [
        .package(url: "https://github.com/google/promises.git", from: "2.4.0")
    ],
    targets: [
        .target(
            name: "MindSDK",
            dependencies: [.product(name: "Promises", package: "promises"), "WebRTC"],
            path: "Sources"
        ),
        .binaryTarget(
            name: "WebRTC",
            path: "WebRTC/WebRTC.xcframework"
        ),
        .testTarget(
            name: "MindSDKTests",
            dependencies: ["MindSDK"],
            path: "Tests"
        )
    ]
)
