import Foundation
import Promises

/**
 * ***
 * Conference class is used for representing a conferences from point of view of the
 * [local participant](x-source-tag://Me). You can get a representation of the conference with
 * [getConference](x-source-tag://Session.getConference) method of [Session](x-source-tag://Session) class. Conference
 * class contains methods for getting and setting parameters of the conference, for
 * [starting](x-source-tag://Conference.startRecording) and [stopping](x-source-tag://Conference.stopRecording)
 * conference recording, for getting [local](x-source-tag://Conference.getMe) and
 * [remote](x-source-tag://Conference.getParticipants) participants, and for getting
 * [conference media stream](x-source-tag://Conference.getMediaStream):
 *
 * ```
 * @IBOutlet var conferenceVideo: Video!
 *
 * ...
 * let conference = session.getConference()
 * let conferenceStream = conference.getMediaStream()
 * conferenceVideo.setMediaStream(conferenceStream)
 * ```
 *
 * Thread-Safety: all methods of Conference class can be called on the main thread only. Calling these methods on
 * another thread will result in an `Error` or undefined behavior.
 *
 *  - Tag: Conference
 */
public class Conference: Hashable {

    static func fromDTO(_ session: Session, _ dto: [String: Any]) -> Conference {
        let participantDTOs = dto["participants"] as! [[String: Any]]
        let me = Me.fromDTO(session, participantDTOs[0])
        var participants = [Participant]()
        for i in 1..<participantDTOs.count {
            let participantDTO = participantDTOs[i]
            if (participantDTO["online"] as! Bool) {
                let participant = Participant.fromDTO(session, participantDTO)
                participants.append(participant)
            }
        }
        return Conference(session, dto["id"] as! String, dto["name"] as! String, ConferenceLayout.fromString(dto["layout"] as! String), (dto["recording"] as! [String: Any])["started"] as! Bool, me, participants)
    }

    // Thread-Safety: all methods of `session` except `executeOnMainThread` are accessed on the main thread only
    private let session: Session

    private var id: String
    private var name: String
    private var layout: ConferenceLayout
    private var recordingStarted: Bool
    private var me: Me
    private var participants: [Participant]
    private let mediaStream: MediaStream
    private var index: [String: Participant]

    private init(_ session: Session, _ id: String, _ name: String, _ layout: ConferenceLayout, _ recordingStarted: Bool, _ me: Me, _ participants: [Participant]) {
        self.session = session
        self.id = id
        self.name = name
        self.layout = layout
        self.recordingStarted = recordingStarted
        self.me = me
        self.participants = participants
        self.mediaStream = MediaStream("conference", session.getWebRtcConnection(), session.getWebRtcConnection())
        self.index = [String: Participant]()
        self.index[me.getId()] = me
        for participant in participants {
            self.index[participant.getId()] = participant
        }
    }

    /**
     * Returns the ID of the conference. The ID is unique and never changes.
     *
     * - Returns: The ID of the conference.
     *
     * - Tag: Conference.getId
     */
    public func getId() -> String {
        return id
    }

    /**
     * Returns the current name of the conference. The name of the conference can be shown above the video in the
     * conference media stream and recording.
     *
     * - Returns: The current name of the conference.
     *
     * - Tag: Conference.getName
     */
    public func getName() -> String {
        return name
    }

    /**
     * Returns the current [layout](x-source-tag://ConferenceLayout) of the conference. The layout determines
     * arrangement of videos in [conference media stream](x-source-tag://Conference.getMediaStream)  which the
     * participants receive, and [recording](x-source-tag://Conference.getRecordingURL).
     *
     * - Returns: The current layout of the conference.
     *
     * - Tag: Conference.getLayout
     */
    public func getLayout() -> ConferenceLayout {
        return layout
    }

    /**
     * Changes the name of the conference. The name of the conference can be shown above the video in the conference
     * media stream and recording. The name changing is an asynchronous operation, that's why this method returns a
     * `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error` (if the
     * operation fails). The operation can succeed only if the [local participant](x-source-tag://Me) plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter name: The new name for the conference.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Conference.setName
     */
    @discardableResult public func setName(_ name: String) -> Promise<Void> {
        let requestDTO = [ "name":  name ]
        return session.newHttpPatch("/", requestDTO).then({ responseDTO in
            self.name = name
        })
    }

    /**
     * Returns the [local participant](x-source-tag://Me).
     *
     * - Returns: The local participant.
     *
     * - Tag: Conference.getMe
     */
    public func getMe() -> Me {
        return me
    }

    /**
     * Returns the list of all online [remote participants](x-source-tag://Participant).
     *
     * - Returns: The list of all online remote participants.
     *
     * - Tag: Conference.getParticipants
     */
    public func getParticipants() -> [Participant]  {
        return participants
    }

    /**
     * Returns [remote participant](x-source-tag://Participant) with the specified ID or `nil` value, if it doesn't
     * exist or if it is offline.
     *
     * - Returns: The remote participant or `nil` value, if it doesn't exist or if it is offline.
     *
     * - Tag: Conference.getParticipantById
     */
    public func getParticipantById(_ participantId: String) -> Participant? {
        return index[participantId]
    }

    /**
     * Checks whether the conference is being recorded or not.
     *
     * - Returns: The boolean value which indicates if the conference is being recorded or not.
     *
     * - Tag: Conference.isRecording
     */
    public func isRecording() -> Bool {
        return recordingStarted
    }

    /**
     * Starts recording of the conference. This is an asynchronous operation, that's why this method returns a `Promise`
     * that either resolves with no value (if the operation succeeds) or rejects with an `Error` (if the operation
     * fails). The operation can succeed only if the [local participant](x-source-tag://Me) plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Conference.startRecording
     */
    @discardableResult public func startRecording() -> Promise<Void> {
        let requestDTO: [String: Any] = [:]
        return session.newHttpPost("/recording/start", requestDTO).then({ responseDTO in
            self.recordingStarted = true
        })
    }

    /**
     * Stops recording of the conference. This is an asynchronous operation, that's why this method returns a `Promise`
     * that either resolves with no value (if the operation succeeds) or rejects with an `Error` (if the operation
     * fails). The operation can succeed only if the [local participant](x-source-tag://Me) plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Conference.stopRecording
     */
    @discardableResult public func stopRecording() -> Promise<Void> {
        let requestDTO: [String: Any] = [:]
        return session.newHttpPost("/recording/stop", requestDTO).then({ responseDTO in
            self.recordingStarted = false
        })
    }

    /**
     * Returns a URL for downloading the recording of the conference. The returned URL can be used for downloading only
     * if the [local participant](x-source-tag://Me) plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Returns: The URL for downloading the recording of the conference.
     *
     * - Tag: Conference.getRecordingURL
     */
    public func getRecordingURL() -> String {
        return session.getRecordingURL()
    }

    /**
     * Returns the [media stream](x-source-tag://MediaStream) of the conference. The returned media stream is a mix of
     * all audios and videos (excluding only audio of the local participant) that participants are streaming at the
     * moment. The videos in the media stream are arranged using [current layout](x-source-tag://Conference.getLayout)
     * of the conference. You can get and play the media stream of the conference at any time.
     *
     * - Returns: The media steam of the conference.
     *
     * - Tag: Conference.getMediaStream
     */
    public func getMediaStream() -> MediaStream {
        return mediaStream
    }

    func addParticipant(_ participant: Participant) {
        if (index[participant.getId()] == nil) {
            participants.append(participant)
            index[participant.getId()] = participant
            session.fireOnParticipantJoined(participant)
        }
    }

    func removeParticipant(_ participant: Participant) {
        if (index[participant.getId()] != nil) {
            index.removeValue(forKey: participant.getId())
            participants.remove(at: participants.firstIndex(where: {$0 === participant})!)
            session.fireOnParticipantExited(participant)
        }
    }

    func update(_ dto: [String: Any]) {
        if (name != (dto["name"] as! String)) {
            name = dto["name"] as! String
            session.fireOnConferenceNameChanged(self)
        }
        let recording = dto["recording"] as! [String: Any]
        if (recordingStarted != recording["started"] as! Bool) {
            recordingStarted = recording["started"] as! Bool
            if (recordingStarted) {
                session.fireOnConferenceRecordingStarted(self)
            } else {
                session.fireOnConferenceRecordingStopped(self)
            }
        }
    }

    func destroy() {
        me.destroy()
        for participant in participants {
            participant.destroy()
        }
        mediaStream.setAudioSupplier(nil)
        mediaStream.setVideoSupplier(nil)
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    public static func == (lhs: Conference, rhs: Conference) -> Bool {
        return lhs === rhs
    }

}
