import Promises
import WebRTC

/**
 * ***
 * MindSDK class is the entry point of Mind iOS SDK. It contains static methods for
 * [joining](x-source-tag://MindSDK.join) and [leaving](x-source-tag://MindSDK.exit2) conferences, for getting
 * [device registry](x-source-tag://MindSDK.getDeviceRegistry), and for
 * [creating local media streams](x-source-tag://MindSDK.createMediaStream). But before you can do all this, the SDK
 * should be initialized:
 *
 * ```
 * @UIApplicationMain
 * class AppDelegate: UIResponder, UIApplicationDelegate {
 *
 *     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
 *         let options = Options()
 *         MindSDK.initialize(options)
 *         return true
 *     }
 *
 * }
 * ```
 *
 * Thread-Safety: all methods of MindSDK class can be called on the main thread only. Calling these methods on another
 * thread will result in an `Error` or undefined behavior.
 *
 * - Tag: MindSDK
 */
public class MindSDK {

    public static let VERSION: String = "5.13.0-SNAPSHOT"

    private static var options: MindSDKOptions!
    private static var peerConnectionFactory: RTCPeerConnectionFactory!
    private static var deviceRegistry: DeviceRegistry!
    private static var audioCodecs: [RTCRtpCodecCapability]!
    private static var videoCodecs: [RTCRtpCodecCapability]!

    private init() {}

    /**
     * Initializes Mind iOS SDK with the specified [configuration options](x-source-tag://MindSDKOptions). The
     * initialization should be completed only once before calling any other method of the MindSDK class.
     *
     * - Parameter options: The configuration options for Mind iOS SDK.
     *
     * - Tag: MindSDK.initialize
     */
    public static func initialize(_ options: MindSDKOptions) {
        MindSDK.options = MindSDKOptions(options)
        if (peerConnectionFactory == nil) {
            RTCInitFieldTrialDictionary(["WebRTC-LegacySimulcastLayerLimit": "Disabled" ])
            RTCInitializeSSL()
            peerConnectionFactory = RTCPeerConnectionFactory(encoderFactory: RTCDefaultVideoEncoderFactory(), decoderFactory: RTCDefaultVideoDecoderFactory())
            let peerConnnectionFactoryOptions = RTCPeerConnectionFactoryOptions()
            peerConnnectionFactoryOptions.ignoreLoopbackNetworkAdapter = true
            peerConnectionFactory.setOptions(peerConnnectionFactoryOptions)
        }
        if (audioCodecs == nil) {
            audioCodecs = peerConnectionFactory.rtpSenderCapabilities(forKind: "audio").codecs.filter({
                $0.mimeType.range(of: "audio/(G722|ILBC|PCMU|PCMA|CN|telephone-event)", options: .regularExpression) == nil
            }).sorted(by: {
                // WebRTC library doesn't activate RED for Opus by default, that's why we have to activate it
                // explicitly through the `setCodecPreferences` (see `WebRtcConnection` class) by changing the
                // order of codecs such that the RED payload appears before the Opus payload in the offer SDP.
                $0.mimeType == "audio/red" && $1.mimeType != "audio/red"
            })
        }
        if (videoCodecs == nil) {
            videoCodecs = peerConnectionFactory.rtpSenderCapabilities(forKind: "video").codecs.filter({
                $0.mimeType.range(of: "video/(H264|AV1)", options: .regularExpression) == nil
            })
        }
        if (deviceRegistry == nil) {
            deviceRegistry = DeviceRegistry(peerConnectionFactory)
        }
    }

    @available(*, deprecated, message: "Use MindSDK.initialize(Options) instead")
    public static func initialize() {
        print("MindSDK: MindSDK.initialize() is deprecated and will be removed in the Mind iOS SDK 6.0.0")
        MindSDK.initialize(MindSDKOptions())
    }

    /**
     * Returns the [device registry](x-source-tag://DeviceRegistry) which provides access to all audio and video
     * peripherals of the iOS device.
     *
     * - Returns: The device registry.
     *
     * - Tag: MindSDK.getDeviceRegistry
     */
    public static func getDeviceRegistry() -> DeviceRegistry {
        return deviceRegistry
    }

    @available(*, deprecated, message: "Use DeviceRegistry.getMicrophone() instead")
    public static func getMicrophone() -> Microphone {
        print("MindSDK: MindSDK.getMicrophone() is deprecated and will be removed in the Mind iOS SDK 6.0.0")
        return deviceRegistry.getMicrophone()
    }

    @available(*, deprecated, message: "Use DeviceRegistry.getCamera() instead")
    public static func getCamera() -> Camera {
        print("MindSDK: MindSDK.getCamera() is deprecated and will be removed in the Mind iOS SDK 6.0.0")
        return deviceRegistry.getCamera()
    }

    /**
     * Creates [local media stream](x-source-tag://MediaStream) with audio and video from the specified suppliers. The
     * `nil` value can be passed instead of one of the suppliers to create audio-only or video-only media stream. Even
     * if audio/video supplier wasn't `nil` it doesn't mean that the result media stream would automatically contain
     * audio/video, e.g. [Microphone](x-source-tag://Microphone) (as a supplier of audio) and
     * [Camera](x-source-tag://Camera) (as a supplier of video) supply no audio and no video, respectively, till they
     * are acquired, and after they were released.
     *
     * - Parameter audioSupplier: The audio supplier or `nil` to create video-only media stream.
     * - Parameter videoSupplier: The video supplier or `nil` to create audio-only media stream.
     *
     * - Returns: The created media stream.
     *
     * - Tag: MindSDK.createMediaStream
     */
    public static func createMediaStream(_ audioSupplier: MediaStreamAudioSupplier!, _ videoSupplier: MediaStreamVideoSupplier!) -> MediaStream {
        if (audioSupplier == nil && videoSupplier == nil) {
            preconditionFailure("Can't create MediaStream of `nil` audio supplier and `nil` video supplier")
        }
        return MediaStream("local", audioSupplier, videoSupplier)
    }

    /**
     * Establishes a participation session (aka joins the conference) on behalf of the participant with the specified
     * token. The establishment is an asynchronous operation, that's why this method returns a `Promise` that either
     * resolves with a [participation session](x-source-tag://Session) (if the operation succeeded) or rejects with an
     * `Error` (if the operation failed).
     *
     * - Parameter uri: The URI of the conference.
     * - Parameter token: The token of the participant on behalf of whom we are joining the conference.
     * - Parameter options: The configuration options for the participation session.
     *
     * - Returns: The promise that either resolves with a a participation session or rejects with an `Error`.
     *
     * - Tag: MindSDK.join
     */
    public static func join(_ uri: String, _ token: String, _ options: SessionOptions) -> Promise<Session> {
        if (uri.range(of: "^https?://[^/]+/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/?$", options: .regularExpression) == nil) {
            preconditionFailure("Conference URI is malformed")
        }
        return Session(uri, token, nil, options).open()
    }

    @available(*, deprecated, message: "Use MindSDK.join instead")
    public static func join2(_ uri: String, _ token: String, _ listener: SessionListener!) -> Promise<Session> {
        print("MindSDK: MindSDK.join2 is deprecated and will be removed in the Mind iOS SDK 6.0.0")
        if (uri.range(of: "^https?://[^/]+/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/?$", options: .regularExpression) == nil) {
            preconditionFailure("Conference URI is malformed")
        }
        let options = SessionOptions();
        options.setUseVp9ForSendingVideo(nil);
        return Session(uri, token, listener, options).open()
    }

    /**
     * Terminates an [established participation session](x-source-tag://MindSDK.join) (aka leaves the conference). The
     * termination is an idempotent synchronous operation. The session object itself and all other objects related to
     * the session are not functional after the leaving.
     *
     * - Parameter session: The participation session which should be terminated.
     *
     * - Tag: MindSDK.exit2
     */
    public static func exit2(_ session: Session) {
        session.close()
    }

    static func getAudioCodecs() -> [RTCRtpCodecCapability]! {
        return audioCodecs
    }

    static func getVideoCodecs() -> [RTCRtpCodecCapability]! {
        return videoCodecs
    }

    static func getOptions() -> MindSDKOptions {
        return MindSDK.options
    }

    static func createPeerConnection(_ iceServers: [RTCIceServer]) -> RTCPeerConnection {
        let configuration = RTCConfiguration()
        configuration.iceServers = iceServers
        configuration.sdpSemantics = RTCSdpSemantics.unifiedPlan
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let pc = peerConnectionFactory.peerConnection(with: configuration, constraints: constraints, delegate: nil)!
        pc.delegate = pc
        return pc
    }

}

extension RTCPeerConnection: RTCPeerConnectionDelegate {

    private struct Keys {
        static var onSignalingStateChange: UInt8 = 0
        static var onConnectionStateChange: UInt8 = 0
        static var onIceConnectionStateChange: UInt8 = 0
        static var onIceGatheringStateChange: UInt8 = 0
        static var onIceCandidate: UInt8 = 0
        static var onIceCandidatesRemoved: UInt8 = 0
        static var onAddStream: UInt8 = 0
        static var onRemoveStream: UInt8 = 0
        static var onDataChannel: UInt8 = 0
        static var onRenegotiationNeeded: UInt8 = 0
        static var onTrack: UInt8 = 0
    }

    var onSignalingStateChange: ((RTCSignalingState) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onSignalingStateChange) as? ((RTCSignalingState) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onSignalingStateChange, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onConnectionStateChange: ((RTCPeerConnectionState) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onConnectionStateChange) as? ((RTCPeerConnectionState) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onConnectionStateChange, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onIceConnectionStateChange: ((RTCIceConnectionState) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onIceConnectionStateChange) as? ((RTCIceConnectionState) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onIceConnectionStateChange, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onIceGatheringStateChange: ((RTCIceGatheringState) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onIceGatheringStateChange) as? ((RTCIceGatheringState) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onIceGatheringStateChange, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onIceCandidate: ((RTCIceCandidate) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onIceCandidate) as? ((RTCIceCandidate) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onIceCandidate, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onIceCandidatesRemoved: (([RTCIceCandidate]) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onIceCandidatesRemoved) as? (([RTCIceCandidate]) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onIceCandidatesRemoved, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onAddStream: ((RTCMediaStream) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onAddStream) as? ((RTCMediaStream) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onAddStream, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onRemoveStream: ((RTCMediaStream) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onRemoveStream) as? ((RTCMediaStream) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onRemoveStream, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onDataChannel: ((RTCDataChannel) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onDataChannel) as? ((RTCDataChannel) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onDataChannel, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onRenegotiationNeeded: (() -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onRenegotiationNeeded) as? (() -> Void) else {
                return {}
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onRenegotiationNeeded, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onTrack: ((RTCRtpReceiver, [RTCMediaStream]) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onTrack) as? ((RTCRtpReceiver, [RTCMediaStream]) -> Void) else {
                return { _, _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onTrack, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        onSignalingStateChange(stateChanged)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        onAddStream(stream)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        onRemoveStream(stream)
    }

    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        onRenegotiationNeeded()
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        onIceConnectionStateChange(newState)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        onIceGatheringStateChange(newState)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        onIceCandidate(candidate)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        onIceCandidatesRemoved(candidates)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        onDataChannel(dataChannel)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd rtpReceiver: RTCRtpReceiver, streams mediaStreams: [RTCMediaStream]) {
        onTrack(rtpReceiver, mediaStreams)
    }

    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCPeerConnectionState) {
        onConnectionStateChange(newState)
    }

    public func createDataChannel(_ label: String, _ configuration: RTCDataChannelConfiguration) -> RTCDataChannel {
        let dataChannel = dataChannel(forLabel: label, configuration: configuration)!
        dataChannel.delegate = dataChannel
        return dataChannel
    }

}

extension RTCDataChannel: RTCDataChannelDelegate {

    private struct Keys {
        static var onStateChange: UInt8 = 0
        static var onMessage: UInt8 = 0
        static var onBufferedAmountChange: UInt8 = 0
    }

    var onStateChange: ((RTCDataChannelState) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onStateChange) as? ((RTCDataChannelState) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onStateChange, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onMessage: ((RTCDataBuffer) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onMessage) as? ((RTCDataBuffer) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onMessage, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var onBufferedAmountChange: ((UInt64) -> Void) {
        get {
            guard let value = objc_getAssociatedObject(self, &Keys.onBufferedAmountChange) as? ((UInt64) -> Void) else {
                return { _ in }
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &Keys.onBufferedAmountChange, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    public func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        onStateChange(dataChannel.readyState)
    }

    public func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        onMessage(buffer)
    }

    public func dataChannel(_ dataChannel: RTCDataChannel, didChangeBufferedAmount amount: UInt64) {
        onBufferedAmountChange(amount)
    }

}
