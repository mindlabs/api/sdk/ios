/**
 * ***
 * SessionState enumerates all available states that a [participation session](x-source-tag://Session) can be in. At
 * any given moment the participation session can be in one of three states: [NORMAL](x-source-tag://NORMAL),
 * [LAGGING](x-source-tag://LAGGING) or [FAILED](x-source-tag://FAILED). The current state of the participation session
 * can be got with [getState](x-source-tag://getState) method of [Session](x-source-tag://Session) class. The
 * [onSessionStateChanged](x-source-tag://SessionListener.onSessionStateChanged) method of the
 * [session listener](x-source-tag://SessionListener) is called whenever the state of the participation session changes.
 *
 * - Tag: ParticipantRole
 */
public enum SessionState: String, CustomStringConvertible {

    /**
     * The `NORMAL` state means that the communication channel with Mind API is established, and it is operating
     * normally: all video and audio which should have been transmitted over the channel are actually transmitted.
     *
     * - Tag: SessionState.NORMAL
     */
    case NORMAL = "normal"

    /**
     * The `LAGGING` state means that the communication channel with Mind API is established, but it is lagging behind:
     * not all video and audio which should have been transmitted over the channel, are actually transmitted. Any
     * [participation session](x-source-tag://Session) which has gone into the `LAGGING` state, immediately starts
     * automatic recovering in order to return to the `NORMAL` state as soon as possible.
     *
     * - Tag: SessionState.LAGGING
     */
    case LAGGING = "lagging"

    /**
     * The `FAILED` state means that the communication channel with Mind API has failed: none of the video and audio
     * which should have been transmitted over the channel, are actually transmitted. Any
     * [participation session](x-source-tag://Session) which has gone into the `FAILED` state, immediately starts
     * automatic recovering in order to return to the `NORMAL` state as soon as possible, but for the time of the
     * recovering the participant may become "offline" for the server part of your application and other participants.
     *
     * - Tag: SessionState.FAILED
     */
    case FAILED = "failed"

    public var description: String {
        return rawValue
    }

}
