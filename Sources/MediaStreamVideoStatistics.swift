import Foundation

/**
 * ***
 * MediaStreamVideoStatistics class is used for representing video statistics of a [media
 * stream](x-source-tag://MediaStream). The statistics consists of instant measures of the network connection which is
 * currently used for transmitting the video. If no video is transmitted at the moment or if the media stream does not
 * have a video at all or yet, the values of all measures are reset to zeros, otherwise the values of all measures are
 * updated about once a second. You can always get the latest video statistics for any media stream with a help of
 * [getVideoStatistics](x-source-tag://MediaStream.getVideoStatistics) method:
 *
 * ```
 * // We assume that microphone and camera have been already acquired
 * let myStream = MindSDK.createMediaStream(microphone, camera)
 * me.setMediaStream(myStream)
 * let myStreamVideoStatistics = myStream.getVideoStatistics()
 *
 * ...
 *
 * @IBOutlet var participantVideo: Video!
 *
 * if let participant = conference.getParticipantById("<PARTICIPANT_ID>") {
 *     let participantStream = participant.getMediaStream()
 *     participantVideo.setMediaStream(participantStream)
 *     let participantStreamVideoStatistics = participantStream.getVideoStatistics()
 * }
 *
 * ...
 *
 * @IBOutlet var conferenceVideo: Video!
 *
 * let conferenceStream = conference.getMediaStream()
 * conferenceVideo.setMediaStream(conferenceStream)
 * let conferenceStreamVideoStatistics = conferenceStream.getVideoStatistics()
 * ```
 *
 * Thread-Safety: all methods of MediaStreamVideoStatistics class can be called on any thread.
 *
 *  - Tag: MediaStreamVideoStatistics
 */
public class MediaStreamVideoStatistics: CustomStringConvertible {

    private let timestamp: Int64
    private let rtt: Int32
    private let bitrate: Int32
    private let delay: Int32
    private let losses: Int32

    private let width: Int32
    private let height: Int32
    private let rate: Int32

    init(_ rtt: Int32, _ bitrate: Double, _ delay: Double, _ losses: Double, _ width: Int32, _ height: Int32, _ rate: Double) {
        self.timestamp = Int64(Date().timeIntervalSince1970 * 1000)
        self.rtt = rtt
        self.bitrate = MediaStreamVideoStatistics.integerify(bitrate)
        self.delay = MediaStreamVideoStatistics.integerify(delay)
        self.losses = MediaStreamVideoStatistics.integerify(losses)
        self.width = width
        self.height = height
        self.rate = MediaStreamVideoStatistics.integerify(rate)
    }

    /**
     * Returns the creation timestamp of the statistics. The creation timestamp of the statistics is the number of
     * milliseconds that have elapsed between 1 January 1970 00:00:00 UTC and the time at which the statistics was
     * created.
     *
     * - Returns: The creation timestamp of the statistics.
     *
     * - Tag: MediaStreamVideoStatistics.getTimestamp
     */
    public func getTimestamp() -> Int64 {
        return timestamp
    }

    /**
     * Returns the instant round-trip time of the video transmission. The instant round-trip time of the video
     * transmission is an average sum of transmission delays (in milliseconds) of the network connection in both
     * directions (from the iOS device to Mind API and vice versa) during the latest observed second.
     *
     * - Returns: The instant round-trip time of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getRtt
     */
    public func getRtt() -> Int32 {
        return rtt
    }

    /**
     * Returns the instant bitrate of the video transmission. The instant bitrate of the video transmission is a size
     * (in bits) of all video packets that have been transmitted over the network connection during the latest observed
     * second.
     *
     * - Returns: The instant bitrate of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getBitrate
     */
    public func getBitrate() -> Int32 {
        return bitrate
    }

    /**
     * Returns the instant delay of the video transmission. The instant delay of the video transmission is an average
     * number of milliseconds that video frames have spent buffered locally (on the iOS device) during the latest
     * observed second before (in case of sending a local media stream) or after (in case of receiving a remote media
     * stream) being transmitted over the network connection.
     *
     * - Returns: The instant delay of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getDelay
     */
    public func getDelay() -> Int32 {
        return delay
    }

    /**
     * Returns the instant loss rate of the video transmission. The instant loss rate of the video transmission is a
     * percentage (i.e. an integer between 0 and 100 inclusively) of video packets that have been sent over the network
     * connection during the latest observed second but have not been received (i.e. have been lost).
     *
     * - Returns: The instant loss rate of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getLosses
     */
    public func getLosses() -> Int32 {
        return losses
    }

    /**
     * Returns the instant frame width of the video transmission. The instant frame width of the video transmission is
     * a width (in pixels) of the last frame that have been successfully transmitted over the network connection during
     * the latest observed second.
     *
     * - Returns: The instant frame width of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getWidth
     */
    public func getWidth() -> Int32 {
        return width
    }

    /**
     * Returns the instant frame height of the video transmission. The instant frame height of the video transmission
     * is a height (in pixels) of the last frame that have been successfully transmitted over the network connection
     * during the latest observed second.
     *
     * - Returns: The instant frame height of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getHeight
     */
    public func getHeight() -> Int32 {
        return height
    }

    /**
     * Returns the instant frame rate of the video transmission. The instant frame rate of the video transmission is
     * a number of video frames that have been successfully transmitted over the network connection during the latest
     * observed second.
     *
     * - Returns: The instant frame rate of the video transmission.
     *
     * - Tag: MediaStreamVideoStatistics.getRate
     */
    public func getRate() -> Int32 {
        return rate
    }

    public var description: String {
        return """
               {
                 timestamp: \(timestamp),
                 rtt: \(rtt)ms,
                 bitrate: \(bitrate / 1024)kbit/s,
                 delay: \(delay)ms,
                 losses: \(losses)%,
                 width: \(width)px,
                 height: \(height)px,
                 rate: \(rate)fps
               }
               """.replacingOccurrences(of: "\"", with: "")
    }

    private static func integerify(_ value: Double) -> Int32 {
        return value.isNaN || value.isInfinite ? 0 : Int32(value)
    }

}
