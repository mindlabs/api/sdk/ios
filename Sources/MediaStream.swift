import WebRTC

/**
 * ***
 * MediaStream class is used for representing audio/video content which participants can send and receive. It can
 * contain one audio and one video only. There are two types of media streams: local and remote. The distinction is
 * nominal — still there is only one class to represent both of them.
 *
 * Local media streams are created explicitly with a help of
 * [createMediaStream](x-source-tag://MindSDK.createMediaStream) method of [MindSDK](x-source-tag://MindSDK) class.
 * They can contain audio/video from local suppliers only (such as [Microphone](x-source-tag://Microphone) and
 * [Camera](x-source-tag://Camera)). Local media streams are intended to be
 * [streamed on behalf of the local participant](x-source-tag://Me.setMediaStream) only.
 *
 * Remote media streams contain audio/video of remote participants or the conference. The primary and the secondary
 * media streams of any remote participant
 * can be got with help of [getMediaStream](x-source-tag://Participant.getMediaStream) or
 * [getSecondaryMediaStream](x-source-tag://Participant.getSecondaryMediaStream) methods of
 * [Participant](x-source-tag://Participant) class, respectively. The media stream of the conference can be got with
 * [getMediaStream](x-source-tag://Conference.getMediaStream) method of [Conference](x-source-tag://Conference) class.
 *
 * Any media stream can be played with [Video](x-source-tag://Video) or [Audio](x-source-tag://Audio) classes. Keep in
 * mind that [Video](x-source-tag://Video) tries to play (tries to consume) audio and video of the media stream,
 * whereas [Audio](x-source-tag://Audio) tries to play (tries to consume) audio only of the media stream. This means
 * that if you want to play only audio of a remote media stream, it is better to play it with
 * [Audio](x-source-tag://Audio) instead of playing it with invisible [Video](x-source-tag://Video) because the latter
 * will force underlying WebRTC connection to receive and decode both audio and video.
 *
 * ```
 * @IBOutlet var myVideo: Video!
 * @IBOutlet var participantVideo: Video!
 *
 * ...
 *
 * let me = conference.getMe()
 *
 * // We assume that microphone and camera have been already acquired or will be acquired later
 * let myStream = MindSDK.createMediaStream(microphone, camera)
 * me.setMediaStream(myStream)
 * self.myVideo.setMediaStream(myStream)
 *
 * if let participant = conference.getParticipantById("<PARTICIPANT_ID>") {
 *     self.participantVideo.setMediaStream(participant.getMediaStream())
 * }
 *
 * ...
 *
 * let conferenceAudio = Audio()
 * conferenceAudio.setMediaStream(conference.getMediaStream())
 * ```
 *
 * Thread-Safety: all methods of MediaStream class can be called on the main thread only. Calling these methods on
 * another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: MediaStream
 */
public class MediaStream: Hashable {

    private var audioConsumers = [AnyHashable: Double]()
    private var videoConsumers = Set<AnyHashable>()

    private let label: String
    private var audioSupplier: MediaStreamAudioSupplier!
    private var videoSupplier: MediaStreamVideoSupplier!

    private var audioStatistics: MediaStreamAudioStatistics
    private var videoStatistics: MediaStreamVideoStatistics

    private var audioBuffer: MediaStreamAudioBuffer!
    private var videoBuffer: MediaStreamVideoBuffer!

    private var maxVideoFrameArea: Int32
    private var maxVideoFrameRate: Int32
    private var maxVideoBitrate: Int32

    private var oldStatistics: [String: Int64]!

    init(_ label: String, _ audioSupplier: MediaStreamAudioSupplier!, _ videoSupplier: MediaStreamVideoSupplier!) {
        self.label = label
        self.audioSupplier = audioSupplier
        self.videoSupplier = videoSupplier
        self.audioStatistics = MediaStreamAudioStatistics(0, 0, 0, 0, 0, 0)
        self.videoStatistics = MediaStreamVideoStatistics(0, 0, 0, 0, 0, 0, 0)
        self.audioBuffer = nil
        self.videoBuffer = nil
        self.oldStatistics = nil
        self.maxVideoFrameArea = Int32.max
        self.maxVideoFrameRate = Int32.max
        self.maxVideoBitrate = Int32.max
    }

    /**
     * Returns the current maximum video frame area of the media stream. The maximum video frame area of the media
     * stream is a product of the width and height which the video should not exceed while being transmitting over the
     * network.
     *
     * - Returns: The current maximum video frame area of the media stream.
     *
     * - Tag: MediaStream.getMaxVideoFrameArea
     */
    public func getMaxVideoFrameArea() -> Int32 {
        return maxVideoFrameArea
    }

    /**
     * Sets the maximum video frame area of the media stream. The maximum video frame area of the media stream is a
     * product of the width and height which the video should not exceed while being transmitting over the network.
     * This method can be used for limiting the video frame area of any media stream, but the limiting effect depends
     * on the type of the media stream: setting the maximum video frame area for a local media stream (the video of
     * which usually consist of multiple encodings) leads to filtering out from the transmission all the encodings with
     * the higher video frame areas; setting the maximum video frame area for a remote media stream (the video of which
     * always consists of only one encoding) leads to decreasing the frame area of the transmitted video to make it fit
     * into the limit. Though this method cannot be used for stopping the transmission of the stream's video completely
     * — if the limit cannot be satisfied, the encoding with the lowest video frame area will be transmitted anyway. By
     * default, the maximum video frame area of any media stream is unlimited.
     *
     * - Parameter maxVideoFrameArea: The maximum video frame area of the media stream.
     *
     * - Tag: MediaStream.setMaxVideoFrameArea
     */
    public func setMaxVideoFrameArea(_ maxVideoFrameArea: Int32) {
        if (maxVideoFrameArea <= 0) {
            preconditionFailure("Can't change max video frame area to `\(maxVideoFrameArea)`")
        }
        if (self.maxVideoFrameArea != maxVideoFrameArea) {
            self.maxVideoFrameArea = maxVideoFrameArea
            if (videoConsumers.count > 0 && videoSupplier != nil) {
                videoSupplier.addVideoConsumer(self)
            }
        }
    }

    /**
     * Returns the current maximum video frame rate of the media stream. The maximum video frame rate of the media
     * stream is a frame rate which the video should not exceed while being transmitting over the network.
     *
     * - Returns: The current maximum video frame rate of the media stream.
     *
     * - Tag: MediaStream.getMaxVideoFrameRate
     */
    public func getMaxVideoFrameRate() -> Int32 {
        return maxVideoFrameRate
    }

    /**
     * Sets the maximum video frame rate of the media stream. The maximum video frame rate of the media stream is a
     * frame rate which the video should not exceed while being transmitting over the network. This method can be used
     * for limiting the video frame rate of any media stream, but the limiting effect depends on the type of the media
     * stream: setting the maximum video frame rate for a local media stream (the video of which usually consist of
     * multiple encodings) leads to filtering out from the transmission all the encodings with the higher video frame
     * rates; setting the maximum video frame rate for a remote media stream (the video of which always consists of
     * only one encoding) leads to decreasing the frame rate of the transmitted video to make it fit into the limit.
     * Though this method cannot be used for stopping the transmission of the stream's video completely — if the limit
     * cannot be satisfied, the encoding with the lowest video frame rate will be transmitted anyway. By default, the
     * maximum video frame rate of any media stream is unlimited.
     *
     * - Parameter maxVideoFrameRate: The maximum video frame rate of the media stream.
     *
     * - Tag: MediaStream.setMaxVideoFrameRate
     */
    public func setMaxVideoFrameRate(_ maxVideoFrameRate: Int32) {
        if (maxVideoFrameRate <= 0) {
            preconditionFailure("Can't change max video frame rate to `\(maxVideoFrameRate)`")
        }
        if (self.maxVideoFrameRate != maxVideoFrameRate) {
            self.maxVideoFrameRate = maxVideoFrameRate
            if (videoConsumers.count > 0 && videoSupplier != nil) {
                videoSupplier.addVideoConsumer(self)
            }
        }
    }

    /**
     * Returns the current maximum video bitrate of the media stream. The maximum video bitrate of the media stream is
     * a number of bits which each second of stream's video should not exceed while being transmitting over the network.
     *
     * - Returns: The current maximum video bitrate of the media stream.
     *
     * - Tag: MediaStream.getMaxVideoBitrate
     */
    public func getMaxVideoBitrate() -> Int32 {
        return maxVideoBitrate
    }

    /**
     * Sets the maximum video bitrate of the media stream. The maximum video bitrate of the media stream is a number of
     * bits which each second of stream's video should not exceed while being transmitting over the network. This
     * method can be used for limiting the maximum video bitrate of any media stream, but the limiting effect depends
     * on the type of the media stream: setting the maximum video bitrate for a local media stream (the video of which
     * usually consist of multiple encodings) leads to filtering out (from the transmission) all the encodings which do
     * not fit into the limit (starting from the most voluminous one); setting the maximum video bitrate for a remote
     * media stream (the video of which always consists of only one encoding) leads to decreasing the maximum quality
     * of the transmitted video to make it fit into the limit. Though, this method cannot be used for stopping the
     * transmission of the stream's video completely — if the limit cannot be satisfied, the video in the least
     * voluminous encoding (in case of local stream) or with the poorest quality (in case of remote stream) will be
     * transmitted anyway. By default, the maximum video bitrate of any media stream is unlimited.
     *
     * - Parameter maxVideoBitrate: The maximum video bitrate of the media stream.
     *
     * - Tag: MediaStream.setMaxVideoBitrate
     */
    public func setMaxVideoBitrate(_ maxVideoBitrate: Int32) {
        if (maxVideoBitrate <= 0) {
            preconditionFailure("Can't change max video bitrate to `\(maxVideoBitrate)`")
        }
        if (self.maxVideoBitrate != maxVideoBitrate) {
            self.maxVideoBitrate = maxVideoBitrate
            if (videoConsumers.count > 0 && videoSupplier != nil) {
                videoSupplier.addVideoConsumer(self)
            }
        }
    }

    /**
     * Returns the latest [audio statistics](x-source-tag://MediaStreamAudioStatistics) of the media stream. The
     * statistics consists of instant measures of the network connection which is currently used for transmitting the
     * audio of the media stream.
     *
     * - Returns: The latest audio statistics of the media stream.
     *
     * - Tag: MediaStream.getAudioStatistics
     */
    public func getAudioStatistics() -> MediaStreamAudioStatistics {
        return audioStatistics
    }

    /**
     * Returns the latest [video statistics](x-source-tag://MediaStreamVideoStatistics) of the media stream. The
     * statistics consists of instant measures of the network connection which is currently used for transmitting the
     * video of the media stream.
     *
     * - Returns: The latest video statistics of the media stream.
     *
     * - Tag: MediaStream.getVideoStatistics
     */
    public func getVideoStatistics() -> MediaStreamVideoStatistics {
        return videoStatistics
    }

    func getLabel() -> String {
        return label
    }

    func hasAudioSupplier() -> Bool {
        return audioSupplier != nil
    }

    func setAudioSupplier(_ audioSupplier: MediaStreamAudioSupplier!) {
        if (self.audioSupplier !== audioSupplier) {
            if (audioConsumers.count > 0) {
                if (self.audioSupplier != nil) {
                    let oldAudioSupplier = self.audioSupplier!
                    self.audioSupplier = nil
                    oldAudioSupplier.removeAudioConsumer(self)
                }
                self.audioSupplier = audioSupplier
                if (audioSupplier != nil) {
                    audioSupplier.addAudioConsumer(self)
                } else {
                    resetAudioStatistics()
                }
            } else {
                self.audioSupplier = audioSupplier
            }
        }
    }

    func hasVideoSupplier() -> Bool {
        return videoSupplier != nil
    }

    func setVideoSupplier(_ videoSupplier: MediaStreamVideoSupplier!) {
        if (self.videoSupplier !== videoSupplier) {
            if (videoConsumers.count > 0) {
                if (self.videoSupplier != nil) {
                    let oldVideoSupplier = self.videoSupplier!
                    self.videoSupplier = nil
                    oldVideoSupplier.removeVideoConsumer(self)
                }
                self.videoSupplier = videoSupplier
                if (videoSupplier != nil) {
                    videoSupplier.addVideoConsumer(self)
                } else {
                    resetVideoStatistics()
                }
            } else {
                self.videoSupplier = videoSupplier
            }
        }
    }

    func onAudioBuffer(_ audioBuffer: MediaStreamAudioBuffer!) {
        if (self.audioBuffer !== audioBuffer) {
            self.audioBuffer = audioBuffer
            if (audioBuffer != nil) {
                applyVolume()
            }
            for consumer in audioConsumers.keys {
                (consumer as! (any MediaStreamAudioConsumer)).onAudioBuffer(audioBuffer, self)
            }
        }
    }

    func onVideoBuffer(_ videoBuffer: MediaStreamVideoBuffer!) {
        if (self.videoBuffer !== videoBuffer) {
            self.videoBuffer = videoBuffer
            for consumer in videoConsumers {
                (consumer as! (any MediaStreamVideoConsumer)).onVideoBuffer(videoBuffer, self)
            }
        }
    }

    func addAudioConsumer(_ consumer: any MediaStreamAudioConsumer, _ volume: Double) {
        if (audioConsumers.updateValue(volume, forKey: consumer as! AnyHashable) == nil) {
            if (audioConsumers.count == 1 && audioSupplier != nil) {
                audioSupplier.addAudioConsumer(self)
            } else {
                consumer.onAudioBuffer(audioBuffer, self)
            }
        }
        if (audioBuffer != nil) {
            applyVolume()
        }
    }

    func removeAudioConsumer(_ consumer: any MediaStreamAudioConsumer) {
        if (audioConsumers.removeValue(forKey: consumer as! AnyHashable) != nil) {
            consumer.onAudioBuffer(nil, self)
            if (audioConsumers.count == 0 && audioSupplier != nil) {
                audioSupplier.removeAudioConsumer(self)
                resetAudioStatistics()
            }
        }
    }

    func addVideoConsumer(_ consumer: any MediaStreamVideoConsumer) {
        if (videoConsumers.insert(consumer as! AnyHashable).inserted) {
            if (videoConsumers.count == 1 && videoSupplier != nil) {
                videoSupplier.addVideoConsumer(self)
            } else {
                consumer.onVideoBuffer(videoBuffer, self)
            }
        }
    }

    func removeVideoConsumer(_ consumer: any MediaStreamVideoConsumer) {
        if (videoConsumers.remove(consumer as! AnyHashable) != nil) {
            consumer.onVideoBuffer(nil, self)
            if (videoConsumers.count == 0 && videoSupplier != nil) {
                videoSupplier.removeVideoConsumer(self)
                resetVideoStatistics()
            }
        }
    }

    func resetStatistics() {
        oldStatistics = nil
        updateStatistics(nil)
    }

    func updateStatistics(_ report: [RTCStatistics]!) {
        var newStatistics: [String: Int64] = [:]
        newStatistics["roundTripTime"] = 0
        newStatistics["audioBytesSent"] = 0
        newStatistics["audioBytesReceived"] = 0
        newStatistics["audioPacketsSent"] = 0
        newStatistics["audioPacketsReceived"] = 0
        newStatistics["audioPacketsLost"] = 0
        newStatistics["audioPacketSendDelay"] = 0
        newStatistics["audioJitterBufferDelay"] = 0
        newStatistics["audioJitterBufferEmittedCount"] = 0
        newStatistics["audioLevel"] = 0
        newStatistics["audioSamplesSent"] = 0
        newStatistics["audioSamplesPlayedOut"] = 0
        newStatistics["audioSilenceDuration"] = 0
        newStatistics["videoBytesSent"] = 0
        newStatistics["videoBytesReceived"] = 0
        newStatistics["videoPacketsSent"] = 0
        newStatistics["videoPacketsReceived"] = 0
        newStatistics["videoPacketsLost"] = 0
        newStatistics["videoPacketSendDelay"] = 0
        newStatistics["videoJitterBufferDelay"] = 0
        newStatistics["videoJitterBufferEmittedCount"] = 0
        newStatistics["videoWidth"] = 0
        newStatistics["videoHeight"] = 0
        newStatistics["videoFramesSent"] = 0
        newStatistics["videoFramesPlayedOut"] = 0
        if (oldStatistics == nil) {
            let currentTimestamp = Int64(Date().timeIntervalSince1970 * 1000)
            newStatistics["timestamp"] = currentTimestamp
            oldStatistics = newStatistics
        } else {
            let currentTimestamp = Int64(Date().timeIntervalSince1970 * 1000)
            newStatistics["timestamp"] = currentTimestamp
            if (report != nil) {
                var selectedCandidatePair: RTCStatistics! = nil
                var audioSource: RTCStatistics! = nil
                var videoSource: RTCStatistics! = nil
                var audioOutboundRtp: RTCStatistics! = nil
                var videoOutboundRtps: [RTCStatistics] = []
                var audioRemoteInboundRtp: RTCStatistics! = nil
                var videoRemoteInboundRtps: [RTCStatistics] = []
                var audioInboundRtp: RTCStatistics! = nil
                var videoInboundRtp: RTCStatistics! = nil
                for stats in report! {
                    switch ("\(stats.values["kind"] as? String ?? "nil")-\(stats.type)") {
                        case "nil-candidate-pair":
                            selectedCandidatePair = stats
                        case "audio-media-source":
                            audioSource = stats
                        case "video-media-source":
                            videoSource = stats
                        case "audio-outbound-rtp":
                            audioOutboundRtp = stats
                        case "video-outbound-rtp":
                            videoOutboundRtps.append(stats)
                        case "audio-remote-inbound-rtp":
                            audioRemoteInboundRtp = stats
                        case "video-remote-inbound-rtp":
                            videoRemoteInboundRtps.append(stats)
                        case "audio-inbound-rtp":
                            audioInboundRtp = stats
                        case "video-inbound-rtp":
                            videoInboundRtp = stats
                        default:
                            break
                    }
                }
                if (selectedCandidatePair != nil) {
                    newStatistics["roundTripTime"] = Int64(selectedCandidatePair.values["currentRoundTripTime"] as! Double * 1000)
                }
                if (label == "local") { // FIXME: We should use more robust approach for detecting local media streams
                    if (audioOutboundRtp != nil) {
                        newStatistics["audioBytesSent"] = (audioOutboundRtp.values["bytesSent"] as! Int64)
                        newStatistics["audioPacketSendDelay"] = 0 // TODO: Replace with `totalPacketSendDelay` when it is added
                        newStatistics["audioPacketsSent"] = (audioOutboundRtp.values["packetsSent"] as! Int64)
                    }
                    if (audioRemoteInboundRtp != nil) {
                        newStatistics["audioPacketsLost"] = (audioRemoteInboundRtp.values["packetsLost"] as! Int64)
                    }
                    if (audioSource != nil) {
                        if (audioSource.values["audioLevel"] != nil) { // FIXME: During recovery `audioSource` might have no `audioLevel` property
                            newStatistics["audioLevel"] = Int64(audioSource.values["audioLevel"] as! Double * 100)
                        }
                        if (audioSource.values["totalSamplesDuration"] != nil) { // FIXME: During recovery `audioSource` might have no `totalSamplesDuration` property
                            newStatistics["audioSamplesSent"] = Int64(audioSource.values["totalSamplesDuration"] as! Double * 48000)
                        }
                    }
                    for i in 0..<videoOutboundRtps.count {
                        let videoOutboundRtp = videoOutboundRtps[i]
                        newStatistics["videoBytesSent"] = newStatistics["videoBytesSent"]! + (videoOutboundRtp.values["bytesSent"] as! Int64)
                        newStatistics["videoPacketSendDelay"] = newStatistics["videoPacketSendDelay"]! + Int64(videoOutboundRtp.values["totalPacketSendDelay"] as! Double * 1000)
                        newStatistics["videoPacketsSent"] = newStatistics["videoPacketsSent"]! + (videoOutboundRtp.values["packetsSent"] as! Int64)
                        newStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"] = (videoOutboundRtp.values["framesSent"] as! Int64)
                        if (oldStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"] == nil) {
                            oldStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"] = 0
                        }
                        if (newStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"]! - oldStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"]! > 0) {
                            if (videoOutboundRtp.values["frameWidth"] != nil && videoOutboundRtp.values["frameHeight"] != nil && newStatistics["videoWidth"]! < (videoOutboundRtp.values["frameWidth"] as! Int64) && newStatistics["videoHeight"]! < (videoOutboundRtp.values["frameHeight"] as! Int64)) {
                                newStatistics["videoWidth"] = (videoOutboundRtp.values["frameWidth"] as! Int64)
                                newStatistics["videoHeight"] = (videoOutboundRtp.values["frameHeight"] as! Int64)
                                newStatistics["videoFramesSent"] = newStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"]
                                oldStatistics["videoFramesSent"] = oldStatistics["videoFramesSent\(videoOutboundRtp.values["ssrc"]!)"]
                            }
                        }
                    }
                    for videoRemoteInboundRtp in videoRemoteInboundRtps {
                        newStatistics["videoPacketsLost"] = newStatistics["videoPacketsLost"]! + (videoRemoteInboundRtp.values["packetsLost"] as! Int64)
                    }
                } else { // label != "local"
                    if (audioInboundRtp != nil) {
                        newStatistics["audioBytesReceived"] = (audioInboundRtp.values["bytesReceived"] as! Int64)
                        newStatistics["audioPacketsReceived"] = (audioInboundRtp.values["packetsReceived"] as! Int64)
                        newStatistics["audioPacketsLost"] = (audioInboundRtp.values["packetsLost"] as! Int64)
                        newStatistics["audioJitterBufferDelay"] = Int64(audioInboundRtp.values["jitterBufferDelay"] as! Double * 1000)
                        newStatistics["audioJitterBufferEmittedCount"] = (audioInboundRtp.values["jitterBufferEmittedCount"] as! Int64)
                        newStatistics["audioLevel"] = Int64((audioInboundRtp.values["audioLevel"] as? Double ?? 0) * 100.0) // FIXME: There could be no `audioLevel` in the firsts reports, so we use `?? 0`
                        newStatistics["audioSamplesPlayedOut"] = (audioInboundRtp.values["totalSamplesReceived"] as! Int64) - (audioInboundRtp.values["concealedSamples"] as! Int64)
                        newStatistics["audioSilenceDuration"] = audioInboundRtp.values["silentConcealedSamples"] as! Int64 / 48
                    }
                    if (videoInboundRtp != nil) {
                        newStatistics["videoBytesReceived"] = (videoInboundRtp.values["bytesReceived"] as! Int64)
                        newStatistics["videoPacketsReceived"] = (videoInboundRtp.values["packetsReceived"] as! Int64)
                        newStatistics["videoPacketsLost"] = (videoInboundRtp.values["packetsLost"] as! Int64)
                        newStatistics["videoJitterBufferDelay"] = Int64(videoInboundRtp.values["jitterBufferDelay"] as! Double * 1000)
                        newStatistics["videoJitterBufferEmittedCount"] = (videoInboundRtp.values["jitterBufferEmittedCount"] as! Int64)
                        if (videoInboundRtp.values["frameWidth"] != nil) {
                            newStatistics["videoWidth"] = (videoInboundRtp.values["frameWidth"] as! Int64)
                        }
                        if (videoInboundRtp.values["frameHeight"] != nil) {
                            newStatistics["videoHeight"] = (videoInboundRtp.values["frameHeight"] as! Int64)
                        }
                        newStatistics["videoFramesPlayedOut"] = (videoInboundRtp.values["framesReceived"] as! Int64) - (videoInboundRtp.values["framesDropped"] as! Int64)
                    }
                }
            }
            let dT = Double(newStatistics["timestamp"]! - oldStatistics["timestamp"]!) / 1000.0
            var audioStatistics = MediaStreamAudioStatistics(0, 0, 0, 0, 0, 0)
            var videoStatistics = MediaStreamVideoStatistics(0, 0, 0, 0, 0, 0, 0)
            if (audioBuffer != nil) {
                if (label == "local") { // FIXME: We should use more robust approach for detecting local media streams
                    audioStatistics = MediaStreamAudioStatistics(Int32(newStatistics["roundTripTime"]!),
                                                                 Double(newStatistics["audioBytesSent"]! - oldStatistics["audioBytesSent"]!) * 8.0 / dT,
                                                                 Double(newStatistics["audioPacketSendDelay"]! - oldStatistics["audioPacketSendDelay"]!) * 1.0 / Double(newStatistics["audioPacketsSent"]! - oldStatistics["audioPacketsSent"]!),
                                                                 Double(newStatistics["audioPacketsLost"]! - oldStatistics["audioPacketsLost"]!) * 100.0 / Double(newStatistics["audioPacketsSent"]! - oldStatistics["audioPacketsSent"]! + newStatistics["audioPacketsLost"]! - oldStatistics["audioPacketsLost"]!),
                                                                 Int32(newStatistics["audioLevel"]!),
                                                                 Double(newStatistics["audioSamplesSent"]! - oldStatistics["audioSamplesSent"]!) / dT)
                } else {
                    audioStatistics = MediaStreamAudioStatistics(Int32(newStatistics["roundTripTime"]!),
                                                                 Double(newStatistics["audioBytesReceived"]! - oldStatistics["audioBytesReceived"]!) * 8.0 / dT,
                                                                 Double(newStatistics["audioJitterBufferDelay"]! - oldStatistics["audioJitterBufferDelay"]!) * 1.0 / Double(newStatistics["audioJitterBufferEmittedCount"]! - oldStatistics["audioJitterBufferEmittedCount"]!),
                                                                 Double(newStatistics["audioPacketsLost"]! - oldStatistics["audioPacketsLost"]!) * 100.0 / Double(newStatistics["audioPacketsReceived"]! - oldStatistics["audioPacketsReceived"]! + newStatistics["audioPacketsLost"]! - oldStatistics["audioPacketsLost"]!),
                                                                 Int32(newStatistics["audioLevel"]!),
                                                                 Double(newStatistics["audioSamplesPlayedOut"]! - oldStatistics["audioSamplesPlayedOut"]!) / dT)
                }
            }
            if (videoBuffer != nil) {
                if (label == "local") { // FIXME: We should use more robust approach for detecting local media streams
                    videoStatistics = MediaStreamVideoStatistics(Int32(newStatistics["roundTripTime"]!),
                                                                 Double(newStatistics["videoBytesSent"]! - oldStatistics["videoBytesSent"]!) * 8.0 / dT,
                                                                 Double(newStatistics["videoPacketSendDelay"]! - oldStatistics["videoPacketSendDelay"]!) * 1.0 / Double(newStatistics["videoPacketsSent"]! - oldStatistics["videoPacketsSent"]!),
                                                                 Double(newStatistics["videoPacketsLost"]! - oldStatistics["videoPacketsLost"]!) * 100.0 / Double(newStatistics["videoPacketsSent"]! - oldStatistics["videoPacketsSent"]! + newStatistics["videoPacketsLost"]! - oldStatistics["videoPacketsLost"]!),
                                                                 Int32(newStatistics["videoWidth"]!),
                                                                 Int32(newStatistics["videoHeight"]!),
                                                                 Double(newStatistics["videoFramesSent"]! - oldStatistics["videoFramesSent"]!) / dT)
                } else {
                    videoStatistics = MediaStreamVideoStatistics(Int32(newStatistics["roundTripTime"]!),
                                                                 Double(newStatistics["videoBytesReceived"]! - oldStatistics["videoBytesReceived"]!) * 8.0 / dT,
                                                                 Double(newStatistics["videoJitterBufferDelay"]! - oldStatistics["videoJitterBufferDelay"]!) * 1.0 / Double(newStatistics["videoJitterBufferEmittedCount"]! - oldStatistics["videoJitterBufferEmittedCount"]!),
                                                                 Double(newStatistics["videoPacketsLost"]! - oldStatistics["videoPacketsLost"]!) * 100.0 / Double(newStatistics["videoPacketsReceived"]! - oldStatistics["videoPacketsReceived"]! + newStatistics["videoPacketsLost"]! - oldStatistics["videoPacketsLost"]!),
                                                                 Int32(newStatistics["videoWidth"]!),
                                                                 Int32(newStatistics["videoHeight"]!),
                                                                 Double(newStatistics["videoFramesPlayedOut"]! - oldStatistics["videoFramesPlayedOut"]!) / dT)
                }
            }
            if (audioSupplier != nil && audioConsumers.count > 0) {
                self.audioStatistics = audioStatistics
            }
            if (videoSupplier != nil && videoConsumers.count > 0) {
                self.videoStatistics = videoStatistics
            }
            oldStatistics = newStatistics
        }
    }

    private func resetAudioStatistics() {
        audioStatistics = MediaStreamAudioStatistics(0, 0, 0, 0, 0, 0)
    }

    private func resetVideoStatistics() {
        videoStatistics = MediaStreamVideoStatistics(0, 0, 0, 0, 0, 0, 0)
    }

    private func applyVolume() {
        var maxVolume = 0.0
        for volume in audioConsumers.values {
            if (volume > maxVolume) {
                maxVolume = volume
            }
        }
        audioBuffer.getTrack().source.volume = maxVolume
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(self))
    }

    public static func == (lhs: MediaStream, rhs: MediaStream) -> Bool {
        return lhs === rhs
    }

}
