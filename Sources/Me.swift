import Foundation
import Promises

/**
 * ***
 * Me class is used for representing participant on behalf of whom iOS-application is participating in the conference
 * (aka the local participant). All other participants are considered to be remote and represented with
 * [Participant](x-source-tag://Participant) class. You can get a representation of the local participant with
 * [getMe](x-source-tag://Conference.getMe) method of [Conference](x-source-tag://Conference) class. Me is a subclass
 * of [Participant](x-source-tag://Participant) class, so that it inherits all public methods of the superclass and
 * adds methods for setting primary and secondary media streams that should be sent on behalf of the local participant,
 * and for sending messages from the local participant to other participant(s) or to the server part of your
 * application:
 *
 * ```
 * let deviceRegistry = MindSDK.getDeviceRegistry()
 * let microphone = deviceRegistry.getMicrophone()
 * let camera = deviceRegistry.getCamera()
 * let myStream = MindSDK.createMediaStream(microphone, camera)
 * let me = conference.getMe()
 * me.setMediaStream(myStream)
 * // Acquire camera or/and microphone to start streaming video or/and audio on behalf of `me`
 * all(microphone.acquire(), camera.acquire()).catch({ error in
 *     print("Can't acquire camera or microphone: \(error)")
 * })
 *
 * ...
 *
 * me.sendMessageToAll("Hello, everybody!")
 * me.sendMessageToApplication("Hello, the server part of the application!")
 * if let participant = conference.getParticipantById("<PARTICIPANT_ID>") {
 *     me.sendMessageToParticipant("Hello, " + participant.getName(), participant)
 * }
 * ```
 *
 * Thread-Safety: all methods of Me class can be called on the main thread only. Calling these methods on another
 * thread will result in an `Error` or undefined behavior.
 *
 * - Tag: Me
 */
public class Me: Participant {

    override class func fromDTO(_ session: Session, _ dto: [String: Any]) -> Me {
        return Me(session, dto["id"] as! String, dto["name"] as! String, dto["priority"] as! Double, ParticipantRole.fromString(dto["role"] as! String))

    }

    private init(_ session: Session, _ id: String, _ name: String, _ priority: Double, _ role: ParticipantRole) {
        super.init(session, id, name, priority, role, false, false, false, false)
    }

    /**
     * Return the ID of the local participant. The ID is unique and never changes.
     *
     * - Returns: The ID of the local participant.
     *
     * - Tag: Me.getId
     */
    public override func getId() -> String {
        return super.getId()
    }

    /**
     * Returns the current name of the local participant. The name of the local participant can be shown above his
     * video in the conference media stream and recording.
     *
     * - Returns: The current name of the local participant.
     *
     * - Tag: Me.getName
     */
    public override func getName() -> String {
        return super.getName()
    }

    /**
     * Changes the name of the local participant. The name of the local participant can be shown above his video in the
     * conference media stream and recording. The name changing is an asynchronous operation, that's why this method
     * returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an `Error`
     * (if the operation fails).
     *
     * - Parameter name: The new name for the local participant.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Me.setName
     */
    public override func setName(_ name: String) -> Promise<Void> {
        return super.setName(name)
    }

    /**
     * Returns the current priority of the local participant. The priority defines a place which local participant
     * takes in conference media stream and recording.
     *
     * - Returns: The current priority of the local participant.
     *
     * - Tag: Me.getPriority
     */
    public override func getPriority() -> Double {
        return super.getPriority()
    }

    /**
     * Changes the priority of the local participant. The priority defines a place which local participant takes in
     * conference media stream and recording. The priority changing is an asynchronous operation, that's why this
     * method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an
     * `Error (if the operation fails). The operation can succeed only if the [local participant](x-source-tag://Me)
     * plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter priority: The new priority for the local participant.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Me.setPriority
     */
    public override func setPriority(_ priority: Double) -> Promise<Void> {
        return super.setPriority(priority)
    }

    /**
     * Returns the current [role](x-source-tag://ParticipantRole) of the local participant. The role defines a set of
     * permissions which the local participant is granted.
     *
     * - Returns: The current role of the local participant.
     *
     * - Tag: Me.getRole
     */
    public override func getRole() -> ParticipantRole {
        return super.getRole()
    }

    /**
     * Changes the [role](x-source-tag://ParticipantRole) of the local participant. The role defines a set of
     * permissions which the local participant is granted. The role changing is an asynchronous operation, that's why
     * this method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with
     * an `Error (if the operation fails). The operation can succeed only if the [local participant](x-source-tag://Me)
     * plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter role: The new role for the local participant.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Me.setRole
     */
    public override func setRole(_ role: ParticipantRole) -> Promise<Void> {
        return super.setRole(role)
    }

    /**
     * Checks whether the local participant is streaming primary audio (i.e. audio taken from his microphone). If both
     * this method and [isStreamingVideo](x-source-tag://Me.isStreamingVideo) return `false` then the participant is
     * not streaming the primary media stream at all.
     *
     * - Returns: The boolean value which indicates if the local participant is streaming primary audio or not.
     *
     * - Tag: Me.isStreamingAudio
     */
    public override func isStreamingAudio() -> Bool {
        return session.getWebRtcConnection().isSendingPrimaryAudio()
    }

    /**
     * Checks whether the local participant is streaming primary video (i.e. video taken from his camera). If both
     * this method and [isStreamingAudio](x-source-tag://Me.isStreamingAudio) return `false` then the participant is
     * not streaming the primary media stream at all.
     *
     * - Returns: The boolean value which indicates if the local participant is streaming primary video or not.
     *
     * - Tag: Me.isStreamingVideo
     */
    public override func isStreamingVideo() -> Bool {
        return session.getWebRtcConnection().isSendingPrimaryVideo()
    }

    /**
     * Returns [media stream](x-source-tag://MediaStream) which is being streamed on behalf of the local participant as
     * the primary media stream or `nil` value if the local participant is not streaming the primary media stream at
     * the moment. The primary media stream is intended for streaming video and audio taken from a camera and a
     * microphone of the iOS device, respectively.
     *
     * - Returns: The current primary media stream of the local participant.
     *
     * - Tag: Me.getMediaStream
     */
    public override func getMediaStream() -> MediaStream {
        return session.getWebRtcConnection().getPrimaryMediaStream()
    }

    /**
     * Sets [media stream](x-source-tag://MediaStream) for streaming on behalf of the local participant as the primary
     * media stream. The primary media stream is intended for streaming video and audio taken from a camera and a
     * microphone of the iOS device, respectively. If the primary media stream is already being streamed, then it
     * will be replaced with the passed one. Set `nil` value to stop streaming the primary media stream on behalf of
     * the local participant.
     *
     * - Parameter stream: The new primary media stream of the local participant.
     *
     * - Tag: Me.setMediaStream
     */
    public func setMediaStream(_ stream: MediaStream!) {
        session.getWebRtcConnection().setPrimaryMediaStream(stream)
    }

    /**
     * Checks whether the local participant is streaming secondary audio (i.e. an arbitrary content with audio). If
     * both this method and [isStreamingSecondaryVideo](x-source-tag://Me.isStreamingSecondaryVideo) return `false`
     * then the participant is not streaming secondary media stream at all.
     *
     * - Returns: The boolean value which indicates if the local participant is streaming secondary audio or not.
     *
     * - Tag: Me.isStreamingSecondaryAudio
     */
    public override func isStreamingSecondaryAudio() -> Bool {
        return session.getWebRtcConnection().isSendingSecondaryAudio()
    }

    /**
     * Checks whether the local participant is streaming secondary video (i.e. an arbitrary content with video). If
     * both this method and [isStreamingSecondaryAudio](x-source-tag://Me.isStreamingSecondaryAudio) return `false`
     * then the participant is not streaming secondary media stream at all.
     *
     * - Returns: The boolean value which indicates if the local participant is streaming secondary video or not.
     *
     * - Tag: Me.isStreamingSecondaryVideo
     */
    public override func isStreamingSecondaryVideo() -> Bool {
        return session.getWebRtcConnection().isSendingSecondaryVideo()
    }

    /**
     * Returns [media stream](x-source-tag://MediaStream) which is being streamed on behalf of the local participant as
     * the secondary media stream or `nil` value if the local participant is not streaming the secondary media stream
     * at the moment. The secondary media stream is intended for streaming an arbitrary audio/video content, e.g. for
     * sharing a screen of the iOS device.
     *
     * - Returns: The current secondary media stream of the local participant.
     *
     * - Tag: Me.getSecondaryMediaStream
     */
    public override func getSecondaryMediaStream() -> MediaStream {
        return session.getWebRtcConnection().getSecondaryMediaStream()
    }

    /**
     * Sets [media stream](x-source-tag://MediaStream) for streaming on behalf of the local participant as the
     * secondary media stream. The secondary media stream is intended for streaming an arbitrary audio/video content,
     * e.g. for sharing a screen of the iOS device. If the secondary media stream is already being streamed, then
     * it will be replaced with the passed one. Set `nil` value to stop streaming the secondary media stream on behalf
     * of the local participant.
     *
     * - Parameter stream: The new secondary media stream of the local participant.
     *
     * - Tag: Me.setSecondaryMediaStream
     */
    public func setSecondaryMediaStream(stream: MediaStream!) {
        session.getWebRtcConnection().setSecondaryMediaStream(stream)
    }

    /**
     * Sends a text message on behalf of the local participant to the server part of your application. The message
     * sending is an asynchronous operation, that's why this method returns a `Promise` that either resolves with no
     * value (if the operation succeeds) or rejects with an `Error` (if the operation fails).
     *
     * - Parameter message: The text of the message.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Me.sendMessageToApplication
     */
    @discardableResult public func sendMessageToApplication(_ message: String) -> Promise<Void> {
        let requestDTO: [String: Any] = [ "sendTo":  session.getApplicationId(), "text":  message, "persistent":  false ]
        return session.newHttpPost("/messages", requestDTO).then({ responseDTO in })
    }

    /**
     * Sends a text message on behalf of the local participant to the specified participant. The message sending is an
     * asynchronous operation, that's why this method returns a `Promise` that either resolves with no value (if the
     * operation succeeds) or rejects with an `Error` (if the operation fails).
     *
     * - Parameter message: The text of the message.
     * - Parameter participant: The participant which the message should be sent to.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Me.sendMessageTo
     */
    @discardableResult public func sendMessageTo(_ message: String, _ participant: Participant) -> Promise<Void> {
        let requestDTO: [String: Any] = [ "sendTo":  participant.getId(), "text":  message, "persistent":  false ]
        return session.newHttpPost("/messages", requestDTO).then({ responseDTO in })
    }

    /**
     * Sends a text message on behalf of the local participant to all in the conference, i.e. to the server part of
     * your application and to all participants at once. The message sending is an asynchronous operation, that's why
     * this method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with
     * an `Error` (if the operation fails).
     *
     * - Parameter message: The text of the message.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Me.sendMessageToAll
     */
    @discardableResult public func sendMessageToAll(_ message: String) -> Promise<Void> {
        let requestDTO: [String: Any] = [ "sendTo":  session.getConferenceId(), "text":  message, "persistent":  false ]
        return session.newHttpPost("/messages", requestDTO).then({ responseDTO in })
    }

    override func update(_ dto: [String : Any]) {
        let name = dto["name"] as! String
        if (self.name != name) {
            self.name = name
            session.fireOnMeNameChanged(self)
        }
        let priority = dto["priority"] as! Double
        if (self.priority != priority) {
            self.priority = priority
            session.fireOnMePriorityChanged(self)
        }
        let role = ParticipantRole.fromString(dto["role"] as! String)
        if (self.role != role) {
            self.role = role
            session.fireOnMeRoleChanged(self)
        }
    }

    override func destroy() {
        super.destroy()
    }

}
