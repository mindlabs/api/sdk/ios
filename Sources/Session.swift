import Foundation
import UIKit
import Promises
import WebRTC

/**
 * ***
 * Session class is used for representing a participation session of the [local participant](x-source-tag://Me). You
 * can get a representation of the participation session only as a result of [joining](x-source-tag://MindSDK.join) a
 * conference on behalf of one of the participants. It stays valid till you leave the conference in one of three ways:
 * [exit the conference at your own will](x-source-tag://MindSDK.exit2),
 * [being expelled from the conference](x-source-tag://SessionListener.onMeExpelled) or
 * [witness the end of the conference](x-source-tag://SessionListener.onConferenceEnded). Session class contains
 * methods for getting [state](x-source-tag://Session.getState) and [statistics](x-source-tag://Session.getStatistics)
 * of the participation session, and a method for getting the [conference](x-source-tag://Session.getConference) which
 * the [local participant](x-source-tag://Me) is participating in:
 *
 * ```
 * @IBOutlet var conferenceVideo: Video!
 *
 * ...
 * let conferenceURI = "https://api.mind.com/<APPLICATION_ID>/<CONFERENCE_ID>"
 * let participantToken = "<PARTICIPANT_TOKEN>"
 * let options = SessionOptions()
 * MindSDK.join(conferenceURI, participantToken, options).then({ session in
 *     let sessionListener = SessionListener()
 *     session.setListener(sessionListener)
 *     let conference = session.getConference()
 *     ...
 * })
 * ```
 *
 * Thread-Safety: all public methods of Session class can be called on the main thread only. Calling these methods on
 * another thread will result in an `Error` or undefined behavior.
 *
 *  - Tag: Session
 */
public class Session {

    private static let HTTP_CONNECTION_TIMEOUT_SECONDS: TimeInterval = 10
    private static var LAST_SESSION_ID: Int = 0

    // Thread-Safety: `currentContext` can be accessed on any thread
    private let currentContext = DispatchSpecificKey<Session>()

    private let id: Int
    private let uri: String
    private let token: String
    private let options: SessionOptions
    private var listener: SessionListener!

    private var state: SessionState
    private var destroyed: Bool!

    private var wrc: WebRtcConnection!
    private var recoveryDelay: Int = 0
    private var recoveryWorkItem: DispatchWorkItem!

    private var conference: Conference!

    private var notifications: [String]

    private var statistics: SessionStatistics

    init(_ uri: String, _ token: String, _ listener: SessionListener!, _ options: SessionOptions) {
        Session.LAST_SESSION_ID += 1
        self.id = Session.LAST_SESSION_ID
        self.uri = uri.replacingOccurrences(of: "/+$", with: "", options: .regularExpression)
        self.token = token
        self.listener = listener
        self.options = SessionOptions(options)
        self.state = .NORMAL
        self.destroyed = true
        self.notifications = [String]()
        self.statistics = SessionStatistics("none", "none", 0, "none", 0)
    }

    /**
     * Returns the ID of the session. The ID is unique and never changes.
     *
     * - Returns: The ID of the session.
     *
     * - Tag: Session.getId
     */
    public func getId() -> Int {
        return id
    }

    /**
     * Returns the current [state] {x-source-tag://SessionState state} of the session.
     *
     * - Returns: The current state of the session.
     *
     * - Tag: Session.getState
     */
    public func getState() -> SessionState {
        return state
    }

    /**
     * Returns the latest [statistics](x-source-tag://SessionStatistics) of the session. The statistics consists of
     * instant measures of the underlying network connection of the session.
     *
     * - Returns: The latest statistics of the session.
     *
     * - Tag: Session.getStatistics
     */
    public func getStatistics() -> SessionStatistics {
        return statistics
    }

    /**
     * Returns the [Conference](x-source-tag://Conference).
     *
     * - Returns: The conference.
     *
     * - Tag: Session.getConference
     */
    public func getConference() -> Conference {
        return conference
    }

    /**
     * Sets the listener which should be notified of all events related to the conference session. The listener can be
     * set at any moment.
     *
     * - Parameter listener: The listener which should be notified of all events related to the conference session.
     *
     * - Tag: Session.setListener
     */
    public func setListener(_ listener: SessionListener) {
        self.listener = listener
    }

    func getApplicationId() -> String {
        return uri.components(separatedBy: "/")[3]
    }

    func getConferenceId() -> String {
        return uri.components(separatedBy: "/")[4]
    }

    func getRecordingURL() -> String {
        return uri + "/recording?access_token=" + token
    }

    func getOptions() -> SessionOptions {
        return options
    }

    func getWebRtcConnection() -> WebRtcConnection {
        return wrc
    }

    func open() -> Promise<Session> {
        destroyed = false
        let result = Promise<Session>.pending()
        let recover: ((Error) -> Void) = { error in
            self.setState(.FAILED)
            if (self.conference != nil) {
                print("Conference session will be reopened in \(self.recoveryDelay) seconds after a failure \(error)")
                self.notifications.removeAll()
                self.recoveryWorkItem = self.scheduleOnMainThread({ self.wrc.open() }, self.recoveryDelay * 1000)
                self.recoveryDelay = min(self.recoveryDelay + 1, 10)
            } else {
                result.reject(error)
            }
        }
        wrc = WebRtcConnection(self)
        wrc.onOpened = {
            self.newHttpGet("/?detailed=true").then({ responseDTO in
                if self.conference == nil {
                    self.conference = Conference.fromDTO(self, responseDTO)
                    result.fulfill(self)
                } else {
                    self.updateEntireModel(responseDTO)
                }
                self.processNotifications()
                self.recoveryDelay = 0
                self.setState(.NORMAL)
            }).catch({ error in
                recover(error)
            })
        }
        wrc.onMessageReceived = { notification in
            self.notifications.append(notification)
            if (self.conference != nil) {
                self.processNotifications()
            }
        }
        wrc.onStartedLagging = {
            self.setState(.LAGGING)
        }
        wrc.onStoppedLagging = {
            self.setState(.NORMAL)
        }
        wrc.onFailed = { error in
            recover(error)
        }
        wrc.onClosed = { code in
            switch (code) {
                case 4000:
                    self.notifications.append("{\"type\":\"deleted\",\"location\":\"/\"}")
                    if (self.conference != nil) {
                        self.processNotifications()
                    }
                case 4001:
                    self.notifications.append("{\"type\":\"deleted\",\"location\":\"/participants/me\"}")  // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
                    if (self.conference != nil) {
                        self.processNotifications()
                    }
                default:
                    recover(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "WebRTC connection closed: \(code)"]))
            }
        }
        wrc.open()
        return result
    }

    func close() {
        statistics = SessionStatistics("none", "none", 0, "none", 0)
        if (recoveryWorkItem != nil) {
            recoveryWorkItem.cancel()
            recoveryWorkItem = nil
        }
        if (conference != nil) {
            conference.destroy()
            conference = nil
        }
        if (wrc != nil) {
            wrc.close()
            wrc = nil
        }
        destroyed = true
    }

    // Thread-Safety: can be called on any thread
    func executeOnMainThread(_ function: @escaping () -> Void) {
        if (Thread.isMainThread) {
            if (!self.destroyed) {
                function()
            }
        } else {
            let item = DispatchWorkItem(block: {
                if (!self.destroyed) {
                    function()
                }
            })
            DispatchQueue.main.async(execute: item)
        }
    }

    // Thread-Safety: can be called on any thread
    func scheduleOnMainThread(_ function: @escaping () -> Void, _ delayMs: Int) -> DispatchWorkItem {
        let item = DispatchWorkItem(block: {
            if (!self.destroyed) {
                function()
            }
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delayMs), execute: item)
        return item
    }

    // Thread-Safety: can be called on any thread
    func scheduleOnMainThreadWithFixedDelay(_ function: @escaping () -> Void, _ delayMs: Int) -> DispatchWorkItem {
        let result = DispatchWorkItem(block: {})
        var block: (() -> Void)! = nil
        block = {
            if (!self.destroyed && !result.isCancelled) {
                function()
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delayMs), execute: DispatchWorkItem(block: block))
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delayMs), execute: DispatchWorkItem(block: block))
        return result
    }

    @discardableResult func newHttpGet(_ relativeURI: String, _ cancellation: ((URLSessionDataTask) -> Void)? = nil) -> Promise<[String: Any]> {
        return newHttpRequest("GET", uri + relativeURI, nil, cancellation)
    }

    @discardableResult func newHttpPost(_ relativeURI: String, _ dto: [ String: Any ], _ cancellation: ((URLSessionDataTask) -> Void)? = nil) -> Promise<[String: Any]> {
        return newHttpRequest("POST", uri + relativeURI, dto, cancellation)
    }

    @discardableResult func newHttpPatch(_ relativeURI: String, _ dto: [ String: Any ], _ cancellation: ((URLSessionDataTask) -> Void)? = nil) -> Promise<[String: Any]> {
        return newHttpRequest("PATCH", uri + relativeURI, dto, cancellation)
    }

    @discardableResult func newHttpRequest(_ method: String, _ url: String, _ dto: [ String: Any ]!, _ cancellation: ((URLSessionDataTask) -> Void)? = nil) -> Promise<[String: Any]> {
        let result = Promise<[String: Any]>.pending()
        var request = URLRequest(url: URL(string: url)!, timeoutInterval: Session.HTTP_CONNECTION_TIMEOUT_SECONDS)
        request.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
        request.setValue("Mind iOS SDK \(MindSDK.VERSION)", forHTTPHeaderField: "Mind-SDK")
        request.setValue("\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)", forHTTPHeaderField: "User-Agent")
        request.httpMethod = method
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        if (dto != nil) {
            request.httpBody = try! JSONSerialization.data(withJSONObject: dto, options: [])
        }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                if let error = error {
                    if ((error as NSError).code != NSURLErrorCancelled) {
                        result.reject(error)
                    }
                } else {
                    let response = response as! HTTPURLResponse
                    if (200...299 ~= response.statusCode) {
                        if let data = data, let dto = data.count > 0 ? try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] : [:] {
                            result.fulfill(dto)
                        } else {
                            result.reject(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't parse JSON : \(data as Optional)"]))
                        }
                    } else {
                        if (response.statusCode == 404 && self.conference != nil) {
                            self.executeOnMainThread {
                                self.notifications.append("{\"type\":\"deleted\",\"location\":\"/participants/me\"}")  // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
                                self.processNotifications()
                            }
                        } else {
                            result.reject(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "HTTP response status code: \(response.statusCode)"]))
                        }
                    }
                }
            } catch {
                result.reject(error)
            }
        }
        cancellation?(task)
        task.resume()
        return result
    }

    func fireOnConferenceNameChanged(_ conference: Conference) {
        if (listener != nil) {
            listener.onConferenceNameChanged(conference)
        }
    }

    func fireOnConferenceRecordingStarted(_ conference: Conference) {
        if (listener != nil) {
            listener.onConferenceRecordingStarted(conference)
        }
    }

    func fireOnConferenceRecordingStopped(_ conference: Conference) {
        if (listener != nil) {
            listener.onConferenceRecordingStopped(conference)
        }
    }

    func fireOnConferenceEnded(_ conference: Conference) {
        if (listener != nil) {
            listener.onConferenceEnded(conference)
        }
    }

    func fireOnParticipantJoined(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantJoined(participant)
        }
    }

    func fireOnParticipantExited(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantExited(participant)
        }
    }

    func fireOnParticipantNameChanged(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantNameChanged(participant)
        }
    }

    func fireOnParticipantPriorityChanged(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantPriorityChanged(participant)
        }
    }

    func fireOnParticipantRoleChanged(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantRoleChanged(participant)
        }
    }

    func fireOnParticipantMediaChanged(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantMediaChanged(participant)
        }
    }

    func fireOnParticipantSecondaryMediaChanged(_ participant: Participant) {
        if (listener != nil) {
            listener.onParticipantSecondaryMediaChanged(participant)
        }
    }

    func fireOnMeExpelled(_ me: Me) {
        if (listener != nil) {
            listener.onMeExpelled(me)
        }
    }

    func fireOnMeNameChanged(_ me: Me) {
        if (listener != nil) {
            listener.onMeNameChanged(me)
        }
    }

    func fireOnMePriorityChanged(_ me: Me) {
        if (listener != nil) {
            listener.onMePriorityChanged(me)
        }
    }

    func fireOnMeRoleChanged(_ me: Me) {
        if (listener != nil) {
            listener.onMeRoleChanged(me)
        }
    }

    func fireOnMeReceivedMessageFromApplication(_ me: Me, _ message: String) {
        if (listener != nil) {
            listener.onMeReceivedMessageFromApplication(me, message)
        }
    }

    func fireOnMeReceivedMessageFromParticipant(_ me: Me, _ message: String, _ participant: Participant) {
        if (listener != nil) {
            listener.onMeReceivedMessageFromParticipant(me, message, participant)
        }
    }

    func updateStatistics(_ report: [RTCStatistics]!) {
        if (report != nil) {
            var selectedCandidatePair: RTCStatistics! = nil
            var localCandidate: RTCStatistics! = nil
            var remoteCandidate: RTCStatistics! = nil
            for stats in report! {
                switch (stats.type) {
                    case "candidate-pair":
                        selectedCandidatePair = stats
                    case "local-candidate":
                        localCandidate = stats
                    case "remote-candidate":
                        remoteCandidate = stats
                    default:
                        break
                }
            }
            if (selectedCandidatePair != nil && localCandidate != nil && remoteCandidate != nil) {
                self.statistics = SessionStatistics(localCandidate.values["protocol"] as! String,
                                                    localCandidate.values["ip"] as! String,
                                                    localCandidate.values["port"] as! UInt16,
                                                    remoteCandidate.values["ip"] as! String,
                                                    remoteCandidate.values["port"] as! UInt16)
            }
        }

    }

    private func updateEntireModel(_ dto: [String: Any]) {
        updateModelItem("/", dto)
        let participantDTOs = dto["participants"] as! [[String: Any]]
        // Build a set of IDs of online participants
        var onlineParticipantIDs = Set<String>()
        for i in 0..<participantDTOs.count {
            let participantDTO = participantDTOs[i]
            if (participantDTO["online"] as! Bool) {
                onlineParticipantIDs.insert(participantDTO["id"] as! String)
            }
        }
        // Remove missing and offline participants
        let participants = conference.getParticipants()
        for i in (0..<participants.count).reversed() {
            if(!onlineParticipantIDs.contains(participants[i].getId())) {
                deleteModelItem("/participants/" + participants[i].getId())
            }
        }
        // Update existent participants and add new participants
        for i in 0..<participantDTOs.count {
            let participantDTO = participantDTOs[i]
            if (participantDTO["online"] as! Bool) {
                let participant = conference.getParticipantById(participantDTO["id"] as! String)
                if (participant != nil) {
                    updateModelItem("/participants/" + (participantDTO["id"] as! String), participantDTO)
                } else {
                    createModelItem("/participants/" + (participantDTO["id"] as! String), participantDTO)
                }
            }
        }
    }

    private func createModelItem(_ location: String, _ dto: [String: Any]) {
        if (location.starts(with: "/participants/")) {
            if (dto["online"] as! Bool) {
                let participant = Participant.fromDTO(self, dto)
                conference.addParticipant(participant)
            }
        } else if (location.starts(with: "/messages/")) {
            let sentBy = dto["sentBy"] as! String
            if (sentBy == getApplicationId()) {
                fireOnMeReceivedMessageFromApplication(conference.getMe(), dto["text"] as! String)
            } else {
                fireOnMeReceivedMessageFromParticipant(conference.getMe(), dto["text"] as! String, conference.getParticipantById(sentBy)!)
            }
        }
    }

    private func updateModelItem(_ location: String, _ dto: [String: Any]) {
        if (location == "/") {
            conference.update(dto)
        } else if (location.starts(with: "/participants/")) {
            if (dto["online"] as! Bool) {
                if let participant = conference.getParticipantById(String(location["/participants/".endIndex..<location.endIndex])) {
                    participant.update(dto)
                } else {
                    createModelItem(location, dto)
                }
            } else {
                deleteModelItem(location)
            }
        }
    }

    private func deleteModelItem(_ location: String) {
        if (location == "/") {
            let conference = self.conference!
            MindSDK.exit2(self)
            fireOnConferenceEnded(conference)
        } else if (location == "/participants/me") { // FIXME: We have to use `me` alias here because we can be expelled from the conference before joining is completed (i.e. before the actual ID of `me` is known)
            let conference = self.conference!
            MindSDK.exit2(self)
            fireOnMeExpelled(conference.getMe())
        } else if (location.starts(with: "/participants/")) {
            if let participant = conference.getParticipantById(String(location["/participants/".endIndex..<location.endIndex])) {
                conference.removeParticipant(participant)
                participant.destroy()
            }
        }
    }

    private func processNotifications() {
        for notification in notifications {
            do {
                let json = try JSONSerialization.jsonObject(with: notification.data(using: .utf8)!, options: []) as! [String: Any]
                switch (json["type"] as! String) {
                    case "created":
                        self.createModelItem(json["location"] as! String, json["resource"] as! [String: Any])
                    case "updated":
                        self.updateModelItem(json["location"] as! String, json["resource"] as! [String: Any])
                    case "deleted":
                        self.deleteModelItem(json["location"] as! String)
                    default:
                        break
                }
            } catch {
                print("Can't parse notification `\(notification)`: \(error)")
            }
        }
        notifications.removeAll()
    }

    private func setState(_ state: SessionState) {
        if (self.state != state) {
            self.state = state
            if (listener != nil) {
                listener.onSessionStateChanged(self)
            }
        }
    }

}
