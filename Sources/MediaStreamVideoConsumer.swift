/**
 * ***
 * Any video consumer of [MediaStream](x-source-tag://MediaStream) is required to implement MediaStreamVideoConsumer
 * protocol. The protocol defines only one method:
 * [onVideoBuffer](x-source-tag://MediaStreamVideoConsumer.onVideoBuffer). [MediaStream](x-source-tag://MediaStream)
 * uses it to supply a new video to the consumer or cancel the previously supplied one. The consumer should use
 * [addVideoConsumer](x-source-tag://MediaStream.addVideoConsumer) and
 * [removeVideoConsumer](x-source-tag://MediaStream.removeVideoConsumer) methods of
 * [MediaStream](x-source-tag://MediaStream) class to register and unregister itself as a consumer of video from the
 * [MediaStream](x-source-tag://MediaStream), respectively.
 *
 * Thread-Safety: all methods of MediaStreamVideoConsumer class can be called on the main thread only. Calling these
 * methods on another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: MediaStreamVideoConsumer
 */
public protocol MediaStreamVideoConsumer: Hashable {

    /**
     * Supplies a new video to the consumer or cancels the previously supplied one in which case `nil` is passed as a
     * value for `videoBuffer` argument. It is guaranteed that during
     * [unregistration](x-source-tag://MediaStream.removeVideoConsumer) this method is always called with `nil` as a
     * value for `videoBuffer` argument regardless whether video has been supplied to the consumer or not.
     *
     * - Parameter videoBuffer: The new video buffer or `nil` if the previously supplied video buffer should be canceled.
     * - Parameter supplier: The media stream which supplies or cancels the video buffer.
     *
     * - Tag: MediaStreamVideoConsumer.onVideoBuffer
     */
    func onVideoBuffer(_ videoBuffer: MediaStreamVideoBuffer!, _ supplier: MediaStream)

}
