import WebRTC

public class MediaStreamVideoBuffer {

    private let track: RTCVideoTrack
    private let remote: Bool
    private let width: Int
    private let height: Int
    private let bitrate: Int
    private let adaptivity: Int
    private let scale: Double

    init(_ track: RTCVideoTrack, _ remote: Bool, _ width: Int, _ height: Int, _ bitrate: Int, _ adaptivity: Int, _ scale: Double) {
        self.track = track
        self.remote = remote
        self.width = width
        self.height = height
        self.bitrate = bitrate
        self.adaptivity = adaptivity
        self.scale = scale
    }

    func getTrack() -> RTCVideoTrack {
        return self.track
    }

    func isRemote() -> Bool {
        return self.remote
    }

    func getWidth() -> Int {
        return self.width
    }

    func getHeight() -> Int {
        return self.height
    }

    func getBitrate() -> Int {
        return self.bitrate
    }

    func getAdaptivity() -> Int {
        return self.adaptivity
    }

    func getScale() -> Double {
        return self.scale
    }

}
