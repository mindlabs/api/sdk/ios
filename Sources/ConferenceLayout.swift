/**
 * ***
 * ConferenceLayout enumerates all available layouts for arranging videos in
 * [recording](x-source-tag://Conference.getRecordingURL) and
 * [conference media stream](x-source-tag://Conference.getMediaStream). The arrangement of videos depends not only on
 * layout but also on a conference mode. At any given moment conference can be in one of two modes: conversion or
 * presentation. If none of [presenters](x-source-tag://ParticipantRole.PRESENTER) or
 * [moderators](x-source-tag://ParticipantRole.MODERATOR) is streaming its secondary video, then the conference
 * considered to be in the conversion mode, if at least one of [presenters](x-source-tag://ParticipantRole.PRESENTER)
 * or [moderators](x-source-tag://ParticipantRole.MODERATOR) is streaming its secondary video, then the conference
 * considered to be in the presentation mode. The conference layout is set during conference creation and cannot be
 * changed afterwards.
 *
 * - Tag: ConferenceLayout
 */
public enum ConferenceLayout: String, CustomStringConvertible {

    /**
     * In conversation mode the `MOSAIC` layout assumes displaying all primary videos of all
     * [speakers](x-source-tag://ParticipantRole.SPEAKER), [presenters](x-source-tag://ParticipantRole.PRESENTER) and
     * [moderators](x-source-tag://ParticipantRole.MODERATOR) at the same time arranged according to their priorities:
     * the area is divided into equal rectangles, each of which is given for primary video of one of the participants —
     * the top-left rectangle is given to the participant with the highest priority, the bottom-right rectangle — to
     * the participant with the lowest priority. In presentation mode `MOSAIC` layout assumes displaying fullscreen
     * only one secondary video of a [presenter](x-source-tag://ParticipantRole.PRESENTER) or a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR) with the highest priority.
     *
     * - Tag: ConferenceLayout.MOSAIC
     */
    case MOSAIC = "mosaic"

    /**
     * In conversation mode the `SELECTOR` layout assumes displaying fullscreen only one primary video of the loudest
     * [speaker](x-source-tag://ParticipantRole.SPEAKER), [presenter](x-source-tag://ParticipantRole.PRESENTER) or
     * [moderator](x-source-tag://ParticipantRole.MODERATOR). In presentation mode `SELECTOR` layout assumes displaying
     * fullscreen only one secondary video of a [presenter](x-source-tag://ParticipantRole.PRESENTER) or a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR) with the highest priority.
     *
     * - Tag: ConferenceLayout.SELECTOR
     */
    case SELECTOR = "selector"

    /**
     * In conversation mode the `PRESENTING_MOSAIC` layout assumes displaying all primary videos of all
     * [speakers](x-source-tag://ParticipantRole.SPEAKER), [presenters](x-source-tag://ParticipantRole.PRESENTER) and
     * [moderators](x-source-tag://ParticipantRole.MODERATOR) at the same time arranged according to their priorities:
     * the area is divided into equal rectangles, each of which is given for primary video of one of the participants —
     * the top-left rectangle is given to the participant with the highest priority, the bottom-right rectangle — to
     * the participant with the lowest priority. In presentation mode `PRESENTING_MOSAIC` layout assumes displaying
     * fullscreen secondary video of a [presenter](x-source-tag://ParticipantRole.PRESENTER) or a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR) with the highest priority and also its primary video in
     * the bottom-right corner (above the secondary video).
     *
     * - Tag: ConferenceLayout.PRESENTING_MOSAIC
     */
    case PRESENTING_MOSAIC = "presenting_mosaic"

    /**
     * In conversation mode the `PRESENTING_SELECTOR` layout assumes displaying fullscreen only one primary video of
     * the loudest [speaker](x-source-tag://ParticipantRole.SPEAKER),
     * [presenter](x-source-tag://ParticipantRole.PRESENTER) or [moderator](x-source-tag://ParticipantRole.MODERATOR).
     * In presentation mode `PRESENTING_SELECTOR` layout assumes displaying fullscreen secondary video of a
     * [presenter](x-source-tag://ParticipantRole.PRESENTER) or a [moderator](x-source-tag://ParticipantRole.MODERATOR)
     * with the highest priority and also its primary video in the bottom-right corner (above the secondary video).
     *
     * - Tag: ConferenceLayout.PRESENTING_SELECTOR
     */
    case PRESENTING_SELECTOR = "presenting_selector"

    static func fromString(_ string: String) -> ConferenceLayout {
        return ConferenceLayout(rawValue: string)!
    }

    public var description: String {
        return rawValue
    }

}
