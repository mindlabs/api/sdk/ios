import Foundation
import WebRTC

public class Video: UIView, RTCVideoViewDelegate, MediaStreamAudioConsumer, MediaStreamVideoConsumer {

#if arch(arm64)
    private var videoView: RTCMTLVideoView!
#else
    private var videoView: RTCEAGLVideoView!
#endif

    private var videoSize: CGSize = CGSize.zero
    private var scalingMode: UIView.ContentMode = .scaleAspectFit
    private var stream: MediaStream!
    private var videoBuffer: MediaStreamVideoBuffer!
    private var volume: Double = 1.0

    public override init(frame: CGRect) {
        super.init(frame: frame)
    #if arch(arm64)
        videoView = RTCMTLVideoView(frame: frame)
        videoView.delegate = self
        videoView.videoContentMode = scalingMode
    #else
        videoView = RTCEAGLVideoView(frame: frame)
        videoView.delegate = self
    #endif
        addSubview(videoView)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    #if arch(arm64)
        videoView = RTCMTLVideoView(coder: aDecoder)!
        videoView.delegate = self
        videoView.videoContentMode = scalingMode
    #else
        videoView = RTCEAGLVideoView(coder: aDecoder)!
        videoView.delegate = self
    #endif
        addSubview(videoView)
    }

    /**
     * Sets {@code MediaStream} that should used as a source of audio and video for the {@code Video}. The `nil`
     * value can be used for stopping playing previously set {@code MediaStream}.
     */
    public func setMediaStream(_ stream: MediaStream!) {
        if (self.stream !== stream) {
            if (self.stream != nil) {
                self.stream.removeAudioConsumer(self)
                self.stream.removeVideoConsumer(self)
            }
            if (stream == nil) {
                if (self.stream != nil) {
                    videoSize = CGSize.zero
                    setNeedsLayout()
                    self.stream = nil
                }
            } else {
                self.stream = stream
                stream.addAudioConsumer(self, volume)
                stream.addVideoConsumer(self)
            }
        }
    }

    /**
     * Sets the volume of the audio. The volume can be in range between 0.0 and 1.0, inclusively, where 0.0 means the
     * absolute silence, and 1.0 means the highest volume. The default volume is 1.0.
     *
     * - Parameter volume: The volume of the audio.
     */
    public func setVolume(_ volume: Double) {
        self.volume = volume
        if (stream != nil) {
            stream.addAudioConsumer(self, volume)
        }
    }

    /**
     * Configures how the video will fill the allowed layout area. We support two scaling modes: `.scaleAspectFill` and
     * `.scaleAspectFit`.
     */
    public func setScalingMode(_ scalingMode: UIView.ContentMode) {
        precondition(scalingMode == .scaleAspectFill || scalingMode == .scaleAspectFit, "Unsupported scaling type: \(scalingMode)")
        self.scalingMode = scalingMode
        setNeedsLayout()
    }

    public func onAudioBuffer(_ audioBuffer: MediaStreamAudioBuffer!, _ supplier: MediaStream) {}

    public func onVideoBuffer(_ videoBuffer: MediaStreamVideoBuffer!, _ supplier: MediaStream) {
        if (self.videoBuffer !== videoBuffer) {
            if (self.videoBuffer != nil) {
                self.videoBuffer.getTrack().remove(videoView)
            }
            self.videoBuffer = videoBuffer
            if (self.videoBuffer != nil) {
                self.videoBuffer.getTrack().add(videoView)
            }
        }
    }

    public func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        if (self.videoView === videoView) {
            videoSize = size
            setNeedsLayout()
        }
    }

    public override func layoutSubviews() {
        if (videoSize == CGSize.zero) {
            videoView.isHidden = true
        } else {
        #if arch(arm64)
            videoView.videoContentMode = scalingMode
            videoView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height) // FIXME: We can't use `videoView.frame = self.view` because somehow `self.frame.y` is 20...
        #else
            var videoViewBounds = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
            if (scalingMode == .scaleAspectFill) {
                if (bounds.width / bounds.height >= 1.0 && videoSize.width / videoSize.height < 1.0) {
                    videoViewBounds = CGRect(x: 0, y: 0, width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
                } else if (bounds.width / bounds.height < 1.0 && videoSize.width / videoSize.height >= 1.0) {
                    videoViewBounds = CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: bounds.height)
                } else {
                    if (bounds.width / bounds.height > videoSize.width / videoSize.height) {
                        videoViewBounds = CGRect(x: 0, y: 0, width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
                    } else {
                        videoViewBounds = CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: bounds.height)
                    }
                }
            }
            videoView.frame = AVMakeRect(aspectRatio: videoSize, insideRect: videoViewBounds)
            videoView.center = CGPoint(x: bounds.midX, y: bounds.midY)
        #endif
            videoView.isHidden = false
        }
    }

}
