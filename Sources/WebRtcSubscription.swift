import WebRTC

class WebRtcSubscription {

    private var audioTransceiver: WebRtcTransceiver!
    private var videoTransceiver: WebRtcTransceiver!
    private var audioConsumer: MediaStream!
    private var videoConsumer: MediaStream!

    func getAudioTransceiver() -> WebRtcTransceiver! {
        return audioTransceiver
    }

    func setAudioTransceiver(_ audioTransceiver: WebRtcTransceiver!) {
        self.audioTransceiver = audioTransceiver
    }

    func getVideoTransceiver() -> WebRtcTransceiver! {
        return videoTransceiver
    }

    func setVideoTransceiver(_ videoTransceiver: WebRtcTransceiver!) {
        self.videoTransceiver = videoTransceiver
    }

    func getAudioConsumer() -> MediaStream! {
        return audioConsumer
    }

    func setAudioConsumer(_ audioConsumer: MediaStream!) {
        self.audioConsumer = audioConsumer
    }

    func getVideoConsumer() -> MediaStream! {
        return videoConsumer
    }

    func setVideoConsumer(_ videoConsumer: MediaStream!) {
        self.videoConsumer = videoConsumer
    }

}
