import WebRTC
import Promises

/**
 * ***
 * Microphone class is used for representing the microphone of the iOS device. The instance of Microphone class that
 * represent the built-in microphone can be got with [getMicrophone](x-source-tag://DeviceRegistry.getMicrophone)
 * method of [DeviceRegistry](x-source-tag://DeviceRegistry) class. Microphone class implements
 * [MediaStreamAudioSupplier](x-source-tag://MediaStreamAudioSupplier) protocol, so it can be used as a source of
 * audio for local [MediaStream](x-source-tag://MediaStream).
 *
 * ```
 * let deviceRegistry = MindSDK.getDeviceRegistry()
 * let microphone = deviceRegistry.getMicrophone()
 * MediaStream myStream = MindSDK.createMediaStream(microphone, nil)
 * me.setMediaStream(myStream)
 * microphone.acquire().catch({ error in
 *     print("Microphone can't be acquired: \(error)")
 * })
 * ```
 *
 * Thread-Safety: all methods of Microphone class can be called on the main thread only. Calling these methods on
 * another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: Microphone
 */
public class Microphone: MediaStreamAudioSupplier {

    private var consumers = Set<MediaStream>()

    private let peerConnectionFactory: RTCPeerConnectionFactory

    private var audioSource: RTCAudioSource!
    private var audioTrack: RTCAudioTrack!
    private var acquiringPromise: Promise<Void>!

    private var muted: Bool

    init(_ peerConnectionFactory: RTCPeerConnectionFactory) {
        self.peerConnectionFactory = peerConnectionFactory
        self.muted = false
    }

    /**
     * Sets the muted state of the microphone. The muted state of the microphone determines whether the microphone
     * produces an actual audio (if it is unmuted) or silence (if it is muted). The muted state can be changed at any
     * moment regardless whether the microphone is acquired or not.
     *
     * - Parameter muted: The muted state of the microphone.
     *
     * - Tag: Microphone.setMuted
     */
    public func set(muted: Bool) {
        if (self.muted != muted) {
            self.muted = muted
            if (audioTrack != nil) {
                audioTrack.isEnabled = !muted
            }
        }
    }

    /**
     * Starts microphone recording. This is an asynchronous operation which assumes acquiring the underlying microphone
     * device and distributing microphone's audio among all [consumers](x-source-tag://MediaStream). This method
     * returns a `Promise` which resolves with no value (if the microphone recording starts successfully) or rejects
     * with an `Error` (if there is no permission to access the microphone or if the microphone was unplugged). If the
     * microphone recording has been already started, this method returns already resolved `Promise`.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Microphone.acquire
     */
    public func acquire() -> Promise<Void> {
        if (acquiringPromise == nil) {
            if (AVAudioSession.sharedInstance().recordPermission != .granted) {
                return Promise(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't acquire microphone: permission denied"]))
            }
            audioSource = peerConnectionFactory.audioSource(with: RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil))
            audioTrack = peerConnectionFactory.audioTrack(with: audioSource, trackId: UUID().uuidString)
            audioTrack.isEnabled = !muted
            fireAudioBuffer()
            acquiringPromise = Promise(())
        }
        return acquiringPromise
    }

    /**
     * Stops microphone recording. This is a synchronous operation which assumes revoking the previously distributed
     * microphone's audio and releasing the underlying microphone device. The stopping is idempotent: the method does
     * nothing if the microphone is not acquired, but it would fail if it was called in the middle of acquisition.
     *
     * - Tag: Microphone.release
     */
    public func release() {
        if (acquiringPromise != nil && audioTrack == nil) {
            preconditionFailure("Can't release microphone in the middle of acquisition")
        }
        if (audioTrack != nil) {
            for consumer in consumers {
                consumer.onAudioBuffer(nil)
            }
            audioTrack = nil
            audioSource = nil
        }
        acquiringPromise = nil
    }

    public func addAudioConsumer(_ consumer: MediaStream) {
        consumers.insert(consumer)
        if (audioTrack != nil) {
            consumer.onAudioBuffer(MediaStreamAudioBuffer(audioTrack, false))
        } else {
            consumer.onAudioBuffer(nil)
        }
    }

    public func removeAudioConsumer(_ consumer: MediaStream) {
        if (consumers.remove(consumer) != nil) {
            consumer.onAudioBuffer(nil)
        }
    }

    private func fireAudioBuffer() {
        for consumer in consumers {
            consumer.onAudioBuffer(MediaStreamAudioBuffer(audioTrack, false))
        }
    }

}
