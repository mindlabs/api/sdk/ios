import WebRTC

/**
 * ***
 * DeviceRegistry class provides access to all audio and video peripherals of the iOS device. It contains methods for
 * getting the [camera](x-source-tag://DeviceRegistry.getCamera) and the
 * [microphone](x-source-tag://DeviceRegistry.getMicrophone).
 *
 * Thread-Safety: all methods of DeviceRegistry class can be called on the main thread only. Calling these methods on
 * another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: DeviceRegistry
 */
public class DeviceRegistry {

    private let microphone: Microphone
    private let camera: Camera

    init(_ peerConnectionFactory: RTCPeerConnectionFactory) {
        self.microphone = Microphone(peerConnectionFactory)
        self.camera = Camera(peerConnectionFactory)
    }

    /**
     * Returns the [microphone](x-source-tag://Microphone) of the iOS device.
     *
     * - Returns: The microphone of the iOS device.
     *
     * - Tag: DeviceRegistry.getMicrophone
     */
    public func getMicrophone() -> Microphone {
        return microphone
    }

    /**
     * Returns the [camera](x-source-tag://Camera) of the iOS device.
     *
     * - Returns: The camera of the iOS device.
     *
     * - Tag: DeviceRegistry.getCamera
     */
    public func getCamera() -> Camera {
        return camera
    }

}
