import WebRTC

class DistortionRegistry {

    private var mtids: [String: Int64] = [:]
    private var distortions: [String: Distortion] = [:]
    private var messages: [String] = []

    init() {}

    func addMtid(_ mtid: String) {
        mtids[mtid] = Int64(Date().timeIntervalSince1970 * 1000)
    }

    func removeMtid(_ mtid: String) {
        mtids.removeValue(forKey: mtid)
    }

    func registerDistortion(_ mtid: String) {
        if (mtids[mtid] != nil && Int64(Date().timeIntervalSince1970 * 1000) - mtids[mtid]! > 3000) {
            let distortion: Distortion! = distortions[mtid]
            if (distortion != nil) {
                if (distortion.prolong()) {
                    return
                } else {
                    messages.append(String(describing: distortion))
                }
            }
            distortions[mtid] = Distortion(mtid)
        }
    }

    func report(_ dataChannel: RTCDataChannel) {
        for (msid, distortion) in distortions {
            if (distortion.isEnded()) {
                messages.append(String(describing: distortion))
                distortions.removeValue(forKey: msid)
            }
        }
        for message in messages {
            dataChannel.sendData(RTCDataBuffer(data: message.data(using: .utf8)!, isBinary: false))
        }
        messages.removeAll()
    }

}
