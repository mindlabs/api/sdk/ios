import Foundation

class Distortion: CustomStringConvertible {

    private let mtid: String
    private let timestamp: Int64
    private var duration: Int32

    init(_ mtid: String) {
        self.mtid = mtid
        self.timestamp = Int64(Date().timeIntervalSince1970 * 1000) - 1000 // A distortion is always reported with one-second delay
        self.duration = 1000
    }

    func isEnded() -> Bool {
        return Int64(Date().timeIntervalSince1970 * 1000) - (timestamp + Int64(duration)) > 1500
    }

    func prolong() -> Bool {
        if (!isEnded()) {
            duration = Int32((Int64(Date().timeIntervalSince1970 * 1000) - timestamp))
            return true
        } else {
            return false
        }
    }

    public var description: String {
        return "1:\(mtid):\(timestamp):\(Int32(round(Double(duration) / 1000.0)))"
    }

}
