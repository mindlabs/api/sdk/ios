/**
 * ***
 * Any audio source of [MediaStream](x-source-tag://MediaStream) is required to implement MediaStreamAudioSupplier
 * protocol. The protocol defines two methods:
 * [addAudioConsumer](x-source-tag://MediaStreamAudioSupplier.addAudioConsumer) and
 * [removeAudioConsumer](x-source-tag://MediaStreamAudioSupplier.removeAudioConsumer).
 * [MediaStream](x-source-tag://MediaStream) uses them to register and unregister itself as a consumer of audio from
 * the supplier. The supplier should use [onAudioBuffer](x-source-tag://MediaStream.onAudioBuffer) method of
 * [MediaStream](x-source-tag://MediaStream) class to supply its audio to every registered media stream. The supplier
 * is expected to supply its audio or `nil` value (if the audio isn't available yet) to every newly registering media
 * stream, and the `nil` value to every unregistering media stream.
 *
 * Thread-Safety: all methods of MediaStreamAudioSupplier class can be called on the main thread only. Calling these
 * methods on another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: MediaStreamAudioSupplier
 */
public protocol MediaStreamAudioSupplier: AnyObject {

    /**
     * Registers the specified [MediaStream](x-source-tag://MediaStream) as a consumer of audio from the supplier. It
     * is expected that during the registration the supplier will use
     * [onAudioBuffer](x-source-tag://MediaStream.onAudioBuffer) method of [MediaStream](x-source-tag://MediaStream)
     * class to supply its audio or `nil` value (if the audio isn't available yet) to the registering media stream.
     *
     * - Parameter consumer: The media stream which should be registered as a consumer of audio from the supplier.
     *
     * - Tag: MediaStreamAudioSupplier.addAudioConsumer
     */
    func addAudioConsumer(_ consumer: MediaStream)

    /**
     * Unregisters the specified [MediaStream](x-source-tag://MediaStream) as a consumer of audio from the supplier. It
     * is expected that during the unregistration the supplier will use
     * [onAudioBuffer](x-source-tag://MediaStream.onAudioBuffer) method of [MediaStream](x-source-tag://MediaStream)
     * class to supply the `nil` value to the unregistering media stream.
     *
     * - Parameter consumer: The media stream which should be unregistered as a consumer of audio from the supplier.
     *
     * - Tag: MediaStreamAudioSupplier.removeAudioConsumer
     */
    func removeAudioConsumer(_ consumer: MediaStream)

}
