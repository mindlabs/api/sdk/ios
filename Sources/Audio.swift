import Foundation

public class Audio: MediaStreamAudioConsumer {

    private var stream: MediaStream!
    private var volume: Double = 1.0

    public init() {}

    /**
     * Sets {@code MediaStream} that should used as a source of audio for the {@code Audio}. The `nil`
     * value can be used for stopping playing previously set {@code MediaStream}.
     */
    public func setMediaStream(_ stream: MediaStream!) {
        if (self.stream !== stream) {
            if (self.stream != nil) {
                self.stream.removeAudioConsumer(self)
            }
            if (stream == nil) {
                self.stream = nil
            } else {
                self.stream = stream
                stream.addAudioConsumer(self, volume)
            }
        }
    }

    /**
     * Sets the volume of the audio. The volume can be in range between 0.0 and 1.0, inclusively, where 0.0 means the
     * absolute silence, and 1.0 means the highest volume. The default volume is 1.0.
     *
     * - Parameter volume: The volume of the audio.
     */
    public func setVolume(_ volume: Double) {
        self.volume = volume
        if (stream != nil) {
            stream.addAudioConsumer(self, volume)
        }
    }

    public func onAudioBuffer(_ audioBuffer: MediaStreamAudioBuffer!, _ supplier: MediaStream) {}

    public func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(self))
    }

    public static func == (lhs: Audio, rhs: Audio) -> Bool {
        return lhs === rhs
    }

}
