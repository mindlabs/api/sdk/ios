/**
 * ***
 * SessionOptions class represents a set of configuration options for a {@link Session participation session}. The
 * default constructor creates an instance of SessionOptions class with the default values for all configuration
 * options. If necessary, you can change any default value before passing the instance to the static
 * [join](x-source-tag://MindSDK.join) method of [MindSDK](x-source-tag://MindSDK) class:
 *
 * ```
 * let conferenceURI = "https://api.mind.com/<APPLICATION_ID>/<CONFERENCE_ID>"
 * let participantToken = "<PARTICIPANT_TOKEN>"
 * let options = SessionOptions()
 * options.setStunServer("stun:stun.l.google.com:19302");
 * MindSDK.join(conferenceURI, participantToken, options).then({ session in
 *     ...
 * })
 * ```
 *
 * Thread-Safety: all methods of MindSDK class can be called on the main thread only.
 *
 * - Tag: SessionOptions
 */
public class SessionOptions {

    // TODO: Replace `Bool!` with `Bool` in Mind iOS SDK 6.0.0
    private var useVp9ForSendingVideo: Bool!
    private var stunServerURL: String!
    private var turnServerURL : String!
    private var turnServerUsername : String!
    private var turnServerPassword : String!

    public init() {
        self.useVp9ForSendingVideo = false
        self.stunServerURL = nil;
        self.turnServerURL = nil;
        self.turnServerUsername = nil;
        self.turnServerPassword = nil;
    }

    init(_ options: SessionOptions) {
        self.useVp9ForSendingVideo = options.isUseVp9ForSendingVideo()
        self.stunServerURL = options.getStunServerURL()
        self.turnServerURL = options.getTurnServerURL()
        self.turnServerUsername = options.getTurnServerUsername()
        self.turnServerPassword = options.getTurnServerPassword()
    }

    /**
     * Sets whether VP9 codec should be used for sending video or not. If `true` then any outgoing video will be
     * encoded with VP9 in SVC mode, otherwise — with VP8 in simulcast mode. The default value is `false`.
     *
     * - Parameter useVp9ForSendingVideo: Whether VP9 codec should be used for sending video or not.
     *
     * TODO: Replace `Bool!` with `Bool` in Mind iOS SDK 6.0.0
     *
     * - Tag: SessionOptions.setUseVp9ForSendingVideo
     */
    public func setUseVp9ForSendingVideo(_ useVp9ForSendingVideo: Bool!) {
        self.useVp9ForSendingVideo = useVp9ForSendingVideo
    }

    /**
     * Sets a STUN server which should be used for establishing a participation session. If it is set and if it is not
     * `nil`, then Mind iOS SDK will try to gather and use a reflexive Ice candidates for establishing the participant
     * session.
     *
     * - Parameter stunServerURL: The URL for connecting to the STUN server.
     *
     * - Tag: SessionOptions.setStunServer
     */
    public func setStunServer(_ stunServerURL: String!) {
        self.stunServerURL = stunServerURL
    }

    /**
     * Sets a TURN server which should be used for establishing a participation session. If it is set and if it is not
     * `nil`, then Mind iOS SDK will try to gather and use a relay Ice candidates for establishing the participant
     * session.
     *
     * - Parameter turnServerURL: The URL for connecting to the TURN server.
     * - Parameter turnServerUsername: The username for connecting to the TURN server.
     * - Parameter turnServerPassword: The password for connecting to the TURN server.
     *
     * - Tag: SessionOptions.setTurnServer
     */
    public func setTurnServer(_ turnServerURL: String!, _ turnServerUsername: String!, _ turnServerPassword: String!) {
        self.turnServerURL = turnServerURL
        self.turnServerUsername = turnServerUsername
        self.turnServerPassword = turnServerPassword
    }

    func isUseVp9ForSendingVideo() -> Bool! {
        return useVp9ForSendingVideo
    }

    func getStunServerURL() -> String! {
        return stunServerURL
    }

    func getTurnServerURL() -> String! {
        return turnServerURL
    }

    func getTurnServerUsername() -> String! {
        return turnServerUsername
    }

    func getTurnServerPassword() -> String! {
        return turnServerPassword
    }

}
