import Foundation

/**
 * ***
 * SessionStatistics class is used for representing statistics of a [participation session](x-source-tag://Session).
 * The statistics consists of instant measures of the underlying network connection of the session. The values of all
 * measures are updated about once a second. You can always get the latest statistics with a help of
 * [getStatistics](x-source-tag://Session.getStatistics) method of [Session](x-source-tag://Session) class:
 *
 * ```
 * let conferenceURI = "https://api.mind.com/<APPLICATION_ID>/<CONFERENCE_ID>"
 * let participantToken = "<PARTICIPANT_TOKEN>"
 * let options = SessionOptions()
 * MindSDK.join(conferenceURI, participantToken, options).then({ session in
 *     let sessionStatistics = session.getStatistics()
 * })
 * ```
 *
 * Thread-Safety: all methods of SessionStatistics class can be called on any thread.
 *
 *  - Tag: SessionStatistics
 */
public class SessionStatistics: CustomStringConvertible {

    private let timestamp: Int64
    private let protocoL: String
    private let localAddress: String
    private let localPort: UInt16
    private let remoteAddress: String
    private let remotePort: UInt16

    public init(_ protocoL: String, _ localAddress: String, _ localPort: UInt16, _ remoteAddress: String, _ remotePort: UInt16) {
        self.timestamp = Int64(Date().timeIntervalSince1970 * 1000)
        self.protocoL = protocoL
        self.localAddress = localAddress
        self.localPort = localPort
        self.remoteAddress = remoteAddress
        self.remotePort = remotePort
    }

    /**
     * Returns the creation timestamp of the statistics. The creation timestamp of the statistics is the number of
     * milliseconds that have elapsed between 1 January 1970 00:00:00 UTC and the time at which the statistics was
     * created.
     *
     * - Returns: The creation timestamp of the statistics.
     *
     * - Tag: SessionStatistics.getTimestamp
     */
    public func getTimestamp() -> Int64 {
        return timestamp
    }

    /**
     * Returns the protocol of the session. The protocol of the session is a protocol which currently is used for
     * transmitting data of the session over the network. There are two possible values for the protocol: "udp" and
     * "tcp".
     *
     * - Returns: The protocol of the session.
     *
     * - Tag: SessionStatistics.getProtocol
     */
    public func getProtocol() -> String {
        return protocoL
    }

    /**
     * Returns the local address of the session. The local address of the session is an IP address or a FQDN of the
     * client (Mind iOS SDK) which currently is used for transmitting data of the session over the network.
     *
     * - Returns: The local address of the session.
     *
     * - Tag: SessionStatistics.getLocalAddress
     */
    public func getLocalAddress() -> String {
        return localAddress
    }

    /**
     * Returns the local port of the session. The local port of the session is a port number on the client (Mind iOS
     * SDK) which currently is used for transmitting data of the session over the network.
     *
     * - Returns: The local port of the session.
     *
     * - Tag: SessionStatistics.getLocalPort
     */
    public func getLocalPort() -> UInt16 {
        return localPort
    }

    /**
     * Returns the remote address of the session. The remote address of the session is an IP address or a FQDN of the
     * server (Mind API) which currently is used for transmitting data of the session over the network.
     *
     * - Returns: The remote address of the session.
     *
     * - Tag: SessionStatistics.getRemoteAddress
     */
    public func getRemoteAddress() -> String {
        return remoteAddress
    }

    /**
     * Returns the remote port of the session. The remote port of the session is a port number on the server (Mind API)
     * which currently is used for transmitting data of the session over the network.
     *
     * - Returns: The remote port of the session.
     *
     * - Tag: SessionStatistics.getRemotePort
     */
    public func getRemotePort() -> UInt16 {
        return remotePort
    }

    public var description: String {
        return """
               {
                 timestamp: \(timestamp),
                 protocol: \(protocoL),
                 localAddress: \(localAddress),
                 localPort: \(localPort),
                 remoteAddress: \(remoteAddress),
                 remotePort: \(remotePort)
               }
               """.replacingOccurrences(of: "\"", with: "")

    }

}
