/**
 * ***
 * ParticipantRole enumerates all available roles that can be assigned to participants. The role defines a set of
 * permissions which the assignee is granted. If the local participant is a
 * [moderator](x-source-tag://ParticipantRole.MODERATOR) than you can change the role of the local participant with
 * [setRole](x-source-tag://Me.setRole) and the role of any remote participants with
 * [setRole](x-source-tag://Participant.setRole) method of [Participant](x-source-tag://Participant) class.
 *
 * ```
 * let me = conference.getMe()
 * // Turn all remote participants into SPEAKERs if we are permitted to do so
 * if me.getRole() == ParticipantRole.MODERATOR {
 *     for participant in conference.getParticipants() {
 *         participant.setRole(ParticipantRole.SPEAKER)
 *     }
 * }
 * ```
 *
 * - Tag: ParticipantRole
 */
public enum ParticipantRole: String, CustomStringConvertible {

    /**
     * The `ATTENDEE` can join and leave the conference, can change his own name, can send and receive messages, can
     * receive media streams (of the conference and other participants), finally it can stream primary and secondary
     * media streams, but neither of them is mixed into conference media stream and recording.
     *
     * - Tag: ParticipantRole.ATTENDEE
     */
    case ATTENDEE = "attendee"

    /**
     * The `SPEAKER` can do everything the `ATTENDEE` can, and also its primary media stream is mixed into conference
     * media stream and recording.
     *
     * - Tag: ParticipantRole.SPEAKER
     */
    case SPEAKER = "speaker"

    /**
     * The `PRESENTER` can do everything the `SPEAKER` can, and also its secondary media stream is mixed into
     * conference media stream and recording.
     *
     * - Tag: ParticipantRole.PRESENTER
     */
    case PRESENTER = "presenter"

    /**
     * The `MODERATOR` can do everything the `PRESENTER` can, and also it can change the name of the conference, can
     * change name and role of any participant, and finally it can start/stop and download recording of the conference.
     *
     * - Tag: ParticipantRole.MODERATOR
     */
    case MODERATOR = "moderator"

    static func fromString(_ string: String) -> ParticipantRole {
        return ParticipantRole(rawValue: string)!
    }

    public var description: String {
        return rawValue
    }

}
