import WebRTC
import Promises

/**
 * ***
 * Camera class is used for representing all cameras of the iOS device. iOS doesn't allow capturing multiple cameras
 * simultaneously, that's why Mind iOS SDK represents all cameras with a single
 * [multi-facing](x-source-tag://Camera.setFacing) instance of Camera class that can be got with
 * [getCamera](x-source-tag://DeviceRegistry.getCamera) method of [DeviceRegistry](x-source-tag://DeviceRegistry)
 * class. Camera class implements [MediaStreamVideoSupplier](x-source-tag://MediaStreamVideoSupplier) protocol, so it
 * can be used as a source of video for local [MediaStream](x-source-tag://MediaStream).
 *
 * ```
 * let deviceRegistry = MindSDK.getDeviceRegistry()
 * let camera = deviceRegistry.getCamera()
 * camera.set(facing: .USER)
 * camera.set(width: 1280, height: 720)
 * camera.set(fps: 25)
 * camera.set(bitrate: 2500000)
 * camera.set(adaptivity: 3)
 * let myStream = MindSDK.createMediaStream(nil, camera)
 * me.setMediaStream(myStream)
 * camera.acquire().catch({ error in
 *     print("Camera can't be acquired: \(error)")
 * })
 * ```
 *
 * Thread-Safety: all methods of Camera class can be called on the main thread only. Calling these methods on another
 * thread will result in an `Error` or undefined behavior.
 *
 * - Tag: Camera
*/
public class Camera: MediaStreamVideoSupplier {

    private var consumers = Set<MediaStream>()

    private let peerConnectionFactory: RTCPeerConnectionFactory

    private var videoSource: RTCVideoSource!
    private var cameraCapturer: RTCCameraVideoCapturer!
    private var videoTrack: RTCVideoTrack!
    private var acquiringPromise: Promise<Void>!

    private var facing: CameraFacing = .USER
    private var width: Int32 = 640
    private var height: Int32 = 360
    private var fps: Int = 30
    private var bitrate: Int = 1000000
    private var adaptivity: Int = 2
    private var scale: Double = 1.0

    init(_ peerConnectionFactory: RTCPeerConnectionFactory) {
        self.peerConnectionFactory = peerConnectionFactory
    }

    /**
     * Sets the facing of the camera. The facing of the camera is an actual camera device which the camera should use
     * for capturing video. The facing can be changed at any moment regardless whether the camera is acquired or not.
     *
     * - Parameter facing: The facing of the camera.
     *
     * - Tag: Camera.setFacing
     */
    public func set(facing: CameraFacing) {
        if (self.facing != facing) {
            self.facing = facing
            if (videoTrack != nil) {
                let captureFormat = chooseCaptureFormat();
                cameraCapturer.startCapture(with: getCamera(), format: captureFormat, fps: fps)
                fireVideoBuffer()
            }
        }
    }

    /**
     * Sets the resolution of the camera. The resolution of the camera is a resolution which the camera should capture
     * the video in. The video from the camera can be transmitted over the network in
     * [multiple encodings (e.g. resolutions) simultaneously](x-source-tag://Camera.setAdaptivity). The resolution can
     * be changed at any moment regardless whether the camera is acquired or not.
     *
     * - Parameter width: The horizontal resolution of the camera.
     * - Parameter height: The vertical resolution of the camera.
     *
     * - Tag: Camera.setResolution
     */
    public func set(width: Int32, height: Int32) {
        if (width <= 0 || width % 4 != 0) {
            preconditionFailure("Invalid width value: \(width)")
        }
        if (height <= 0 || height % 4 != 0) {
            preconditionFailure("Invalid height value: \(height)")
        }
        if (self.width != width || self.height != height) {
            self.width = width
            self.height = height
            if (videoTrack != nil) {
                let captureFormat = chooseCaptureFormat();
                cameraCapturer.startCapture(with: getCamera(), format: captureFormat, fps: fps)
                fireVideoBuffer()
            }
        }
    }

    /**
     * Sets the frame rate of the camera. The frame rate of the camera is a rate which the camera should capture the
     * video at. The frame rate can be changed at any moment regardless whether the camera is acquired or not.
     *
     * - Parameter fps: The frame rate of the camera.
     *
     * - Tag: Camera.setFps
     */
    public func set(fps: Int) {
        if (fps <= 0) {
            preconditionFailure("Invalid fps value: \(fps)")
        }
        if (self.fps != fps) {
            self.fps = fps
            if (videoTrack != nil) {
                let captureFormat = chooseCaptureFormat();
                cameraCapturer.startCapture(with: getCamera(), format: captureFormat, fps: fps)
                fireVideoBuffer()
            }
        }
    }

    /**
     * Sets the bitrate of the camera. The bitrate of the camera is a number of bits which each second of video from
     * the camera should not exceed while being transmitting over the network. The bitrate is shared among all
     * [encodings](x-source-tag://Camera.setAdaptivity) proportionally and can be changed at any moment regardless
     * whether the camera is acquired or not.
     *
     * - Parameter bitrate: The bitrate of the camera.
     *
     * - Tag: Camera.setBitrate
     */
    public func set(bitrate: Int) {
        if (bitrate <= 100000) {
            preconditionFailure("Invalid bitrate value: \(bitrate)")
        }
        if (self.bitrate != bitrate) {
            self.bitrate = bitrate
            if (videoTrack != nil) {
                fireVideoBuffer()
            }
        }
    }

    /**
     * Sets the adaptivity of the camera. The adaptivity of the camera is an integer (in range between 1 and 3,
     * inclusively) which defines a number of encodings in which the video from the camera should be transmitted over
     * the network. The encodings differ in resolution: the resolution of the first encoding equals the
     * [resolution of the camera](x-source-tag://Camera.setResolution), the resolution of the second encoding is half
     * (in each dimension) of the resolution of the first one, the resolution of the third encoding is half (in each
     * dimension) of the resolution of the second one. For example, if the resolution of the camera is 1280x720 and
     * adaptivity is 3, the video from the camera would be transmitted over the network in 3 encoding: 1280x720,
     * 640x360 and 320x180. The adaptivity can be changed at any moment regardless whether the camera is acquired or
     * not.
     *
     * - Parameter adaptivity: The adaptivity of the camera.
     *
     * - Tag: Camera.setAdaptivity
     */
    public func set(adaptivity: Int) {
        if (adaptivity < 1 || adaptivity > 3) {
            preconditionFailure("Invalid adaptivity value: \(adaptivity)")
        }
        if (self.adaptivity != adaptivity) {
            self.adaptivity = adaptivity
            if (videoTrack != nil) {
                fireVideoBuffer()
            }
        }
    }

    /**
     * Starts camera capturing. This is an asynchronous operation which assumes acquiring the underlying camera device
     * and distributing camera's video among all [consumers](x-source-tag://MediaStream). This method returns a
     * `Promise` which resolves with no value (if the camera capturing starts successfully) or rejects with an `Error`
     * (if there is no permission to access the camera or if the camera was unplugged). If the camera capturing has
     * been already started, this method returns already resolved `Promise`.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Camera.acquire
     */
    public func acquire() -> Promise<Void> {
        if (acquiringPromise == nil) {
            if (AVCaptureDevice.authorizationStatus(for: .video) != .authorized) {
                return Promise(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "Can't acquire camera: permission denied"]))
            }
            videoSource = peerConnectionFactory.videoSource()
            cameraCapturer = RTCCameraVideoCapturer(delegate: videoSource)
            let captureFormat = chooseCaptureFormat();
            cameraCapturer.startCapture(with: getCamera(), format: captureFormat, fps: fps)
            videoTrack = peerConnectionFactory.videoTrack(with: videoSource, trackId: UUID().uuidString)
            fireVideoBuffer()
            acquiringPromise = Promise(())
        }
        return acquiringPromise
    }

    /**
     * Stops camera capturing. This is a synchronous operation which assumes revoking the previously distributed
     * camera's video and releasing the underlying camera device. The stopping is idempotent: the method does nothing
     * if the camera is not acquired, but it would fail if it was called in the middle of acquisition.
     *
     * - Tag: Camera.release
     */
    public func release() {
        if (acquiringPromise != nil && videoTrack == nil) {
            preconditionFailure("Can't release camera in the middle of acquisition")
        }
        if (videoTrack != nil) {
            for consumer in consumers {
                consumer.onVideoBuffer(nil)
            }
            videoTrack = nil
            cameraCapturer.stopCapture()
            cameraCapturer = nil
            videoSource = nil
            scale = 1.0
        }
        acquiringPromise = nil
    }

    public func addVideoConsumer(_ consumer: MediaStream) {
        consumers.insert(consumer)
        if (videoTrack != nil) {
            consumer.onVideoBuffer(MediaStreamVideoBuffer(videoTrack, false, Int(width), Int(height), bitrate, adaptivity, scale))
        } else {
            consumer.onVideoBuffer(nil)
        }
    }

    public func removeVideoConsumer(_ consumer: MediaStream) {
        if (consumers.remove(consumer) != nil) {
            consumer.onVideoBuffer(nil)
        }
    }

    private func fireVideoBuffer() {
        for consumer in consumers {
            consumer.onVideoBuffer(MediaStreamVideoBuffer(videoTrack, false, Int(width), Int(height), bitrate, adaptivity, scale))
        }
    }

    private func getCamera() -> AVCaptureDevice {
        let cameras = RTCCameraVideoCapturer.captureDevices()
        for camera in cameras {
            if camera.position == (facing == .USER ? .front : .back) {
                return camera
            }
        }
        return cameras.first!
    }

    private func chooseCaptureFormat() -> AVCaptureDevice.Format {
        let formats = RTCCameraVideoCapturer.supportedFormats(for: getCamera())
        var chosenFormat: AVCaptureDevice.Format = formats.last!
        for format in formats.reversed() {
            let dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription)
            let frameRateRange = format.videoSupportedFrameRateRanges.first!
            if (Double(dimension.width) / Double(dimension.height) == Double(width) / Double(height) && width <= dimension.width && height <= dimension.height && frameRateRange.minFrameRate <= Double(fps) && Double(fps) <= frameRateRange.maxFrameRate) {
                // FIXME: We can choose a format only if its width and height exactly match `this.width` and
                // `this.height` or if its width and height are dividable by (2 ^ number_of_simulcast_layers) (i.e. by 8
                // in our case) without a remainder. Otherwise, WebRTC library could fail silently during initialization
                // of VP8 encoder, because `EncoderStreamFactory::CreateEncoderStreams` would produce streams with
                // wrong resolutions, so that `SimulcastUtility::ValidSimulcastParameters` would fail at
                // <https://webrtc.googlesource.com/src/+/master/modules/video_coding/utility/simulcast_utility.cc#47>
                // line.
                if (dimension.width == width && dimension.height == height || dimension.width % 8 == 0 && dimension.height % 8 == 0) {
                    chosenFormat = format;
                }
                chosenFormat = format
            }
        }
        scale = Double(CMVideoFormatDescriptionGetDimensions(chosenFormat.formatDescription).height) / Double(height)
        return chosenFormat
    }

}
