/**
 * ***
 * CameraFacing enumerates all available facings for [cameras](x-source-tag://Camera). The facing is a direction which
 * a camera can be pointed to. Front and back cameras on smartphones (and other mobile devices) are usually combined
 * into a single multi-facing camera which is represented with a single instance of [Camera](x-source-tag://Camera)
 * class. The facing of any multi-facing camera can be switched with [setFacing](x-source-tag://Camera.setFacing)
 * method of [Camera](x-source-tag://Camera) class.
 *
 * - Tag: CameraFacing
 */
public enum CameraFacing {

    /**
     * The `USER` facing is used for [pointing](x-source-tag://Camera.setFacing) a multi-facing camera to the user
     * (i.e. for switching to the front camera on a smartphone or other mobile device).
     *
     * - Tag: CameraFacing.USER
     */
    case USER

    /**
     * The `ENVIRONMENT` facing is used for [pointing](x-source-tag://Camera.setFacing) a multi-facing camera to the
     * environment (i.e. for switching to the back camera on a smartphone or other mobile device).
     *
     * - Tag: CameraFacing.ENVIRONMENT
     */
    case ENVIRONMENT

}
