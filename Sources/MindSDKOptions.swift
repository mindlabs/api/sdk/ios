/**
 * ***
 * MindSDKOptions class represents all available configuration options for Mind iOS SDK. The default initializer
 * creates an instance of MindSDKOptions class with the default values for all configuration options. If necessary, you
 * can change any default value before passing the instance to the static
 * [initialize](x-source-tag://MindSDK.initialize) method of [MindSDK](x-source-tag://MindSDK) class:
 *
 * - Tag: MindSDKOptions
 */
public class MindSDKOptions {

    private var useVp9ForSendingVideo: Bool

    public init() {
        self.useVp9ForSendingVideo = false
    }

    init(_ options: MindSDKOptions) {
        self.useVp9ForSendingVideo = options.isUseVp9ForSendingVideo()
    }

    func isUseVp9ForSendingVideo() -> Bool {
        return useVp9ForSendingVideo
    }

    /**
     * Sets whether VP9 codec should be used for sending video or not. If `true` then any outgoing video will be
     * encoded with VP9 in SVC mode, otherwise — with VP8 in simulcast mode. The default value is `false`.
     *
     * - Parameter useVp9ForSendingVideo: Whether VP9 codec should be used for sending video or not.
     *
     * - Tag: MindSDKOptions.setUseVp9ForSendingVideo
     */
    public func setUseVp9ForSendingVideo(_ useVp9ForSendingVideo: Bool) {
        self.useVp9ForSendingVideo = useVp9ForSendingVideo
    }

}
