import Foundation
import Promises

/**
 * ***
 * Participant class is used for representing remote participants (each instance represents a single remote
 * participants). The participant on behalf of whom iOS-application is participating in the conference (aka the local
 * participant) is represented with [Me](x-source-tag://Me) class. You can get representations of all remote
 * participants and a representation of a specific remote participant with
 * [getParticipants](x-source-tag://Conference.getParticipants) and
 * [getParticipantById](x-source-tag://Conference.getParticipantById) methods of
 * [Conference](x-source-tag://Conference) class, respectively. Participant class contains methods for getting and
 * setting parameters of the remote participant and for getting its primary and secondary media streams:
 *
 * ```
 * @IBOutlet var participantVideo: Video!
 * @IBOutlet var participantSecondaryVideo: Video!
 *
 * ...
 *
 * let participant = conference.getParticipantById("<PARTICIPANT_ID>")
 *
 * let participantStream = participant.getMediaStream()
 * self.participantVideo.setMediaStream(participantStream)
 *
 * let participantSecondaryStream = participant.getSecondaryMediaStream()
 * self.participantSecondaryVideo.setMediaStream(participantSecondaryStream)
 * ```
 *
 * Thread-Safety: all methods of Participant class can be called on the main thread only. Calling these methods on
 * another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: Participant
 */
public class Participant {

    class func fromDTO(_ session: Session, _ dto: [String: Any]) -> Participant {
        let media = dto["media"] as! [ String: Any]
        let secondaryMedia = dto["secondaryMedia"] as! [ String: Any]
        return Participant(session, dto["id"] as! String, dto["name"] as! String, dto["priority"] as! Double, ParticipantRole.fromString(dto["role"] as! String), media["audio"] as! Bool, media["video"] as! Bool, secondaryMedia["audio"] as! Bool, secondaryMedia["video"] as! Bool)
    }

    // Thread-Safety: all methods of `session` except `executeOnMainThread` are accessed on the main thread only
    let session: Session

    private let id: String
    var name: String
    var priority: Double
    var role: ParticipantRole
    private let mediaStream: MediaStream
    private let secondaryMediaStream: MediaStream

    init(_ session: Session, _ id: String, _ name: String, _ priority: Double, _ role: ParticipantRole, _ audio: Bool, _ video: Bool, _ secondaryAudio: Bool, _ secondaryVideo: Bool) {
        self.session = session
        self.id = id
        self.name = name
        self.priority = priority
        self.role = role
        self.mediaStream = MediaStream(id + "#primary", audio ? session.getWebRtcConnection() : nil, video ? session.getWebRtcConnection() : nil)
        self.secondaryMediaStream = MediaStream(id + "#secondary", secondaryAudio ? session.getWebRtcConnection() : nil, secondaryVideo ? session.getWebRtcConnection() : nil)
    }

    /**
     * Return the ID of the remote participant. The ID is unique and never changes.
     *
     * - Returns: The ID of the remote participant.
     *
     * - Tag: Participant.getId
     */
    public func getId() -> String {
        return id
    }

    /**
     * Returns the current name of the remote participant. The name of the remote participant can be shown above his
     * video in the conference media stream and recording.
     *
     * - Returns: The current name of the remote participant.
     *
     * - Tag: Participant.
     */
    public func getName() -> String {
        return name
    }

    /**
     * Changes the name of the remote participant. The name of the remote participant can be shown above his video in
     * the conference media stream and recording. The name changing is an asynchronous operation, that's why this
     * method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an
     * `Error` (if the operation fails). The operation can succeed only if the [local participant](x-source-tag://Me)
     * plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter name: The new name for the remote participant.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Participant.setName
     */
    @discardableResult public func setName(_ name: String) -> Promise<Void> {
        let requestDTO = [ "name":  name ]
        return session.newHttpPatch("/participants/" + self.id, requestDTO).then({ responseDTO in
            self.name = name
        })
    }

    /**
     * Returns the current priority of the remote participant. The priority defines a place which remote participant
     * takes in conference media stream and recording.
     *
     * - Returns: The current priority of the remote participant.
     *
     * - Tag: Participant.getPriority
     */
    public func getPriority() -> Double {
        return priority
    }

    /**
     * Changes the priority of the remote participant. The priority defines a place which remote participant takes in
     * conference media stream and recording. The priority changing is an asynchronous operation, that's why this
     * method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with an
     * `Error (if the operation fails). The operation can succeed only if the [local participant](x-source-tag://Me)
     * plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter priority: The new priority for the remote participant.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Participant.setPriority
     */
    @discardableResult public func setPriority(_ priority: Double) -> Promise<Void> {
        let requestDTO = [ "priority":  priority ]
        return session.newHttpPatch("/participants/" + self.id, requestDTO).then({ responseDTO in
            self.priority = priority
        })
    }

    /**
     * Returns the current [role](x-source-tag://ParticipantRole) of the remote participant. The role defines a set of
     * permissions which the remote participant is granted.
     *
     * - Returns: The current role of the remote participant.
     *
     * - Tag: Participant.getRole
     */
    public func getRole() -> ParticipantRole {
        return role
    }

    /**
     * Changes the [role](x-source-tag://ParticipantRole) of the remote participant. The role defines a set of
     * permissions which the remote participant is granted. The role changing is an asynchronous operation, that's why
     * this method returns a `Promise` that either resolves with no value (if the operation succeeds) or rejects with
     * an `Error (if the operation fails). The operation can succeed only if the [local participant](x-source-tag://Me)
     * plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter role: The new role for the remote participant.
     *
     * - Returns: The promise that either resolves with no value or rejects with an `Error`.
     *
     * - Tag: Participant.setRole
     */
    @discardableResult public func setRole(_ role: ParticipantRole) -> Promise<Void> {
        let requestDTO = [ "role":  role.rawValue ]
        return session.newHttpPatch("/participants/" + self.id, requestDTO).then({ responseDTO in
            self.role = role
        })
    }

    /**
     * Checks whether the remote participant is streaming primary audio (i.e. audio taken from his microphone). If both
     * this method and [isStreamingVideo](x-source-tag://Participant.isStreamingVideo) return `false` then the
     * participant is not streaming the primary media stream at all.
     *
     * - Returns: The boolean value which indicates if the remote participant is streaming primary audio or not.
     *
     * - Tag: Participant.isStreamingAudio
     */
    public func isStreamingAudio() -> Bool {
        return mediaStream.hasAudioSupplier()
    }

    /**
     * Checks whether the remote participant is streaming primary video (i.e. video taken from his camera). If both
     * this method and [isStreamingAudio](x-source-tag://Participant.isStreamingAudio) return `false` then the
     * participant is not streaming the primary media stream at all.
     *
     * - Returns: The boolean value which indicates if the remote participant is streaming primary video or not.
     *
     * - Tag: Participant.isStreamingVideo
     */
    public func isStreamingVideo() -> Bool {
        return mediaStream.hasVideoSupplier()
    }

    /**
     * Returns the [primary media stream](x-source-tag://MediaStream) of the remote participant. The primary media
     * stream is intended for streaming video and audio taken from a camera and a microphone of participant's iOS
     * device, respectively. You can get and play the primary media stream at any moment regardless of whether the
     * participant is streaming its primary video/audio or not: if the participant started or stopped streaming its
     * primary video or/and audio, the returned media stream would be updated automatically.
     *
     * - Returns: The primary media stream of the remote participant.
     *
     * - Tag: Participant.getMediaStream
     */
    public func getMediaStream() -> MediaStream {
        return mediaStream
    }

    /**
     * Checks whether the remote participant is streaming secondary audio (i.e. an arbitrary content with audio). If
     * both this method and [isStreamingSecondaryVideo](x-source-tag://Participant.isStreamingSecondaryVideo) return
     * `false` then the participant is not streaming secondary media stream at all.
     *
     * - Returns: The boolean value which indicates if the remote participant is streaming secondary audio or not.
     *
     * - Tag: Participant.isStreamingSecondaryAudio
     */
    public func isStreamingSecondaryAudio() -> Bool {
        return secondaryMediaStream.hasAudioSupplier()
    }

    /**
     * Checks whether the remote participant is streaming secondary video (i.e. an arbitrary content with video). If
     * both this method and [isStreamingSecondaryAudio](x-source-tag://Participant.isStreamingSecondaryAudio) return
     * `false` then the participant is not streaming secondary media stream at all.
     *
     * - Returns: The boolean value which indicates if the remote participant is streaming secondary video or not.
     *
     * - Tag: Participant.isStreamingSecondaryVideo
     */
    public func isStreamingSecondaryVideo() -> Bool {
        return secondaryMediaStream.hasVideoSupplier()
    }

    /**
     * Returns the [secondary media stream](x-source-tag://MediaStream) of the remote participant. The secondary media
     * stream is intended for streaming an arbitrary audio/video content, e.g. for sharing a screen of the
     * participant's iOS device. You can get and play the secondary media stream at any moment regardless of whether
     * the participant is streaming its secondary video/audio or not: if the participant started or stopped streaming
     * its secondary video or/and audio, the returned media stream would be updated automatically.
     *
     * - Returns: The secondary media stream of the remote participant.
     *
     * - Tag: Participant.getSecondaryMediaStream
     */
    public func getSecondaryMediaStream() -> MediaStream {
        return secondaryMediaStream
    }

    func update(_ dto: [String: Any]) {
        let name = dto["name"] as! String
        if (self.name != name) {
            self.name = name
            session.fireOnParticipantNameChanged(self)
        }
        let priority = dto["priority"] as! Double
        if (self.priority != priority) {
            self.priority = priority
            session.fireOnParticipantPriorityChanged(self)
        }
        let role = ParticipantRole.fromString(dto["role"] as! String)
        if (self.role != role) {
            self.role = role
            session.fireOnParticipantRoleChanged(self)
        }
        let media = dto["media"] as! [String: Any]
        if (mediaStream.hasAudioSupplier() != (media["audio"] as! Bool) || mediaStream.hasVideoSupplier() != (media["video"] as! Bool)) {
            mediaStream.setAudioSupplier(media["audio"] as! Bool ? session.getWebRtcConnection() : nil)
            mediaStream.setVideoSupplier(media["video"] as! Bool ? session.getWebRtcConnection() : nil)
            session.fireOnParticipantMediaChanged(self)
        }
        let secondaryMedia = dto["secondaryMedia"] as! [ String: Any]
        if (secondaryMediaStream.hasAudioSupplier() != (secondaryMedia["audio"] as! Bool) || secondaryMediaStream.hasVideoSupplier() != (secondaryMedia["video"] as! Bool)) {
            secondaryMediaStream.setAudioSupplier(secondaryMedia["audio"] as! Bool ? session.getWebRtcConnection() : nil)
            secondaryMediaStream.setVideoSupplier(secondaryMedia["video"] as! Bool ? session.getWebRtcConnection() : nil)
            session.fireOnParticipantSecondaryMediaChanged(self)
        }
    }

    func destroy() {
        mediaStream.setAudioSupplier(nil)
        mediaStream.setVideoSupplier(nil)
        secondaryMediaStream.setAudioSupplier(nil)
        secondaryMediaStream.setVideoSupplier(nil)
    }

}
