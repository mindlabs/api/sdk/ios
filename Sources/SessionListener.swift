import Foundation

/**
 * ***
 * SessionListener [can be used](x-source-tag://Session.setListener) for getting notifications of all events related
 * to the [Session](x-source-tag://Session). These include all changes in the [conference](x-source-tag://Conference),
 * [participants](x-source-tag://Participant) and [me](x-source-tag://Me) made by the server part of your application
 * and remote participants. For example, if one of the remote participants who played a role of a
 * [moderator](x-source-tag://ParticipantRole.MODERATOR) changed the name of the conference, then
 * [onConferenceNameChanged](x-source-tag://SessionListener.onConferenceNameChanged) method of the listener would be
 * called.
 *
 * Thread-Safety: all methods of SessionListener class are always called on the main thread.
 *
 * - Tag: SessionListener
 */
public protocol SessionListener {

    /**
     * This method is called when the state of the session is changed.
     *
     * - Parameter session: The session whose state was changed.
     *
     * - Tag: SessionListener.onSessionStateChanged
     */
    func onSessionStateChanged(_ session: Session)

    /**
     * This method is called when the name of the conference is changed by either the server part of your application
     * or one of the remote participants who plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter conference: The conference which name was changed.
     *
     * - Tag: SessionListener.onConferenceNameChanged
     */
    func onConferenceNameChanged(_ conference: Conference)

    /**
     * This method is called when the recording of the conference is started/resumed by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter conference: The conference the recording of which was started or resumed.
     *
     * - Tag: SessionListener.onConferenceRecordingStarted
     */
    func onConferenceRecordingStarted(_ conference: Conference)

    /**
     * This method is called when the recording of the conference is paused/stopped by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter conference: The conference the recording of which was paused or stopped.
     *
     * - Tag: SessionListener.onConferenceRecordingStopped
     */
    func onConferenceRecordingStopped(_ conference: Conference)

    /**
     * This method is called when the conference is ended by the server part of your application. By the time of
     * calling the conference object itself and all other objects related to the conference aren't functional (like
     * after a call of [exit](x-source-tag://MindSDK.exit2) method of [MindSDK](x-source-tag://MindSDK) class).
     *
     * - Parameter conference: The conference that was ended.
     *
     * - Tag: SessionListener.onConferenceEnded
     */
    func onConferenceEnded(_ conference: Conference)

    /**
     * This method is called when a remote participant joins the conference.
     *
     * - Parameter participant: The remote participant who joined the conference.
     *
     * - Tag: SessionListener.onParticipantJoined
     */
    func onParticipantJoined(_ participant: Participant)

    /**
     * This method is called when a remote participant leaves the conference because of his own will or because the
     * server part of your application expelled him.
     *
     * - Parameter participant: The remote participant who left the conference.
     *
     * - Tag: SessionListener.onParticipantExited
     */
    func onParticipantExited(_ participant: Participant)

    /**
     * This method is called when the name of a remote participant is changed by the server part of your application or
     * by one of the remote participants who plays a role of a [moderator](x-source-tag://ParticipantRole.MODERATOR) or
     * by the participant himself.
     *
     * - Parameter participant: The remote participant whose name was changed.
     *
     * - Tag: SessionListener.onParticipantNameChanged
     */
    func onParticipantNameChanged(_ participant: Participant)

    /**
     * This method is called when the priority of a remote participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter participant: The remote participant whose priority was changed.
     *
     * - Tag: SessionListener.onParticipantPriorityChanged
     */
    func onParticipantPriorityChanged(_ participant: Participant)

    /**
     * This method is called when the role of a remote participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter participant: The remote participant whose role was changed.
     *
     * - Tag: SessionListener.onParticipantRoleChanged
     */
    func onParticipantRoleChanged(_ participant: Participant)

    /**
     * This method is called when a remote participant starts or stops streaming his primary audio or/and video.
     *
     * - Parameter participant: The remote participant who started or stopped streaming his primary audio or/and video.
     *
     * - Tag: SessionListener.onParticipantMediaChanged
     */
    func onParticipantMediaChanged(_ participant: Participant)

    /**
     * This method is called when a remote participant starts or stops streaming his secondary audio or/and video.
     *
     * - Parameter participant: The remote participant who started or stopped streaming his secondary audio or/and
     *                          video.
     *
     * - Tag: SessionListener.onParticipantSecondaryMediaChanged
     */
    func onParticipantSecondaryMediaChanged(_ participant: Participant)

    /**
     * This method is called when the local participant is expelled from the conference by the server part of your
     * application. By the time of calling the conference object itself and all other objects related to the conference
     * aren't functional (like after a call of [exit](x-source-tag://MindSDK.exit2) method of
     * [MindSDK](x-source-tag://MindSDK) class).
     *
     * - Parameter me: The local participant who was expelled from the conference.
     *
     * - Tag: SessionListener.onMeExpelled
     */
    func onMeExpelled(_ me: Me)

    /**
     * This method is called when the name of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter me: The local participant whose name was changed.
     *
     * - Tag: SessionListener.onMeNameChanged
     */
    func onMeNameChanged(_ me: Me)

    /**
     * This method is called when the priority of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter me: The local participant whose priority was changed.
     *
     * - Tag: SessionListener.onMePriorityChanged
     */
    func onMePriorityChanged(_ me: Me)

    /**
     * This method is called when the role of the local participant is changed by either the server part of your
     * application or one of the remote participants who plays a role of a
     * [moderator](x-source-tag://ParticipantRole.MODERATOR).
     *
     * - Parameter me: The local participant whose role was changed.
     *
     * - Tag: SessionListener.onMeRoleChanged
     */
    func onMeRoleChanged(_ me: Me)

    /**
     * This method is called when the local participant receives a message from the server part of your application.
     *
     * - Parameter me: The local participant whose received the message.
     * - Parameter message: The text of the message.
     *
     * - Tag: SessionListener.onMeReceivedMessageFromApplication
     */
    func onMeReceivedMessageFromApplication(_ me: Me, _ message: String)

    /**
     * This method is called when the local participant receives a message from a remote participant.
     *
     * - Parameter me: The local participant whose received the message.
     * - Parameter message: The text of the message.
     * - Parameter participant: The remote participant who sent the message.
     *
     * - Tag: SessionListener.onMeReceivedMessageFromParticipant
     */
    func onMeReceivedMessageFromParticipant(_ me: Me, _ message: String, _ participant: Participant)

}

public extension SessionListener {

    func onSessionStateChanged(_ session: Session) {
        print("Session \(session.getId()) state changed: \(session.getState())")
    }

    func onConferenceNameChanged(_ conference: Conference) {
        print("Conference \(conference.getId()) name changed: " + conference.getName())
    }

    func onConferenceRecordingStarted(_ conference: Conference) {
        print("Conference \(conference.getId()) recording started")
    }

    func onConferenceRecordingStopped(_ conference: Conference) {
        print("Conference \(conference.getId()) recording stopped")
    }

    func onConferenceEnded(_ conference: Conference) {
        print("Conference \(conference.getId()) ended")
    }

    func onParticipantJoined(_ participant: Participant) {
        print("Participant \(participant.getId()) joined")
    }

    func onParticipantExited(_ participant: Participant) {
        print("Participant \(participant.getId()) exited")
    }

    func onParticipantNameChanged(_ participant: Participant) {
        print("Participant \(participant.getId()) name changed: \(participant.getName())")
    }

    func onParticipantPriorityChanged(_ participant: Participant) {
        print("Participant \(participant.getId()) priority changed: \(participant.getPriority())")
    }

    func onParticipantRoleChanged(_ participant: Participant) {
        print("Participant \(participant.getId()) role changed: \(participant.getRole())")
    }

    func onParticipantMediaChanged(_ participant: Participant) {
        print("Participant \(participant.getId()) media changed: \(participant.isStreamingAudio()), \(participant.isStreamingVideo())")
    }

    func onParticipantSecondaryMediaChanged(_ participant: Participant) {
        print("Participant \(participant.getId()) secondary media changed: \(participant.isStreamingSecondaryAudio()), \(participant.isStreamingSecondaryVideo())")
    }

    func onMeExpelled(_ me: Me) {
        print("Me \(me.getId()) expelled")
    }

    func onMeNameChanged(_ me: Me) {
        print("Me \(me.getId()) name changed: \(me.getName())")
    }

    func onMePriorityChanged(_ me: Me) {
        print("Me \(me.getId()) priority changed: \(me.getPriority())")
    }

    func onMeRoleChanged(_ me: Me) {
        print("Me \(me.getId()) role changed: \(me.getRole())")
    }

    func onMeReceivedMessageFromApplication(_ me: Me, _ message: String) {
        print("Me \(me.getId()) received message from the application: \(message)")
    }

    func onMeReceivedMessageFromParticipant(_ me: Me, _ message: String, _ participant: Participant) {
        print("Me \(me.getId()) received message from participant \(participant.getId()): \(message)")
    }

}
