import WebRTC

class WebRtcTransceiver {

    private let transceiver: RTCRtpTransceiver
    private let mediaType: RTCRtpMediaType
    private let sdpSectionIndex: Int

    init(_ transceiver: RTCRtpTransceiver, _ mediaType: RTCRtpMediaType, _ sdpSectionIndex: Int) {
        self.transceiver = transceiver
        self.mediaType = mediaType
        self.sdpSectionIndex = sdpSectionIndex
    }

    func getMediaType() -> RTCRtpMediaType {
        return mediaType
    }

    func getSdpSectionIndex() -> Int {
        return sdpSectionIndex
    }

    func getDirection() -> RTCRtpTransceiverDirection {
        return transceiver.direction
    }

    func setDirection(_ direction: RTCRtpTransceiverDirection) {
        transceiver.setDirection(direction, error: nil)
    }

    func getReceivingTrack() -> RTCMediaStreamTrack! {
        return transceiver.receiver.track
    }

    func getSendingTrack() -> RTCMediaStreamTrack! {
        return transceiver.sender.track
    }

    func setSendingTrack(_ track: RTCMediaStreamTrack!) {
        transceiver.sender.track = track
    }

    func getSendingParameters() -> RTCRtpParameters {
        return transceiver.sender.parameters
    }

    func setSendingParameters(_ parameters: RTCRtpParameters) {
        transceiver.sender.parameters = parameters
    }

    func setCodecPreferences(_ codecs: [RTCRtpCodecCapability]) {
        transceiver.setCodecPreferences(codecs)
    }

}
