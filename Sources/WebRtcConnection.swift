import Promises
import WebRTC

// Thread-Safety: any public method can be called on the main thread only
class WebRtcConnection: MediaStreamAudioConsumer, MediaStreamVideoConsumer, MediaStreamAudioSupplier, MediaStreamVideoSupplier, Hashable {

    private static let BACKGROUND_DISPATCH_QUEUE = DispatchQueue(label: "WebRtcConnection", qos: .background)
    private static let VP8_RTPMAP_PATTERN = try! NSRegularExpression(pattern: "\r\na=rtpmap:([0-9]+) VP8/90000")
    private static let VP9_RTPMAP_PATTERN = try! NSRegularExpression(pattern: "\r\na=rtpmap:([0-9]+) VP9/90000")

    // Thread-Safety: all methods of `session` except `executeOnMainThread` are accessed on the main thread only
    private let session: Session
    private let useVP9: Bool

    // Thread-Safety: accessed on the main thread only
    private var pc: RTCPeerConnection! // Thread-Safety: accessed on the background thread and on the main thread
    private var iceCandidatesGatheringCompletePromise: Promise<Void>!
    private var negotiationWorkItem: DispatchWorkItem!
    private var negotiations: Int32
    private var httpPost: URLSessionDataTask!
    private var dataChannel: RTCDataChannel!
    private var dataChannelOpened: Bool
    private var primaryPublication: WebRtcPublication
    private var secondaryPublication: WebRtcPublication
    private var lagging: Bool = false
    private var destroyed: Bool = false

    private var statisticsUpdatingWorkItem: DispatchWorkItem!
    private var oldStatistics: [String: Int64]!

    private var webRtcTransceiverLastSdpSectionIndex: Int = 1
    private var inactiveAudioTransceivers: NSMutableArray = []
    private var inactiveVideoTransceivers: NSMutableArray = []

    private var subscriptions: [MediaStream: WebRtcSubscription] = [:]

    private let distortionRegistry: DistortionRegistry

    init(_ session: Session) {
        self.session = session
        self.useVP9 = session.getOptions().isUseVp9ForSendingVideo() != nil ? session.getOptions().isUseVp9ForSendingVideo() : MindSDK.getOptions().isUseVp9ForSendingVideo()
        self.primaryPublication = WebRtcPublication()
        self.secondaryPublication = WebRtcPublication()
        self.negotiationWorkItem = nil
        self.negotiations = 0
        self.dataChannelOpened = false
        self.distortionRegistry = DistortionRegistry()
    }

    var onOpened: (() -> Void) = {}

    var onMessageReceived: ((String) -> Void) = { message in }

    var onStartedLagging: (() -> Void) = {}

    var onStoppedLagging: (() -> Void) = {}

    var onFailed: ((Error) -> Void) = { error in }

    var onClosed: ((Int) -> Void) = { code in }

    func open() {
        let onFailed: ((Error) -> Void) = { error in
            self.abort()
            self.onFailed(error)
        }
        if (pc == nil) {
            var iceServers: [RTCIceServer] = []
            var desiredIceCandidateTypes = Set<String>()
            if (session.getOptions().getStunServerURL() != nil) {
                desiredIceCandidateTypes.insert("srflx")
                iceServers.append(RTCIceServer(urlStrings: [session.getOptions().getStunServerURL()]))
            }
            if (session.getOptions().getTurnServerURL() != nil) {
                desiredIceCandidateTypes.insert("relay")
                iceServers.append(RTCIceServer(urlStrings: [session.getOptions().getTurnServerURL()], username: session.getOptions().getTurnServerUsername(), credential: session.getOptions().getTurnServerPassword()))
            }
            pc = MindSDK.createPeerConnection(iceServers)
            let PC = pc
            iceCandidatesGatheringCompletePromise = Promise.pending();
            pc.onConnectionStateChange = { connectionState in
                self.session.executeOnMainThread {
                    if (self.pc === PC) {
                        switch (connectionState) {
                            case .failed:
                                onFailed(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "Connection failed"]))
                            default:
                                break
                        }
                    }
                }
            }
            pc.onRenegotiationNeeded = {
                self.session.executeOnMainThread {
                    if (self.negotiationWorkItem == nil) {
                        self.negotiationWorkItem = self.session.scheduleOnMainThread({
                            if (self.pc === PC) {
                                self.negotiationWorkItem = nil
                                self.negotiations+=1
                                if (self.negotiations == 1) {
                                    self.pc.offer(for: RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil), completionHandler: { description, error in
                                        self.session.executeOnMainThread {
                                            if (self.pc === PC) {
                                                if let error = error {
                                                    self.session.executeOnMainThread {
                                                        onFailed(error)
                                                    }
                                                } else {
                                                    var localSdp = self.fixLocalSdpBeforeSetting(description!.sdp)
                                                    WebRtcConnection.BACKGROUND_DISPATCH_QUEUE.async {
                                                        if (self.pc === PC) {
                                                            self.pc.setLocalDescription(RTCSessionDescription(type: .offer, sdp: localSdp), completionHandler: { error in
                                                                self.session.executeOnMainThread {
                                                                    if (self.pc === PC) {
                                                                        if let error = error {
                                                                            onFailed(error)
                                                                        } else {
                                                                            self.iceCandidatesGatheringCompletePromise.then({
                                                                                if (self.pc === PC) {
                                                                                    localSdp = self.fixLocalSdpBeforeSending(self.pc.localDescription!.sdp)
                                                                                    let requestDTO = [ "sdp": localSdp ]
                                                                                    self.session.newHttpPost("/", requestDTO, { httpPost in self.httpPost = httpPost }).then({ responseDTO in
                                                                                        if (self.pc === PC) {
                                                                                            // It is important to pass to `fixRemoteSdp` method the same local SDP, which was set into `PeerConnection` object
                                                                                            // (but not the one that was modified with `fixLocalSdpBeforeSending` method afterward), because otherwise the simulcast
                                                                                            // attributes for inactive video sections wouldn't be removed properly from the remote SDP.
                                                                                            let remoteSdp = self.fixRemoteSdp(responseDTO["sdp"] as! String, self.pc.localDescription!.sdp)
                                                                                            let description = RTCSessionDescription(type: .answer, sdp: remoteSdp)
                                                                                            WebRtcConnection.BACKGROUND_DISPATCH_QUEUE.async {
                                                                                                if (self.pc === PC) {
                                                                                                    self.pc.setRemoteDescription(description, completionHandler: { error in
                                                                                                        self.session.executeOnMainThread {
                                                                                                            if (self.pc === PC) {
                                                                                                                if let error = error {
                                                                                                                    onFailed(error)
                                                                                                                } else {
                                                                                                                    self.negotiations-=1
                                                                                                                    if (self.negotiations > 0) {
                                                                                                                        self.negotiations = 0
                                                                                                                        self.pc.onRenegotiationNeeded()
                                                                                                                    } else {
                                                                                                                        if (self.lagging) {
                                                                                                                            self.lagging = false
                                                                                                                            self.onStoppedLagging()
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }).catch({ error in
                                                                                        if (self.pc === PC) {
                                                                                            if (self.dataChannelOpened && (error as NSError).code == NSURLErrorTimedOut) {
                                                                                                if (!self.lagging) {
                                                                                                    self.lagging = true
                                                                                                    self.onStartedLagging()
                                                                                                }
                                                                                                self.negotiations = 0
                                                                                                self.pc.onRenegotiationNeeded()
                                                                                            } else {
                                                                                                onFailed(error)
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                }
                                                                            }) // We catch nothing here because iceCandidatesGatheringCompletePromise can't fail
                                                                        }
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    })
                                }
                            }
                        }, 0)
                    }
                }
            }
            pc.onIceCandidate = { iceCandidate in
                self.session.executeOnMainThread {
                    if (self.pc === PC) {
                        desiredIceCandidateTypes.remove(iceCandidate.sdp.replacingOccurrences(of: ".* typ ([^ ]+).*", with: "$1", options: .regularExpression))
                        if (desiredIceCandidateTypes.isEmpty) {
                            self.iceCandidatesGatheringCompletePromise.fulfill(())
                        }
                    }
                }
            }
            pc.onIceGatheringStateChange = { iceGatheringState in
                self.session.executeOnMainThread {
                    if (self.pc === PC) {
                        if (iceGatheringState == .complete) {
                            self.iceCandidatesGatheringCompletePromise.fulfill(())
                        }
                    }
                }
            }
            dataChannel = pc.createDataChannel("", createDataChannelInit())
            dataChannel.onStateChange = { state in
                self.session.executeOnMainThread {
                    if (self.pc === PC) {
                        switch state {
                            case .open:
                                self.dataChannelOpened = true
                                self.open()
                                self.onOpened()
                            case .closed:
                                if (self.pc != nil) {
                                    onFailed(NSError(domain: "MindSDK", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data channel closed without closing code"]))
                                }
                            default:
                                break
                        }
                    }
                }
            }
            dataChannel.onMessage = { buffer in
                if (!buffer.isBinary) {
                    let message = String(data: buffer.data, encoding: String.Encoding.utf8)!
                    self.session.executeOnMainThread {
                        if (self.pc === PC) {
                            if (message == "4000" || message == "4001") {
                                self.onClosed(Int(message)!)
                            } else {
                                self.onMessageReceived(message)
                            }
                        }
                    }
                }
            }
            if (statisticsUpdatingWorkItem == nil) {
                if (primaryPublication.getStream() != nil) {
                    primaryPublication.getStream().resetStatistics()
                }
                if (secondaryPublication.getStream() != nil) {
                    secondaryPublication.getStream().resetStatistics()
                }
                for stream in subscriptions.keys {
                    stream.resetStatistics()
                }
                oldStatistics = nil
                updateStatistics()
                statisticsUpdatingWorkItem = session.scheduleOnMainThreadWithFixedDelay({ self.updateStatistics() }, 1000)
            }
        }
        if (dataChannelOpened) {
            if (primaryPublication.getAudioTransceiver() == nil) {
                webRtcTransceiverLastSdpSectionIndex += 1
                primaryPublication.setAudioTransceiver(WebRtcTransceiver(pc.addTransceiver(of: .audio, init: createAudioTransceiverInit())!, .audio, webRtcTransceiverLastSdpSectionIndex))
                primaryPublication.getAudioTransceiver().setCodecPreferences(MindSDK.getAudioCodecs())
            }
            if (primaryPublication.getVideoTransceiver() == nil) {
                webRtcTransceiverLastSdpSectionIndex += 1
                primaryPublication.setVideoTransceiver(WebRtcTransceiver(pc.addTransceiver(of: .video, init: createVideoTransceiverInit())!, .video, webRtcTransceiverLastSdpSectionIndex))
                primaryPublication.getVideoTransceiver().setCodecPreferences(MindSDK.getVideoCodecs())
            }
            if (secondaryPublication.getAudioTransceiver() == nil) {
                webRtcTransceiverLastSdpSectionIndex += 1
                secondaryPublication.setAudioTransceiver(WebRtcTransceiver(pc.addTransceiver(of: .audio, init: createAudioTransceiverInit())!, .audio, webRtcTransceiverLastSdpSectionIndex))
                secondaryPublication.getAudioTransceiver().setCodecPreferences(MindSDK.getAudioCodecs())
            }
            if (secondaryPublication.getVideoTransceiver() == nil) {
                webRtcTransceiverLastSdpSectionIndex += 1
                secondaryPublication.setVideoTransceiver(WebRtcTransceiver(pc.addTransceiver(of: .video, init: createVideoTransceiverInit())!, .video, webRtcTransceiverLastSdpSectionIndex))
                secondaryPublication.getVideoTransceiver().setCodecPreferences(MindSDK.getVideoCodecs())
            }
            if (isSendingPrimaryAudio()) {
                primaryPublication.getAudioTransceiver().setDirection(.sendOnly)
                primaryPublication.getAudioTransceiver().setSendingTrack(primaryPublication.getAudioBuffer().getTrack())
                configureAudioTransceiver(primaryPublication.getAudioTransceiver(), primaryPublication.getAudioBuffer(), primaryPublication.getStream())
            } else {
                primaryPublication.getAudioTransceiver().setDirection(.inactive)
                primaryPublication.getAudioTransceiver().setSendingTrack(nil)
            }
            if (isSendingPrimaryVideo()) {
                primaryPublication.getVideoTransceiver().setDirection(.sendOnly)
                primaryPublication.getVideoTransceiver().setSendingTrack(primaryPublication.getVideoBuffer().getTrack())
                configureVideoTransceiver(primaryPublication.getVideoTransceiver(), primaryPublication.getVideoBuffer(), primaryPublication.getStream())
            } else {
                primaryPublication.getVideoTransceiver().setDirection(.inactive)
                primaryPublication.getVideoTransceiver().setSendingTrack(nil)
            }
            if (isSendingSecondaryAudio()) {
                secondaryPublication.getAudioTransceiver().setDirection(.sendOnly)
                secondaryPublication.getAudioTransceiver().setSendingTrack(secondaryPublication.getAudioBuffer().getTrack())
                configureAudioTransceiver(secondaryPublication.getAudioTransceiver(), secondaryPublication.getAudioBuffer(), secondaryPublication.getStream())
            } else {
                secondaryPublication.getAudioTransceiver().setDirection(.inactive)
                secondaryPublication.getAudioTransceiver().setSendingTrack(nil)
            }
            if (isSendingSecondaryVideo()) {
                secondaryPublication.getVideoTransceiver().setDirection(.sendOnly)
                secondaryPublication.getVideoTransceiver().setSendingTrack(secondaryPublication.getVideoBuffer().getTrack())
                configureVideoTransceiver(secondaryPublication.getVideoTransceiver(), secondaryPublication.getVideoBuffer(), secondaryPublication.getStream())
            } else {
                secondaryPublication.getVideoTransceiver().setDirection(.inactive)
                secondaryPublication.getVideoTransceiver().setSendingTrack(nil)
            }
            for subscription in subscriptions.values {
                if (subscription.getAudioConsumer() != nil) {
                    if (subscription.getAudioTransceiver() == nil) {
                        subscription.setAudioTransceiver(acquireTransceiver(.audio))
                        subscription.getAudioConsumer().onAudioBuffer(MediaStreamAudioBuffer(subscription.getAudioTransceiver().getReceivingTrack() as! RTCAudioTrack, true))
                        subscription.getAudioTransceiver().setCodecPreferences(MindSDK.getAudioCodecs())
                    }
                } else {
                    if (subscription.getAudioTransceiver() != nil) {
                        releaseTransceiver(subscription.getAudioTransceiver())
                        subscription.setAudioTransceiver(nil)
                    }
                }
                if (subscription.getVideoConsumer() != nil) {
                    if (subscription.getVideoTransceiver() == nil) {
                        subscription.setVideoTransceiver(acquireTransceiver(.video))
                        subscription.getVideoConsumer().onVideoBuffer(MediaStreamVideoBuffer(subscription.getVideoTransceiver().getReceivingTrack() as! RTCVideoTrack, true, Int.max, Int.max, Int.max, 1, 1.0))
                        subscription.getVideoTransceiver().setCodecPreferences(MindSDK.getVideoCodecs())
                    }
                } else {
                    if (subscription.getVideoTransceiver() != nil) {
                        releaseTransceiver(subscription.getVideoTransceiver())
                        subscription.setVideoTransceiver(nil)
                    }
                }
            }
        }
        pc.onRenegotiationNeeded()
    }

    func close() {
        destroyed = true
        abort()
        if (statisticsUpdatingWorkItem != nil) {
            statisticsUpdatingWorkItem.cancel()
            statisticsUpdatingWorkItem = nil
        }
        primaryPublication.setAudioBuffer(nil)
        primaryPublication.setVideoBuffer(nil)
        if (primaryPublication.getStream() != nil) {
            primaryPublication.getStream().removeAudioConsumer(self)
            primaryPublication.getStream().removeVideoConsumer(self)
            primaryPublication.setStream(nil)
        }
        secondaryPublication.setAudioBuffer(nil)
        secondaryPublication.setVideoBuffer(nil)
        if (secondaryPublication.getStream() != nil) {
            secondaryPublication.getStream().removeAudioConsumer(self)
            secondaryPublication.getStream().removeVideoConsumer(self)
            secondaryPublication.setStream(nil)
        }
        subscriptions.removeAll()
    }

    func getPrimaryMediaStream() -> MediaStream! {
        return primaryPublication.getStream()
    }

    func setPrimaryMediaStream(_ primaryStream: MediaStream!) {
        if (destroyed) {
            preconditionFailure("WebRTC connection has been already destroyed")
        }
        if (primaryPublication.getStream() !== primaryStream) {
            if (primaryPublication.getStream() != nil) {
                primaryPublication.getStream().removeAudioConsumer(self)
                primaryPublication.getStream().removeVideoConsumer(self)
            }
            primaryPublication.setStream(primaryStream)
            if (primaryStream != nil) {
                primaryStream.addAudioConsumer(self, 1.0)
                primaryStream.addVideoConsumer(self)
            }
        }
    }

    func isSendingPrimaryAudio() -> Bool {
        return primaryPublication.getAudioBuffer() != nil
    }

    func isSendingPrimaryVideo() -> Bool {
        return primaryPublication.getVideoBuffer() != nil
    }

    func getSecondaryMediaStream() -> MediaStream! {
        return secondaryPublication.getStream()
    }

    func setSecondaryMediaStream(_ secondaryStream: MediaStream!) {
        if (destroyed) {
            preconditionFailure("WebRTC connection has been already destroyed")
        }
        if (secondaryPublication.getStream() !== secondaryStream) {
            if (secondaryPublication.getStream() != nil) {
                secondaryPublication.getStream().removeAudioConsumer(self)
                secondaryPublication.getStream().removeVideoConsumer(self)
            }
            secondaryPublication.setStream(secondaryStream)
            if (secondaryStream != nil) {
                secondaryStream.addAudioConsumer(self, 1.0)
                secondaryStream.addVideoConsumer(self)
            }
        }
    }

    func isSendingSecondaryAudio() -> Bool {
        return secondaryPublication.getAudioBuffer() != nil
    }

    func isSendingSecondaryVideo() -> Bool {
        return secondaryPublication.getVideoBuffer() != nil
    }

    func onAudioBuffer(_ audioBuffer: MediaStreamAudioBuffer!, _ supplier: MediaStream) {
        if (!destroyed) {
            if (supplier === primaryPublication.getStream()) {
                if (primaryPublication.getAudioBuffer() !== audioBuffer) {
                    primaryPublication.setAudioBuffer(audioBuffer)
                    open()
                }
            }
            if (supplier === secondaryPublication.getStream()) {
                if (secondaryPublication.getAudioBuffer() !== audioBuffer) {
                    secondaryPublication.setAudioBuffer(audioBuffer)
                    open()
                }
            }
        }
    }

    func onVideoBuffer(_ videoBuffer: MediaStreamVideoBuffer!, _ supplier: MediaStream) {
        if (!destroyed) {
            if (supplier === primaryPublication.getStream()) {
                if (primaryPublication.getVideoBuffer() !== videoBuffer) {
                    primaryPublication.setVideoBuffer(videoBuffer)
                    open()
                }
            }
            if (supplier === secondaryPublication.getStream()) {
                if (secondaryPublication.getVideoBuffer() !== videoBuffer) {
                    secondaryPublication.setVideoBuffer(videoBuffer)
                    open()
                }
            }
        }
    }

    func addAudioConsumer(_ consumer: MediaStream) {
        if (!destroyed) {
            var subscription: WebRtcSubscription! = subscriptions[consumer]
            if (subscription == nil) {
                subscription = WebRtcSubscription()
                subscriptions[consumer] = subscription
            }
            subscription.setAudioConsumer(consumer)
            distortionRegistry.addMtid("\(consumer.getLabel())#audio")
            open()
        }
    }

    func removeAudioConsumer(_ consumer: MediaStream) {
        if (!destroyed) {
            let subscription: WebRtcSubscription! = subscriptions[consumer]
            subscription.setAudioConsumer(nil)
            distortionRegistry.removeMtid("\(consumer.getLabel())#audio")
            consumer.onAudioBuffer(nil)
            open()
            if (subscription.getAudioConsumer() == nil && subscription.getVideoConsumer() == nil) {
                subscriptions[consumer] = nil
            }
        }
    }

    func addVideoConsumer(_ consumer: MediaStream) {
        if (!destroyed) {
            var subscription: WebRtcSubscription! = subscriptions[consumer]
            if (subscription == nil) {
                subscription = WebRtcSubscription()
                subscriptions[consumer] = subscription
            }
            subscription.setVideoConsumer(consumer)
            distortionRegistry.addMtid("\(consumer.getLabel())#video")
            open()
        }
    }

    func removeVideoConsumer(_ consumer: MediaStream) {
        if (!destroyed) {
            let subscription: WebRtcSubscription! = subscriptions[consumer]
            subscription.setVideoConsumer(nil)
            distortionRegistry.removeMtid("\(consumer.getLabel())#video")
            consumer.onVideoBuffer(nil)
            open()
            if (subscription.getAudioConsumer() == nil && subscription.getVideoConsumer() == nil) {
                subscriptions[consumer] = nil
            }
        }
    }

    private func abort() {
        if (negotiationWorkItem != nil) {
            negotiationWorkItem.cancel()
            negotiationWorkItem = nil
        }
        negotiations = 0
        if (dataChannel != nil) {
            if (destroyed) {
                dataChannel.close()
            }
            dataChannel = nil
            dataChannelOpened = false
        }
        if (pc != nil) {
            let pc = self.pc!
            // We close PeerConnection on the background thread (i.e. out of the main thread) because it the closing could take significant amount of time (several hundreds ms).
            WebRtcConnection.BACKGROUND_DISPATCH_QUEUE.async { pc.close() }
            self.pc = nil
            iceCandidatesGatheringCompletePromise = nil
            primaryPublication.setAudioTransceiver(nil)
            primaryPublication.setVideoTransceiver(nil)
            secondaryPublication.setAudioTransceiver(nil)
            secondaryPublication.setVideoTransceiver(nil)
            for subscription in subscriptions.values {
                if (subscription.getAudioConsumer() != nil) {
                    subscription.getAudioConsumer().onAudioBuffer(nil)
                }
                if (subscription.getVideoConsumer() != nil) {
                    subscription.getVideoConsumer().onVideoBuffer(nil)
                }
                subscription.setAudioTransceiver(nil)
                subscription.setVideoTransceiver(nil)
            }
            inactiveAudioTransceivers.removeAllObjects()
            inactiveVideoTransceivers.removeAllObjects()
            webRtcTransceiverLastSdpSectionIndex = 1
        }
        if (httpPost != nil) {
            httpPost.cancel()
            httpPost = nil
        }
        lagging = false
        if (oldStatistics != nil) {
            if (primaryPublication.getStream() != nil) {
                primaryPublication.getStream().resetStatistics()
            }
            if (secondaryPublication.getStream() != nil) {
                secondaryPublication.getStream().resetStatistics()
            }
            for stream in subscriptions.keys {
                stream.resetStatistics()
            }
            oldStatistics = nil
            updateStatistics()
        }
    }

    private func updateStatistics() {
        let newStatistics: [String: Int64] = [:]
        if (oldStatistics == nil) {
            session.updateStatistics(nil)
            if (primaryPublication.getStream() != nil) {
                primaryPublication.getStream().updateStatistics(nil)
            }
            if (secondaryPublication.getStream() != nil) {
                secondaryPublication.getStream().updateStatistics(nil)
            }
            for stream in subscriptions.keys {
                stream.updateStatistics(nil)
            }
            oldStatistics = newStatistics
        } else {
            let PC = pc
            let gettingStatsPromise = Promise<RTCStatisticsReport?>.pending()
            if (!dataChannelOpened) {
                gettingStatsPromise.fulfill(nil)
            } else {
                pc.statistics { report in
                    gettingStatsPromise.fulfill(report)
                }
            }
            gettingStatsPromise.then({ report in
                if (self.pc == PC) {
                    var sessionReport: [RTCStatistics] = []
                    var streamReportMap: [MediaStream: [RTCStatistics]]  = [:]
                    if (report != nil) {
                        if (self.primaryPublication.getStream() != nil) {
                            streamReportMap[self.primaryPublication.getStream()] = []
                        }
                        if (self.secondaryPublication.getStream() != nil) {
                            streamReportMap[self.secondaryPublication.getStream()] = []
                        }
                        for stream in self.subscriptions.keys {
                            streamReportMap[stream] = []
                        }
                        var selectedCandidatePair: RTCStatistics! = nil
                        var localCandidate: RTCStatistics! = nil
                        var remoteCandidate: RTCStatistics! = nil
                        for stats in report!.statistics.values {
                            switch ("\(stats.values["kind"] as? String ?? "nil")-\(stats.type)") {
                                case "nil-transport":
                                    if (stats.values["selectedCandidatePairId"] != nil) {
                                        selectedCandidatePair = report!.statistics[stats.values["selectedCandidatePairId"] as! String]
                                        if (selectedCandidatePair != nil) {
                                            localCandidate = report!.statistics[selectedCandidatePair.values["localCandidateId"] as! String]
                                            remoteCandidate = report!.statistics[selectedCandidatePair.values["remoteCandidateId"] as! String]
                                        }
                                    }
                                case "audio-media-source",
                                     "video-media-source",
                                     "audio-outbound-rtp",
                                     "video-outbound-rtp",
                                     "audio-remote-inbound-rtp",
                                     "video-remote-inbound-rtp",
                                     "audio-inbound-rtp",
                                     "video-inbound-rtp":
                                    let stream = self.getMediaStreamForStats(stats, report!)
                                    if (stream != nil) {
                                        streamReportMap[stream!]!.append(stats)
                                    }
                                    break
                                default:
                                    break
                            }
                        }
                        if (selectedCandidatePair != nil) {
                            sessionReport.append(selectedCandidatePair)
                            sessionReport.append(localCandidate)
                            sessionReport.append(remoteCandidate)
                            for var stats in streamReportMap.values {
                                stats.append(selectedCandidatePair)
                            }
                        }
                    }
                    self.session.updateStatistics(sessionReport)
                    if (self.primaryPublication.getStream() != nil) {
                        self.primaryPublication.getStream().updateStatistics(streamReportMap[self.primaryPublication.getStream()])
                    }
                    if (self.secondaryPublication.getStream() != nil) {
                        self.secondaryPublication.getStream().updateStatistics(streamReportMap[self.secondaryPublication.getStream()])
                    }
                    for stream in self.subscriptions.keys {
                        stream.updateStatistics(streamReportMap[stream])
                    }
                    if (report != nil) {
                        for subscription in self.subscriptions.values {
                            if (subscription.getAudioConsumer() != nil && subscription.getAudioConsumer().getAudioStatistics().getRate() < 40_000) {
                                self.distortionRegistry.registerDistortion("\(subscription.getAudioConsumer().getLabel())#audio")
                            }
                            if (subscription.getVideoConsumer() != nil && subscription.getVideoConsumer().getVideoStatistics().getRate() < 1) {
                                self.distortionRegistry.registerDistortion("\(subscription.getVideoConsumer().getLabel())#video")
                            }
                        }
                    }
                    if (self.dataChannelOpened) {
                        self.distortionRegistry.report(self.dataChannel)
                    }
                    self.oldStatistics = newStatistics
                }
            })
        }
    }

    private func getMediaStreamForStats(_ stats: RTCStatistics, _ report: RTCStatisticsReport ) -> MediaStream! {
        let mid: String! = stats.values["mid"] as? String
        if (mid != nil) { // if the stats has an associated `mid` (e.g. `inbound-rtp` and `outbound-rtp`)
            let sdpSectionIndex = Int(mid)! + 1
            for publication in [ primaryPublication, secondaryPublication ] {
                if (publication.getAudioTransceiver() != nil && publication.getAudioTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return publication.getStream()
                }
                if (publication.getVideoTransceiver() != nil && publication.getVideoTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return publication.getStream()
                }
            }
            for subscription in subscriptions.values {
                if (subscription.getAudioTransceiver() != nil && subscription.getAudioTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return subscription.getAudioConsumer()
                }
                if (subscription.getVideoTransceiver() != nil && subscription.getVideoTransceiver().getSdpSectionIndex() == sdpSectionIndex) {
                    return subscription.getVideoConsumer()
                }
            }
        }
        let trackIdentifier: String! = stats.values["trackIdentifier"] as? String
        if (trackIdentifier != nil) { // if the stats doesn't have an associated `mid` (i.e. `media-source`)
            for publication in [ primaryPublication, secondaryPublication ] {
                if (publication.getAudioTransceiver() != nil && publication.getAudioTransceiver().getSendingTrack() != nil && publication.getAudioTransceiver().getSendingTrack().trackId == trackIdentifier) {
                    return publication.getStream()
                }
                if (publication.getVideoTransceiver() != nil && publication.getVideoTransceiver().getSendingTrack() != nil && publication.getVideoTransceiver().getSendingTrack().trackId == trackIdentifier) {
                    return publication.getStream()
                }
            }
            for subscription in subscriptions.values {
                if (subscription.getAudioTransceiver() != nil && subscription.getAudioTransceiver().getReceivingTrack() != nil && subscription.getAudioTransceiver().getReceivingTrack().trackId == trackIdentifier) {
                    return subscription.getAudioConsumer()
                }
                if (subscription.getVideoTransceiver() != nil && subscription.getVideoTransceiver().getReceivingTrack() != nil && subscription.getVideoTransceiver().getReceivingTrack().trackId == trackIdentifier) {
                    return subscription.getVideoConsumer()
                }
            }
        }
        let localId: String! = stats.values["localId"] as? String
        if (localId != nil) { // if the stats has only reference to another stats through `localId` (e.g. any `remote-inbound-rtp` stats has only a reference to corresponding `outbound-rtp` stats)
            return getMediaStreamForStats(report.statistics[localId]!, report)
        }
        return nil
    }

    private func acquireTransceiver(_ mediaType: RTCRtpMediaType) -> WebRtcTransceiver {
        let inactiveTransceivers = mediaType == .audio ? inactiveAudioTransceivers : inactiveVideoTransceivers
        if (inactiveTransceivers.count > 0) {
            let transceiver = inactiveTransceivers.lastObject as! WebRtcTransceiver
            inactiveTransceivers.removeLastObject();
            transceiver.setDirection(.recvOnly)
            return transceiver
        } else {
            let recvOnly = RTCRtpTransceiverInit()
            recvOnly.direction = .recvOnly
            webRtcTransceiverLastSdpSectionIndex += 1
            return WebRtcTransceiver(pc.addTransceiver(of: mediaType, init: recvOnly)!, mediaType, webRtcTransceiverLastSdpSectionIndex)
        }
    }

    private func releaseTransceiver(_ transceiver: WebRtcTransceiver) {
        let inactiveTransceivers = transceiver.getMediaType() == .audio ? inactiveAudioTransceivers : inactiveVideoTransceivers
        transceiver.setDirection(.inactive)
        inactiveTransceivers.add(transceiver)
    }

    private func createAudioTransceiverInit() -> RTCRtpTransceiverInit {
        let audioInit = RTCRtpTransceiverInit()
        audioInit.direction = .inactive
        return audioInit
    }

    private func createVideoTransceiverInit() -> RTCRtpTransceiverInit {
        let videoTransceiverInit = RTCRtpTransceiverInit()
        videoTransceiverInit.direction = .inactive
        if (useVP9) {
            let rtpEncodingParameters = RTCRtpEncodingParameters()
            rtpEncodingParameters.isActive = false
            rtpEncodingParameters.scaleResolutionDownBy = 1.0
            rtpEncodingParameters.bitratePriority = 1.0
            rtpEncodingParameters.networkPriority = .low
            videoTransceiverInit.sendEncodings = [ rtpEncodingParameters ]
            return videoTransceiverInit
        } else {
            let lowRtpEncodingParameters = RTCRtpEncodingParameters()
            lowRtpEncodingParameters.rid = "l"
            lowRtpEncodingParameters.isActive = false
            lowRtpEncodingParameters.scaleResolutionDownBy = 4.0
            lowRtpEncodingParameters.bitratePriority = 1.0
            lowRtpEncodingParameters.networkPriority = .low
            let mediumRtpEncodingParameters = RTCRtpEncodingParameters()
            mediumRtpEncodingParameters.rid = "m"
            mediumRtpEncodingParameters.isActive = false
            mediumRtpEncodingParameters.scaleResolutionDownBy = 2.0
            mediumRtpEncodingParameters.bitratePriority = 1.0
            mediumRtpEncodingParameters.networkPriority = .low
            let highRtpEncodingParameters = RTCRtpEncodingParameters()
            highRtpEncodingParameters.rid = "h"
            highRtpEncodingParameters.isActive = false
            highRtpEncodingParameters.scaleResolutionDownBy = 1.0
            highRtpEncodingParameters.bitratePriority = 1.0
            highRtpEncodingParameters.networkPriority = .low
            videoTransceiverInit.sendEncodings = [ lowRtpEncodingParameters, mediumRtpEncodingParameters, highRtpEncodingParameters ]
            return videoTransceiverInit
        }
    }

    private func createDataChannelInit() -> RTCDataChannelConfiguration {
        let configuration = RTCDataChannelConfiguration()
        configuration.isOrdered = true
        configuration.isNegotiated = true
        configuration.channelId = 1
        return configuration
    }

    private func configureAudioTransceiver(_ audioTransceiver: WebRtcTransceiver, _ audioBuffer: MediaStreamAudioBuffer, _ stream: MediaStream) {}

    private func configureVideoTransceiver(_ videoTransceiver: WebRtcTransceiver, _ videoBuffer: MediaStreamVideoBuffer, _ stream: MediaStream) {
        let parameters = videoTransceiver.getSendingParameters()
        let adaptivityToBitrateFractionMap = [ 1, 5, 21 ] // 1, 1 + 4, 1 + 4 + 16
        let adaptivityToResolutionScaleMap = [ 4, 2, 1 ]
        if (useVP9) {
            let maxBitrateFraction = Int(stream.getMaxVideoBitrate()) / (videoBuffer.getBitrate() / adaptivityToBitrateFractionMap[videoBuffer.getAdaptivity() - 1])
            var spatialLayersCount = 1
            for i in 1..<videoBuffer.getAdaptivity() {
                if (adaptivityToBitrateFractionMap[i] <= maxBitrateFraction && videoBuffer.getWidth() * videoBuffer.getHeight() / Int(pow(Double(adaptivityToResolutionScaleMap[i]), Double(2))) <= stream.getMaxVideoFrameArea()) {
                    spatialLayersCount += 1
                } else {
                    break
                }
            }
            parameters.encodings[0].isActive = true
            parameters.encodings[0].scaleResolutionDownBy = NSNumber(value: videoBuffer.getScale() * powl(2, Double(videoBuffer.getAdaptivity() - spatialLayersCount)))
            parameters.encodings[0].maxBitrateBps = NSNumber(value: min(videoBuffer.getBitrate(), Int(stream.getMaxVideoBitrate())))
            parameters.encodings[0].scalabilityMode = spatialLayersCount > 1 ? "L\(spatialLayersCount)T3_KEY" : "L1T3"
            parameters.encodings[0].maxFramerate = stream.getMaxVideoFrameRate() == Int32.max ? nil : NSNumber(value: stream.getMaxVideoFrameRate())
        } else {
            for i in 0..<parameters.encodings.count {
                parameters.encodings[i].isActive = false
                parameters.encodings[i].scaleResolutionDownBy = 1
                parameters.encodings[i].maxBitrateBps = 0
            }
            var totalMaxBitrate = 0
            for i in 0..<videoBuffer.getAdaptivity() {
                let maxBitrate = videoBuffer.getBitrate() / adaptivityToBitrateFractionMap[videoBuffer.getAdaptivity() - 1] * NSDecimalNumber(decimal: pow(4, i)).intValue
                totalMaxBitrate += maxBitrate
                if (i == 0 || (totalMaxBitrate <= stream.getMaxVideoBitrate() && videoBuffer.getWidth() * videoBuffer.getHeight() / Int(pow(Double(adaptivityToResolutionScaleMap[i]), Double(2))) <= stream.getMaxVideoFrameArea())) {
                    parameters.encodings[i].isActive = true
                    parameters.encodings[i].scaleResolutionDownBy = NSNumber(value: videoBuffer.getScale() * powl(2, Double(videoBuffer.getAdaptivity() - 1 - i)))
                    parameters.encodings[i].maxBitrateBps = NSNumber(value: maxBitrate)
                    parameters.encodings[i].scalabilityMode = "L1T3"
                    parameters.encodings[i].maxFramerate = stream.getMaxVideoFrameRate() == Int32.max ? nil : NSNumber(value: stream.getMaxVideoFrameRate())
                }
            }
        }
        // FIXME: In case of CPU overloading we prefer VP8 encoder to maintain resolution because otherwise if `scale`
        // didn't equal `1.0` the encoder would fail during the reinitialization due to
        // <https://webrtc.googlesource.com/src/+/master/modules/video_coding/utility/simulcast_utility.cc#42>.
        parameters.degradationPreference = NSNumber(value: RTCDegradationPreference.maintainResolution.rawValue)
        primaryPublication.getVideoTransceiver().setSendingParameters(parameters)
    }

    private func fixLocalSdpBeforeSetting(_ localSdp: String) -> String {
        var localSdpSections = localSdp.replacingOccurrences(of: "\r\nm=", with: "\r\n\0m=").components(separatedBy: "\0")
        for i in 2..<localSdpSections.count {
            // WebRTC library silently supports RTCP Extended Reports with Receiver Reference Time parameters. These
            // reports help non-senders to estimate their round trip time (see RFC3611). In order to enable such
            // reports, we have to add manually `a=rtcp-fb:* rrtr` line to each media section of the offer SDP befor
            // setting it to `PeerConnection`. Once enabled (and confirmed in the answer SDP), there is no need to add
            // such lines manually again because the library will keep them in the following offers.
            if (localSdpSections[i].range(of: "\r\na=rtcp-fb:[^ ]+ rrtr", options: .regularExpression) == nil) {
                localSdpSections[i] = localSdpSections[i] + "a=rtcp-fb:* rrtr\r\n";
            }
        }
        return localSdpSections.joined()
    }

    private func fixLocalSdpBeforeSending(_ localSdp: String) -> String {
        var localSdpSections = localSdp.replacingOccurrences(of: "\r\na=candidate:.*typ host.*", with: "", options: .regularExpression)
                                       .replacingOccurrences(of: "\r\nm=", with: "\r\n\0m=").components(separatedBy: "\0")
        if (dataChannelOpened) {
            for i in 2..<min(6, localSdpSections.count) {
                localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=msid:.*", with: "", options: .regularExpression)
                if (i == 2 && primaryPublication.getAudioTransceiver().getSendingTrack() != nil) {
                    localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=mid:.*", with: "$0\r\na=msid:me#primary \(primaryPublication.getAudioTransceiver().getSendingTrack()!.trackId.lowercased())", options: .regularExpression)
                }
                if (i == 3 && primaryPublication.getVideoTransceiver().getSendingTrack() != nil) {
                    localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=mid:.*", with: "$0\r\na=msid:me#primary \(primaryPublication.getVideoTransceiver().getSendingTrack()!.trackId.lowercased())", options: .regularExpression)
                }
                if (i == 4 && secondaryPublication.getAudioTransceiver().getSendingTrack() != nil) {
                    localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=mid:.*", with: "$0\r\na=msid:me#secondary \(secondaryPublication.getAudioTransceiver().getSendingTrack()!.trackId.lowercased())", options: .regularExpression)
                }
                if (i == 5 && secondaryPublication.getVideoTransceiver().getSendingTrack() != nil) {
                    localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=mid:.*", with: "$0\r\na=msid:me#secondary \(secondaryPublication.getVideoTransceiver().getSendingTrack().trackId.lowercased())", options: .regularExpression)
                }
                if (!useVP9 && (i == 3 || i == 5)) {
                    // FIXME: Mind API doesn't support switching simulcast on/off on the fly, but WebRTC library doesn't
                    // add simulcast attributes to video sections which are initialized in `inactive` state, that's why we
                    // have to add all necessary simulcast attributes to the offer SDP, if they are missing.
                    if (!localSdpSections[i].contains("\r\na=simulcast")) {
                        localSdpSections[i] = localSdpSections[i] + "a=rid:l send\r\na=rid:m send\r\na=rid:h send\r\na=simulcast:send ~l;~m;h\r\n"
                    }
                }
            }
            for i in  6..<localSdpSections.count {
                localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=msid:.*", with: "", options: .regularExpression)
            }
            for subscription in self.subscriptions.values {
                if (subscription.getAudioConsumer() != nil && subscription.getAudioTransceiver() != nil) {
                    let i = subscription.getAudioTransceiver().getSdpSectionIndex()
                    if (i < localSdpSections.count) {
                        localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=mid:.*", with: "$0\r\na=msid:\(subscription.getAudioConsumer().getLabel()) \(subscription.getAudioTransceiver().getReceivingTrack()!.trackId)", options: .regularExpression)
                    }
                }
                if (subscription.getVideoConsumer() != nil && subscription.getVideoTransceiver() != nil) {
                    let i = subscription.getVideoTransceiver().getSdpSectionIndex()
                    if (i < localSdpSections.count) {
                        localSdpSections[i] = localSdpSections[i].replacingOccurrences(of: "\r\na=mid:.*", with: "$0\r\na=msid:\(subscription.getVideoConsumer().getLabel()) \(subscription.getVideoTransceiver().getReceivingTrack()!.trackId)", options: .regularExpression)
                        // FIXME: Since WebRTC provides no API for choosing maximum bitrate of the receiving video, we have to
                        // munge the offer SDP and add `b=TIAS` attribute for video section manually to set the maximum bitrate
                        // of the video which we are going to receive.
                        if (subscription.getVideoConsumer().getMaxVideoBitrate() < Int32.max) {
                            localSdpSections[i] = localSdpSections[i] + "b=TIAS:\(subscription.getVideoConsumer().getMaxVideoBitrate())\r\n"
                        }
                        // We enforce maximum frame area and frame rate of the receiving video through the custom
                        // video-level `x-preferences` attribute in our offer SDP.
                        localSdpSections[i] = localSdpSections[i] + "a=x-preferences:\(subscription.getVideoConsumer().getMaxVideoFrameArea() == Int32.max ? 0 : subscription.getVideoConsumer().getMaxVideoFrameArea())@\(subscription.getVideoConsumer().getMaxVideoFrameRate() == Int32.max ? 0 : subscription.getVideoConsumer().getMaxVideoFrameRate())\r\n";

                    }
                }
            }
        }
        return localSdpSections.joined()
    }

    private func fixRemoteSdp(_ remoteSdp: String, _ localSdp: String) -> String {
        if (dataChannelOpened) {
            var remoteSdpSections = remoteSdp.replacingOccurrences(of: "\r\nm=", with: "\r\n\0m=").components(separatedBy: "\0")
            let localSdpSections = localSdp.replacingOccurrences(of: "\r\nm=", with: "\r\n\0m=").components(separatedBy: "\0")
            for i in 2..<min(6, remoteSdpSections.count) {
                if (i == 3 || i == 5) {
                    var patterns: [NSRegularExpression]!
                    if (useVP9) {
                        patterns = [ WebRtcConnection.VP9_RTPMAP_PATTERN ]
                    } else {
                        // FIXME: Mind API doesn't support switching simulcast on/off on the fly, but WebRTC library doesn't
                        // add simulcast attributes to video sections which are initialized in `inactive` state, but if we
                        // added simulcast attributes to the offer SDP in our own (in `fixLocalSdp` method), it turned out
                        // that in such cases we have to remove all simulcast attributes from the answer SDP, otherwise the
                        // video from the camera will not being transmitted if it was switched on at very beginning (at
                        // least we encountered that problem in Mind Android and iOS SDKs which both uses the same WebRTC
                        // library).
                        if (!localSdpSections[i].contains("\r\na=simulcast")) {
                            remoteSdpSections[i] = remoteSdpSections[i].replacingOccurrences(of: "\r\na=(rid:[lmh]|simulcast).*", with: "", options: .regularExpression)
                        }
                        patterns = [ WebRtcConnection.VP8_RTPMAP_PATTERN ]
                    }
                    // FIXME: For sending its video WebRTC library always uses the codec which corresponds to the first
                    // payload stated in corresponding `m=video` section of the answer SDP. In order to force it use
                    // VP9 or VP8 regardless of the order of the payload types, we change the answer SDP and move the
                    // payload type which corresponds to the desired codec in front of the others.
                    for pattern in patterns {
                        if let match = pattern.firstMatch(in: remoteSdpSections[i], options: [], range: NSRange(location: 0, length: remoteSdpSections[i].utf16.count)) {
                            let payloadType = Range(match.range(at: 1), in: remoteSdpSections[i])!
                            remoteSdpSections[i] = remoteSdpSections[i].replacingOccurrences(of: "^(m=video [^ ]+ [^ ]+)(.*)( \(remoteSdpSections[i][payloadType]))( .*)?", with: "$1$3$2$4", options: .regularExpression)
                        }
                    }
                    // FIXME: Without `transport-cc` (i.e. with REMB only) the phase of discovering of the available
                    // bandwidth can take a significant time. During that time WebRTC library can send video in a low
                    // resolution. That's why if we are connected to Mind API which doesn't support `transport-cc` (i.e.
                    // which supports REMB only), we add a corresponding `x-google-start-bitrate` FMTP parameter to
                    // each active `recvonly` section in the answer SDP in order to force WebRTC library to send video
                    // in the highest available resolution from very beginning.
                    let videoBuffer: MediaStreamVideoBuffer! = i == 3 ? primaryPublication.getVideoBuffer() : secondaryPublication.getVideoBuffer()
                    let match = (try! NSRegularExpression(pattern: "^m=video [^ ]+ [^ ]+ ([0-9]+)")).firstMatch(in: remoteSdpSections[i], options: [], range: NSRange(location: 0, length: remoteSdpSections[i].utf16.count))!
                    if videoBuffer != nil {
                        let payloadType = remoteSdpSections[i][Range(match.range(at: 1), in: remoteSdpSections[i])!]
                        if (try! NSRegularExpression(pattern: "\r\na=rtcp-fb:\(payloadType) transport-cc").firstMatch(in: remoteSdpSections[i], options: [], range: NSRange(location: 0, length: remoteSdpSections[i].utf16.count)) == nil) {
                            let fmtpRegex = "\r\na=fmtp:\(payloadType).*"
                            let startBitrateFmtpParameter = "x-google-start-bitrate=\(videoBuffer.getBitrate() / 1000)"
                            if ((try! NSRegularExpression(pattern: fmtpRegex)).firstMatch(in: remoteSdpSections[i], options: [], range: NSRange(location: 0, length: remoteSdpSections[i].utf16.count)) != nil) {
                                remoteSdpSections[i] = remoteSdpSections[i].replacingOccurrences(of: fmtpRegex, with: "$0; \(startBitrateFmtpParameter)", options: .regularExpression)
                            } else {
                                remoteSdpSections[i] = remoteSdpSections[i] + "a=fmtp:\(payloadType) \(startBitrateFmtpParameter)\r\n"
                            }
                        }
                    }
                }
            }
            for i in 6..<remoteSdpSections.count {
                if (remoteSdpSections[i].starts(with: "m=video")) {
                    // FIXME: Any attempt to set remote SDP which contains `b=TIAS` line(s) in any `m=video` section (even
                    // if it corresponds to receiving or video) forces WebRTC library to pause (for a while) medium & high
                    // simulcast substreams of all videos which we are sending.
                    remoteSdpSections[i] = remoteSdpSections[i].replacingOccurrences(of: "\r\nb=TIAS:.*", with: "", options: .regularExpression)
                }
            }
            return remoteSdpSections.joined()
        } else {
            return remoteSdp
        }
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(ObjectIdentifier(self))
    }

    static func == (lhs: WebRtcConnection, rhs: WebRtcConnection) -> Bool {
        return lhs === rhs
    }

}
