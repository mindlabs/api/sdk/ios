import WebRTC

class WebRtcPublication {

    private var audioTransceiver: WebRtcTransceiver!
    private var videoTransceiver: WebRtcTransceiver!
    private var audioBuffer: MediaStreamAudioBuffer!
    private var videoBuffer: MediaStreamVideoBuffer!
    private var stream: MediaStream!

    func getAudioTransceiver() -> WebRtcTransceiver! {
        return audioTransceiver
    }

    func setAudioTransceiver(_ audioTransceiver: WebRtcTransceiver!) {
        self.audioTransceiver = audioTransceiver
    }

    func getVideoTransceiver() -> WebRtcTransceiver! {
        return videoTransceiver
    }

    func setVideoTransceiver(_ videoTransceiver: WebRtcTransceiver!) {
        self.videoTransceiver = videoTransceiver
    }

    func getAudioBuffer() -> MediaStreamAudioBuffer! {
        return audioBuffer
    }

    func setAudioBuffer(_ audioBuffer: MediaStreamAudioBuffer!) {
        self.audioBuffer = audioBuffer
    }

    func getVideoBuffer() -> MediaStreamVideoBuffer! {
        return videoBuffer
    }

    func setVideoBuffer(_ videoBuffer: MediaStreamVideoBuffer!) {
        self.videoBuffer = videoBuffer
    }

    func getStream() -> MediaStream! {
        return stream
    }

    func setStream(_ stream: MediaStream!) {
        self.stream = stream
    }

}
