/**
 * ***
 * Any video source of [MediaStream](x-source-tag://MediaStream) is required to implement MediaStreamVideoSupplier
 * protocol. The protocol defines two methods:
 * [addVideoConsumer](x-source-tag://MediaStreamVideoSupplier.addVideoConsumer) and
 * [removeVideoConsumer](x-source-tag://MediaStreamVideoSupplier.removeVideoConsumer).
 * [MediaStream](x-source-tag://MediaStream) uses them to register and unregister itself as a consumer of video from
 * the supplier. The supplier should use [onVideoBuffer](x-source-tag://MediaStream.onVideoBuffer) method of
 * [MediaStream](x-source-tag://MediaStream) class to supply its video to every registered media stream. The supplier
 * is expected to supply its video or `nil` value (if the video isn't available yet) to every newly registering media
 * stream, and the `nil` value to every unregistering media stream.
 *
 * Thread-Safety: all methods of MediaStreamVideoSupplier class can be called on the main thread only. Calling these
 * methods on another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: MediaStreamVideoSupplier
 */
public protocol MediaStreamVideoSupplier: AnyObject {

    /**
     * Registers the specified [MediaStream](x-source-tag://MediaStream) as a consumer of video from the supplier. It
     * is expected that during the registration the supplier will use
     * [onVideoBuffer](x-source-tag://MediaStream.onVideoBuffer) method of [MediaStream](x-source-tag://MediaStream)
     * class to supply its video or `nil` value (if the video isn't available yet) to the registering media stream.
     *
     * - Parameter consumer: The media stream which should be registered as a consumer of video from the supplier.
     *
     * - Tag: MediaStreamVideoSupplier.addVideoConsumer
     */
    func addVideoConsumer(_ consumer: MediaStream)

    /**
     * Unregisters the specified [MediaStream](x-source-tag://MediaStream) as a consumer of video from the supplier. It
     * is expected that during the unregistration the supplier will use
     * [onVideoBuffer](x-source-tag://MediaStream.onVideoBuffer) method of [MediaStream](x-source-tag://MediaStream)
     * class to supply the `nil` value to the unregistering media stream.
     *
     * - Parameter consumer: The media stream which should be unregistered as a consumer of video from the supplier.
     *
     * - Tag: MediaStreamVideoSupplier.removeVideoConsumer
     */
    func removeVideoConsumer(_ consumer: MediaStream)

}
