/**
 * ***
 * Any audio consumer of [MediaStream](x-source-tag://MediaStream) is required to implement MediaStreamAudioConsumer
 * protocol. The protocol defines only one method:
 * [onAudioBuffer](x-source-tag://MediaStreamAudioConsumer.onAudioBuffer). [MediaStream](x-source-tag://MediaStream)
 * uses it to supply a new audio to the consumer or cancel the previously supplied one. The consumer should use
 * [addAudioConsumer](x-source-tag://MediaStream.addAudioConsumer) and
 * [removeAudioConsumer](x-source-tag://MediaStream.removeAudioConsumer) methods of
 * [MediaStream](x-source-tag://MediaStream) class to register and unregister itself as a consumer of audio from the
 * [MediaStream](x-source-tag://MediaStream), respectively.
 *
 * Thread-Safety: all methods of MediaStreamAudioConsumer class can be called on the main thread only. Calling these
 * methods on another thread will result in an `Error` or undefined behavior.
 *
 * - Tag: MediaStreamAudioConsumer
 */
public protocol MediaStreamAudioConsumer: Hashable {

    /**
     * Supplies a new audio to the consumer or cancels the previously supplied one in which case `nil` is passed as a
     * value for `audioBuffer` argument. It is guaranteed that during
     * [unregistration](x-source-tag://MediaStream.removeAudioConsumer) this method is always called with `nil` as a
     * value for `audioBuffer` argument regardless whether audio has been supplied to the consumer or not.
     *
     * - Parameter audioBuffer: The new audio buffer or `nil` if the previously supplied audio buffer should be canceled.
     * - Parameter supplier: The media stream which supplies or cancels the audio buffer.
     *
     * - Tag: MediaStreamAudioConsumer.onAudioBuffer
     */
    func onAudioBuffer(_ audioBuffer: MediaStreamAudioBuffer!, _ supplier: MediaStream)

}
