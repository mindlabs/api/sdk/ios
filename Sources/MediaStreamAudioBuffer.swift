import WebRTC

public class MediaStreamAudioBuffer {

    private let track: RTCAudioTrack
    private let remote: Bool

    init(_ track: RTCAudioTrack, _ remote: Bool) {
        self.track = track
        self.remote = remote
    }

    func getTrack() -> RTCAudioTrack {
        return self.track
    }

    func isRemote() -> Bool {
        return self.remote
    }

}
